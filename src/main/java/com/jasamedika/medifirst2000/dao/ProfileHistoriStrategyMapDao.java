package com.jasamedika.medifirst2000.dao;

import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.entity.ProfileHistoriStrategyMap;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapId;

@Lazy
@Repository
public interface ProfileHistoriStrategyMapDao extends CrudRepository<ProfileHistoriStrategyMap, ProfileHistoriStrategyMapId> {
	
	@Cacheable("ProfileHistoriStrategyMapDaofindOneBy")
	ProfileHistoriStrategyMap findOneByIdKdProfileAndIdNoHistoriAndIdKdPerspectiveAndIdKdStrategy(Integer kdProfile, String noHistori, Integer kdPerspective, Integer kdStrategy);
	
	@Query(QListAll)
    @Cacheable("ProfileHistoriStrategyMapDaoFindAllList")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);
	
	@Query(QListAll + " and model.id.kdPerspective =:kdPerspective ")
    @Cacheable("ProfileHistoriStrategyMapDaoFindAllListPage")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("kdPerspective") Integer kdPerspective, Pageable page);
	
	@Query(QListAll + " and model.id.noHistori =:noHistori")
    @Cacheable("ProfileHistoriStrategyMapDaoFindByKode")
    Map<String, Object> findByKode(@Param("kdProfile") Integer kdProfile, @Param("noHistori") String noHistori);
	
	@Query(QListAll + " and strukHistori.tglAwal >:tglAwal and strukHistori.tglAkhir <:tglAkhir ")
    @Cacheable("ProfileHistoriStrategyMapDaoFindByPeriod")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, Pageable page);
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir and model.id.kdPerspective =:kdPerspective ")
    @Cacheable("ProfileHistoriStrategyMapDaoFindByPeriodPage")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, @Param("kdPerspective") Integer kdPerspective, Pageable page);
	
	String QListAll ="select new map(model.id.kdProfile as kdProfile "
            + ", model.id.noHistori as noHistori "
            + ", strukHistori.tglAwal as tglAwal "
			+ ", strukHistori.tglAkhir as tglAkhir "
            + ", model.id.kdPerspective as kdPerspective "
			+ ", perspective.namaPerspective as namaPerspective "
            + ", model.id.kdStrategy as kdStrategy "
			+ ", strategy.namaStrategy as namaStrategy "
    		+ ", model.kdDepartemen as kdDepartemen "
    		+ ", departemen.namaDepartemen as namaDepartemen "
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", model.version as version ) from ProfileHistoriStrategyMap model "
            + " left join model.strukHistori strukHistori "
            + " left join model.perspective perspective "
            + " left join model.strategy strategy "
            + " left join model.departemen departemen "
            + " left join model.profile profile where profile.kode =:kdProfile";

}
