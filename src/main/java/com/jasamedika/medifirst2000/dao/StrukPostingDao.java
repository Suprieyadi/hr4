package com.jasamedika.medifirst2000.dao;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.entity.StrukPosting;
import com.jasamedika.medifirst2000.entity.vo.StrukPostingId;

@Lazy
@Repository
public interface StrukPostingDao extends CrudRepository<StrukPosting, StrukPostingId>{

}
