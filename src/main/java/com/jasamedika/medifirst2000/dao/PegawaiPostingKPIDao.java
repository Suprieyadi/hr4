package com.jasamedika.medifirst2000.dao;

import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.entity.PegawaiPostingKPI;
import com.jasamedika.medifirst2000.entity.vo.PegawaiPostingKPIId;

@Lazy
@Repository
public interface PegawaiPostingKPIDao extends CrudRepository<PegawaiPostingKPI, PegawaiPostingKPIId> {
	
	@Cacheable("PegawaiPostingKPIDaoFindOneBy")
	PegawaiPostingKPI findOneByIdKdProfileAndIdNoPosting(Integer kdProfile, String noPosting);
	
	@Query(QListAll)
    @Cacheable("PegawaiPostingKPIDaoFindAllList")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);
	
	@Query(QListAll + " and model.id.kdDepartemenD like %:kdDepartemenD% ")
    @Cacheable("PegawaiPostingKPIDaoFindAllListPage")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("kdDepartemenD") String kdDepartemenD, Pageable page);
	
	@Query(QListAll + " and model.id.noPosting =:noPosting")
    @Cacheable("PegawaiPostingKPIDaoFindByKode")
    Map<String, Object> findByKode(@Param("kdProfile") Integer kdProfile, @Param("noPosting") String noPosting);
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir ")
    @Cacheable("PegawaiPostingKPIDaoFindByPeriod")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, Pageable page);
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir and model.id.kdDepartemenD =:kdDepartemenD ")
    @Cacheable("PegawaiPostingKPIDaoFindByPeriodDepartemen")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, @Param("kdDepartemenD") String kdDepartemenD, Pageable page);
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir and model.id.kdDepartemenD =:kdDepartemenD and model.id.kdPegawai =:kdPegawai ")
    @Cacheable("PegawaiPostingKPIDaoFindByPeriodPegawai")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, @Param("kdDepartemenD") String kdDepartemenD, @Param("kdPegawai") String kdPegawai, Pageable page);
	
	String QListAll ="select new map(model.id.kdProfile as kdProfile "
			+ ", model.id.noPosting as noPosting "
			+ ", model.id.kdPegawai as kdPegawai "
			+ ", pegawai.namaLengkap as namaLengkap "			
            + ", model.id.noHistori as noHistori "
            + ", strukHistori.tglAwal as tglAwal "
			+ ", strukHistori.tglAkhir as tglAkhir "
			+ ", model.id.kdDepartemenD as kdDepartemenD "
			+ ", departemenD.namaDepartemen as namaDepartemenD "			
			+ ", model.id.kdPerspectiveD as kdPerspectiveD "
			+ ", perspectiveD.namaPerspective as namaPerspectiveD "
            + ", model.id.kdStrategyD as kdStrategyD "
			+ ", strategyD.namaStrategy as namaStrategyD "            
            + ", model.id.kdKPI as kdKPI "
			+ ", kpi.namaKPI as namaKPI "
            + ", model.bobotKPI as bobotKPI "
			+ ", model.targetKPI as targetKPI "
            + ", model.kdMetodeHitungActual as kdMetodeHitungActual "
			+ ", metodePerhitungan.namaMetodeHitung as namaMetodeHitung "
            + ", model.keteranganLainnya as keteranganLainnya "
			+ ", model.noRetur as noRetur "
            + ", model.noVerifikasi as noVerifikasi "
			+ ", model.noSK as noSK "
            + ", model.kdKPISK as kdKPISK "
			+ ", kpiSK.namaKPI as namaKPISK "
            + ", model.bobotKPISK as bobotKPISK "
			+ ", model.targetKPIMinSK as targetKPIMinSK "
            + ", model.targetKPIMaxSK as targetKPIMaxSK "
			+ ", model.kdMetodeHitungActualSK as kdMetodeHitungActualSK "
            + ", metodePerhitunganSK.namaMetodeHitung as namaMetodeHitungSK "
			+ ", model.kdPeriodeDataSK as kdPeriodeDataSK "
            + ", model.noHistoriActual as noHistoriActual "
			+ ", model.totalActualKPI as totalActualKPI "
            + ", model.totalScoreKPI as totalScoreKPI "
			+ ", model.noVerifikasiActual as noVerifikasiActual "
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", model.version as version ) from PegawaiPostingKPI model "
            + " left join model.pegawai pegawai "
            + " left join model.strukHistori strukHistori "
            + " left join model.strukHistoriActual strukHistoriActual "
            + " left join model.departemenD departemenD "
            + " left join model.perspectiveD perspectiveD "
            + " left join model.strategyD strategyD "
            + " left join model.kpi kpi "
            + " left join model.kpiSK kpiSK "
            + " left join model.metodePerhitungan metodePerhitungan "
            + " left join model.metodePerhitunganSK metodePerhitunganSK "
            + " left join model.profile profile where profile.kode =:kdProfile";

}
