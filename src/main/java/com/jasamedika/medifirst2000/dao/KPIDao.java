package com.jasamedika.medifirst2000.dao;

import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.entity.KPI;
import com.jasamedika.medifirst2000.entity.vo.KPIId;

@Lazy
@Repository
public interface KPIDao extends CrudRepository<KPI, KPIId> {
	
	@Cacheable("KPIDaofindOneBy")
    KPI findOneByIdKodeAndIdKdProfileAndKdDepartemen(Integer kode, Integer kdProfile, String KdDepartemen);
	
	@Query(QListAll)
    @Cacheable("KPIDaoFindAllList")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);
	
	@Query(QListAll + " and model.namaKPI like %:namaKPI% ")
    @Cacheable("KPIDaoFindAllListPage")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("namaKPI") String namaKPI, Pageable page);
	
	@Query(QListAll + " and model.id.kode =:kode")
    @Cacheable("KPIDaoFindByKode")
    Map<String, Object> findByKode(@Param("kdProfile") Integer kdProfile, @Param("kode") Integer kode);
	
	String QListAll ="select new map(model.id.kdProfile as kdProfile "
			+ ", model.id.kode as kode "
            + ", model.namaKPI as namaKPI " 
			+ ", model.kdKPIHead as kdKPIHead "
            + ", model.noUrut as noUrut "
			+ ", model.kdTypeDataObjek as kdTypeDataObjek "
            + ", typeDataObjek.namaTypeDataObjek as namaTypeDataObjek "
            + ", model.kdSatuanHasil as kdSatuanHasil "
            + ", satuanHasil.namaSatuanHasil as namaSatuanHasil "
			+ ", model.kdMetodeHitungScore as kdMetodeHitungScore "
            + ", metodePerhitungan.namaMetodeHitung as namaMetodeHitung "
            + ", model.reportDisplay as reportDisplay " 
            + ", model.kodeExternal as kodeExternal " 
            + ", model.namaExternal as namaExternal " 
            + ", model.kdDepartemen as kdDepartemen " 
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", departemen.namaDepartemen as namaDepartemen, model.version as version )  from KPI model "
            + " left join model.departemen departemen "
            + " left join model.typeDataObjek typeDataObjek "
            + " left join model.satuanHasil satuanHasil "
            + " left join model.metodePerhitungan metodePerhitungan "
            + " left join model.profile profile where profile.kode =:kdProfile";

}
