package com.jasamedika.medifirst2000.dao.custom;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.base.v2.BaseDao;
import com.jasamedika.medifirst2000.base.v2.BaseDaoImpl;
import com.jasamedika.medifirst2000.entity.Pegawai;
import com.jasamedika.medifirst2000.entity.vo.PegawaiId;
import com.jasamedika.medifirst2000.util.CommonUtil;

@Lazy
@Repository
public class PegawaiDaoCustom extends BaseDaoImpl<Pegawai,PegawaiId> {
	
	protected Class<PegawaiId> getIdClass() {
		return PegawaiId.class;
	}
	
	protected Class<Pegawai> getDomainClass() {
		return Pegawai.class;
	}

	@PersistenceContext
	EntityManager em;

	@SuppressWarnings("unchecked")	
	public List<Map<String, Object>> findAll(Integer kdProfile) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(
				"select model from Pegawai model join  model.id id where id.kdProfile=:kdProfile and model.statusEnabled=true  ");
		Query query = em.createQuery(buffer.toString());
		query.setParameter("kdProfile", kdProfile);
		return (List<Map<String, Object>>) query.getResultList();
	}

	
	public int findAllCount(Integer kdProfile, String namaLengkap) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select count(model) ");
		buffer.append(
				"from Pegawai model left join model.id id where id.kdProfile=:kdProfile and model.statusEnabled=true ");
		Query query = em.createQuery(buffer.toString());
		query.setParameter("kdProfile", kdProfile);
		return ((Long) query.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findAllList(Integer kdProfile, Integer page, Integer limit, String sort,
			String dir, int rowStart, int rowEnd, String namaLengkap) {
		StringBuffer buffer = new StringBuffer();
		// buffer.append("select new map (pegawai as pegawai, jenisKelamin.namaJenisKelamin as jenisKelamin, status.namaStatus as statusPegawai, ");
		// buffer.append("kualifikasiJurusan.namaKualifikasiJurusan as kualifikasiJurusan, pangkat.namaPangkat as pangkat, eselon.namaEselon as eselon, ");
		// buffer.append("jabatan.namaJabatan as jabatanStruktural, jabatan.namaJabatan as jabatanFungsional, golonganDarah.namaGolonganDarah as golonganDarah, ");
		// buffer.append("typePegawai.namaTypePegawai as typePegawai, kategoryPegawai.namaKategoryPegawai as detailKategoryPegawai, negara.namaNegara as negara, ");
		// buffer.append("ruangan.namaRuangan as ruanganKerja, agama.namaAgama as agama) ");
		
		// buffer.append("from Pegawai pegawai, JenisKelamin jenisKelamin, JenisPegawai jenisPegawai, Status status, ");
		// buffer.append("KualifikasiJurusan kualifikasiJurusan, Pangkat pangkat, Eselon eselon, Jabatan jabatan, GolonganDarah golonganDarah, ");
		// buffer.append("TypePegawai typePegawai, KategoryPegawai kategoryPegawai, Negara negara, Ruangan ruangan, Agama agama ");
		// buffer.append("left join pegawai.id id ");
		
		// buffer.append("where id.kdProfile=:kdProfile and pegawai.statusEnabled=true and ");
		// buffer.append("pegawai.kdJenisKelamin=jenisKelamin.id.kode and ");
		// buffer.append("pegawai.kdJenisPegawai=jenisPegawai.id.kode and ");
		// buffer.append("pegawai.kdStatusPegawai=status.id.kode and ");
		// buffer.append("pegawai.kdKualifikasiJurusan=kualifikasiJurusan.id.kode and ");
		// buffer.append("pegawai.kdPangkat=pangkat.id.kode and ");
		// buffer.append("pegawai.kdEselon=eselon.id.kode and ");
		// buffer.append("pegawai.kdJabatanStruktural=jabatan.id.kode and ");
		// buffer.append("pegawai.kdJabatanFungsional=jabatan.id.kode and ");
		// buffer.append("pegawai.kdGolonganDarah=golonganDarah.id.kode and ");
		// buffer.append("pegawai.kdTypePegawai=typePegawai.id.kode and ");
		// buffer.append("pegawai.kdKategoryPegawai=kategoryPegawai.id.kode and ");
		// buffer.append("pegawai.kdNegara=negara.id.kode and ");
		// buffer.append("pegawai.kdRuanganKerja=ruangan.id.kode and ");
		// buffer.append("pegawai.kdAgama=agama.id.kode ");

		buffer.append("select model from Pegawai model left join model.id id where id.kdProfile=:kdProfile and model.statusEnabled=true ");
		
		
		buffer.append("order by model." + dir + " " + sort);
		Query query = em.createQuery(buffer.toString());
		query.setParameter("kdProfile", kdProfile);
		query.setFirstResult(rowStart);
		query.setMaxResults(rowEnd);
		List<Map<String, Object>> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")	
	public List<Map<String, Object>> findPegawaiTake10(String namaLengkap, Integer kdProfile) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select new map( model.id.kode as kdPegawai,model.namaLengkap as namaLengkap,model.kdJabatan as kdJabatan ) from Pegawai model left join model.id id where id.kdProfile=:kdProfile and model.statusEnabled=true ");
		if(CommonUtil.isNotNullOrEmpty(namaLengkap)){
			buffer.append(" and lower(model.namaLengkap) like :namaLengkap ");
		}
		Query query = em.createQuery(buffer.toString());
		query.setParameter("kdProfile", kdProfile);
		if(CommonUtil.isNotNullOrEmpty(namaLengkap)){
			query.setParameter("namaLengkap", "%"+namaLengkap.toLowerCase()+"%");
		}
		
		query.setMaxResults(10);
		List<Map<String, Object>> list = query.getResultList();
		return list;
	}
}
