package com.jasamedika.medifirst2000.dao.custom;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.base.v2.BaseDao;
import com.jasamedika.medifirst2000.base.v2.BaseDaoImpl;
import com.jasamedika.medifirst2000.entity.Agama;
import com.jasamedika.medifirst2000.entity.vo.AgamaId;
import com.jasamedika.medifirst2000.util.CommonUtil;

/*
 * 
 * Auhor :  Andri H Gunandi
 */
@Lazy
@Repository
public class GenericModelDaoCustom extends BaseDaoImpl<Agama, AgamaId> {

	@PersistenceContext
	private EntityManager em;

	
	protected Class<AgamaId> getIdClass() {
		return AgamaId.class;
	}

	
	protected Class<Agama> getDomainClass() {
		return Agama.class;
	}

	
	public int dataCount(String entity, String criteria, String values, Integer kdprofile, String condition,
			String useProfile) {
		StringBuffer buffer = new StringBuffer();
		StringBuilder filedCriteria = new StringBuilder("");
		filedCriteria.append(parseCriteria(criteria, values, condition)); // criteria

		buffer.append("select count(model)  from ").append(entity).append(" model left join model.id id where ");
		if (useProfile.toLowerCase().contains("y")) {
			buffer.append(" (id.kdProfile=:kdProfile and model.statusEnabled=true) ");
		} else {
			buffer.append(" model.statusEnabled=true ");
		}

		buffer.append(filedCriteria.toString());

		Query query = em.createQuery(buffer.toString());
		if (useProfile.toLowerCase().contains("y")) {
			query.setParameter("kdProfile", kdprofile);
		}
		int total = ((Long) query.getSingleResult()).intValue();

		return total;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object> getData(String entity, String field, Integer rowStart, Integer rowEnd, String criteria,
			String values, Integer kdprofile, String condition, String useProfile) {

		StringBuilder filedShow = new StringBuilder();
		filedShow.append(parseField(field));

		StringBuilder filedCriteria = new StringBuilder();
		filedCriteria.append(parseCriteria(criteria, values, condition)); // criteria

		StringBuilder entitySTR = new StringBuilder();
		entitySTR.append(entity).append(" model");

		StringBuffer buffer = new StringBuffer();
		buffer.append("select ").append(filedShow.toString()).append(" from ").append(entity)
				.append(" model left join model.id id where ");
		if (useProfile.toLowerCase().contains("y")) {
			buffer.append(" (id.kdProfile=:kdProfile and model.statusEnabled=true) ");
		} else {
			buffer.append(" model.statusEnabled=true ");
		}

		buffer.append(filedCriteria.toString());

		if (field.split(",").length == 2) {
			buffer.append(" order by " + field.split(",")[1]);
		}

		Query query = em.createQuery(buffer.toString());
		if (useProfile.toLowerCase().contains("y")) {
			query.setParameter("kdProfile", kdprofile);
		}

		if (rowStart < 1) {
			query.setFirstResult(0);
		} else {
			query.setFirstResult(rowStart);
		}

		query.setMaxResults(rowEnd);
		List<Object> list = query.getResultList();

		return list;
	}

	public String parseField(String field) {

		StringBuilder filedShow = new StringBuilder();
		StringBuilder tmp = new StringBuilder();
		if (field.equals("*")) {
			filedShow.append("model");
		} else if (!field.equals("")) {
			String[] fields = field.split(",");

			for (String fieldisi : fields) {
				if (tmp.length() > 0) {
					tmp.append(",");
				}
				tmp.append("model.").append(fieldisi).append(" as ").append(fieldisi.replace(".", "_"));
			}

			filedShow.append(" new map(").append(tmp.toString()).append(" ) ");

		} else {
			filedShow.append("model");
		}

		return filedShow.toString();
	}

	public String parseCriteria(String criteria, String values, String condition) {
		StringBuilder buffer = new StringBuilder();
		StringBuilder result = new StringBuilder();
		if (criteria != null && values != null) {
			String[] field = criteria.split(",");
			String[] value = values.split(",");
			if (field.length == value.length) {

				for (int x = 0; x < field.length; x++) {
					if (value[x].contains("{") && value[x].contains("}")) {
						buffer.append(" ");
						buffer.append(condition);
						buffer.append(" model.").append(field[x]).append(" = ")
								.append(value[x].replace("{", "").replace("}", ""));
					} else if (value[x].contains("[") && value[x].contains("]")) {
						buffer.append(" ");
						buffer.append(condition);
						buffer.append(" date(model.").append(field[x]).append(") = ")
								.append(value[x].replace("[", "'").replace("]", "'"));
					} else {
						/*
						 * buffer.append(" and cast(model.").append(field[x]).append(" as string) like "
						 * ); buffer.append("'%").append(value[x]).append("%' ");
						 */

						if (x > 0) {
							buffer.append(" ");
							buffer.append(condition);
						}
						if (field[x].toLowerCase().contains("kd") || field[x].toLowerCase().contains("id.")) {
							buffer.append(" lower(model.").append(field[x]).append(") = ");
							buffer.append(value[x].toLowerCase()).append(" ");

						} else {

							buffer.append(" lower(model.").append(field[x]).append(") like ");
							buffer.append("'%").append(value[x].toLowerCase()).append("%' ");
						}
					}
				}

			}
		}

		if (CommonUtil.isNotNullOrEmpty(buffer)) {
			result.append(" And ");
			if (buffer.toString().contains(" or ")) {
				result.append("(");
				result.append(buffer.toString());
				result.append(")");
			} else {
				result.append(buffer.toString());
			}
		}
		return result.toString();
	}
}
