package com.jasamedika.medifirst2000.dao.custom;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.base.v2.BaseDao;
import com.jasamedika.medifirst2000.base.v2.BaseDaoImpl;
import com.jasamedika.medifirst2000.entity.SettingDataFixed;

@Lazy
@Repository
public class SettingDataFixedDaoCustom extends BaseDaoImpl<SettingDataFixed, String> {

	@PersistenceContext
	EntityManager em;

	@SuppressWarnings("unchecked")
	public List<String> getTabelName() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select tbl.name  as NamaTable from sys.tables tbl order by  tbl.name asc ");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<String> getFieldTabel(String namaTabel) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(
				"SELECT col.name FROM sys.columns col INNER JOIN sys.tables tbl ON col.object_id=tbl.object_id WHERE tbl.Name='"
						+ namaTabel + "'");
		buffer.append("order by col.name asc");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}

	
	protected Class<SettingDataFixed> getDomainClass() {
		// TODO Auto-generated method stub
		return null;
	}

	
	protected Class<String> getIdClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<String> getPK(String namaTabel) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(
				"SELECT Col.Column_Name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab,     INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col WHERE     Col.Constraint_Name = Tab.Constraint_Name  AND Col.Table_Name = Tab.Table_Name   AND Constraint_Type = 'PRIMARY KEY'  "
						+ "  AND Col.Table_Name = '" + namaTabel + "'");
		buffer.append("order by Col.Column_Name asc");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<String> getData(String namaTabel, String field1, String field2) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT concat(" + field1 + ",' , '," + field2 + ") from " + namaTabel);
		buffer.append(" order by " + field1 + " asc");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDataOneField(String namaTabel, String field1) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT CONVERT(varchar(500),  " + field1  + ") from " + namaTabel);
		buffer.append(" order by " + field1 + " asc");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}
}
