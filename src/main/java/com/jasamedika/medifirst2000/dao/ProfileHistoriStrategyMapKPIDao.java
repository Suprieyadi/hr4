package com.jasamedika.medifirst2000.dao;

import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.entity.ProfileHistoriStrategyMapDKPI;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapDKPIId;

@Lazy
@Repository
public interface ProfileHistoriStrategyMapKPIDao extends CrudRepository<ProfileHistoriStrategyMapDKPI, ProfileHistoriStrategyMapDKPIId> {
	
	@Cacheable("ProfileHistoriStrategyMapDKPIDaoFindOneBy")
	ProfileHistoriStrategyMapDKPI findOneByIdKdProfileAndIdNoHistori(Integer kdProfile, String noHistori);
	
	@Query(QListAll)
    @Cacheable("ProfileHistoriStrategyMapDKPIDaoFindAllList")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);
	
	@Query(QListAll + " and model.id.kdDepartemenD like %:kdDepartemenD% ")
    @Cacheable("ProfileHistoriStrategyMapDKPIDaoFindAllListPage")
    Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("kdDepartemenD") String kdDepartemenD, Pageable page);
	
	@Query(QListAll + " and model.id.noHistori =:noHistori")
    @Cacheable("ProfileHistoriStrategyMapDKPIDaoFindByKode")
    Map<String, Object> findByKode(@Param("kdProfile") Integer kdProfile, @Param("noHistori") String noHistori);
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir ")
    @Cacheable("ProfileHistoriStrategyMapDKPIDaoFindByPeriod")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, Pageable page);
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir and model.id.kdDepartemenD =:kdDepartemenD ")
    @Cacheable("ProfileHistoriStrategyMapDKPIDaoFindByPeriodPage")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, @Param("kdDepartemenD") String kdDepartemenD, Pageable page);
	
	String QListAll ="select new map(model.id.kdProfile as kdProfile "
            + ", model.id.noHistori as noHistori "
            + ", strukHistori.tglAwal as tglAwal "
			+ ", strukHistori.tglAkhir as tglAkhir "
			+ ", model.id.kdPerspectiveD as kdPerspectiveD "
			+ ", perspectiveD.namaPerspective as namaPerspectiveD "
            + ", model.id.kdStrategyD as kdStrategyD "
			+ ", strategyD.namaStrategy as namaStrategyD "
            + ", model.id.kdDepartemenD as kdDepartemenD "
			+ ", departemenD.namaDepartemen as namaDepartemenD "
            + ", model.id.kdKPI as kdKPI "
			+ ", kpi.namaKPI as namaKPI "
    		+ ", model.kdDepartemen as kdDepartemen "
    		+ ", departemen.namaDepartemen as namaDepartemen "
    		+ ", model.kdPerspective as kdPerspective "
			+ ", perspective.namaPerspective as namaPerspective "
            + ", model.kdStrategy as kdStrategy "
			+ ", strategy.namaStrategy as namaStrategy "
            + ", model.bobotKPI as bobotKPI "
			+ ", model.targetKPIMin as targetKPIMin "
            + ", model.targetKPIMax as targetKPIMax "
			+ ", model.kdMetodeHitungActual as kdMetodeHitungActual "
            + ", metodePerhitungan.namaMetodeHitung as namaMetodeHitung "
			+ ", model.keteranganLainnya as keteranganLainnya "
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", model.version as version ) from ProfileHistoriStrategyMapDKPI model "
            + " left join model.strukHistori strukHistori "
            + " left join model.perspective perspective "
            + " left join model.strategy strategy "
            + " left join model.departemen departemen "
            + " left join model.perspectiveD perspectiveD "
            + " left join model.strategyD strategyD "
            + " left join model.departemenD departemenD "
            + " left join model.kpi kpi "
            + " left join model.metodePerhitungan metodePerhitungan "
            + " left join model.profile profile where profile.kode =:kdProfile";

}
