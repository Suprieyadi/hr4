package com.jasamedika.medifirst2000.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.jasamedika.medifirst2000.entity.ProfileHistoriVisiMisi;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriVisiMisiId;

@Lazy
@Repository
public interface ProfileHistoriVisiMisiDao extends CrudRepository<ProfileHistoriVisiMisi, ProfileHistoriVisiMisiId> {
	
	@Cacheable("ProfileHistoriVisiMisiDaofindOneBy")
	ProfileHistoriVisiMisi findOneByIdKdProfileAndIdNoHistoriAndIdKdMisi(Integer kdProfile, String noHistori, Integer kdMisi);
	
	@Cacheable("ProfileHistoriVisiMisiDaofindOneBy2")
	List<ProfileHistoriVisiMisi> findOneByIdKdProfileAndIdNoHistori(Integer kdProfile, String noHistori);
	
	@Query(QListGroup + " and model.id.noHistori =:noHistori GROUP BY strukHistori.tglAwal, strukHistori.tglAkhir, strukHistori.id.noHistori ")
    @Cacheable("ProfileHistoriVisiMisiDaoFindByKode")
    List<Map<String, Object>> findByKode(@Param("kdProfile") Integer kdProfile, @Param("noHistori") String noHistori);
	
	
	@Query(QListAll + " and strukHistori.tglAwal >=:tglAwal and strukHistori.tglAkhir <=:tglAkhir ")
    @Cacheable("ProfileHistoriVisiMisiDaoFindByPeriod")
    Page<Map<String, Object>> findByPeriod(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir, Pageable page);
	
	@Query(QListGroup + QListGroupby)
    @Cacheable("ProfileHistoriVisiMisiDaoFindAllPeriod")
    Page<Map<String, Object>> findAllPeriod(@Param("kdProfile") Integer kdProfile, Pageable page);
	
	@Query(QListGroup + " and strukHistori.tglAwal >=:startDates and strukHistori.tglAwal <=:endDates "+QListGroupby)
    @Cacheable("ProfileHistoriVisiMisiDaoFindPerPeriod")
    Page<Map<String, Object>> findPerPeriod(@Param("kdProfile") Integer kdProfile, Pageable page, @Param("startDates") Long startDates, @Param("endDates") Long endDates);
	
	@Query(QListAll + " and strukHistori.tglAwal =:tglAwal and strukHistori.tglAkhir =:tglAkhir ")
    @Cacheable("ProfileHistoriVisiMisiDaoFindAllGroup")
    List<Map<String, Object>> findAllGroup(@Param("kdProfile") Integer kdProfile, @Param("tglAwal") Long tglAwal, @Param("tglAkhir") Long tglAkhir);
	
	@Query(QListAll + " and model.id.noHistori =:noHistori ")
    @Cacheable("ProfileHistoriVisiMisiDaoFindAllGroup")
    List<Map<String, Object>> findListByKode(@Param("kdProfile") Integer kdProfile, @Param("noHistori") String noHistori);
	
	String QListAll ="select new map(model.id.kdProfile as kdProfile "
            + ", model.id.kdMisi as kdMisi "
    		+ ", misi.namaMisi as namaMisi "
    		+ ", misi.kdVisi as kdVisi "
    		+ ", visi.namaVisi as namaVisi "
    		+ ", model.keteranganLainnya as keteranganLainnya "
    		+ ", model.kdDepartemen as kdDepartemen "
    		+ ", departemen.namaDepartemen as namaDepartemen "         
            + ", model.version as version ) from ProfileHistoriVisiMisi model "
            + " left join model.misi misi "
            + " left join model.misi.visi visi "
            + " left join model.strukHistori strukHistori "
            + " left join model.departemen departemen "
            + " left join model.profile profile where profile.kode =:kdProfile and model.statusEnabled=true ";

	String QListGroup = "select new map( strukHistori.id.noHistori as noHistori, "
			+ " strukHistori.tglAwal as tglAwal "
			+ ", strukHistori.tglAkhir as tglAkhir,strukHistori.tglHistori as tglHistori,strukHistori.statusEnabled as statusEnabled ) from ProfileHistoriVisiMisi model "
			+ " left join model.misi misi "
            + " left join model.misi.visi visi "
            + " left join model.strukHistori strukHistori "
            + " left join model.profile profile where profile.kode =:kdProfile and model.statusEnabled=true ";
	
	String QListGroupby = "  GROUP BY strukHistori.tglAwal, strukHistori.tglAkhir, strukHistori.tglHistori, strukHistori.id.noHistori,strukHistori.statusEnabled ";

	
}
