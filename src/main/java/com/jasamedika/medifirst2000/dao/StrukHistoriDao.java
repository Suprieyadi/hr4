package com.jasamedika.medifirst2000.dao;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jasamedika.medifirst2000.entity.StrukHistori;
import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;

@Lazy
@Repository
public interface StrukHistoriDao extends CrudRepository<StrukHistori, StrukHistoriId> {
	
	StrukHistori findOneByIdNoHistoriAndIdKdProfile(String noHistori, Integer kdProfile);

}
