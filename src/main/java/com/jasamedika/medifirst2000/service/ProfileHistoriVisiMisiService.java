package com.jasamedika.medifirst2000.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.ProfileHistoriVisiMisiDao;
import com.jasamedika.medifirst2000.dao.StrukHistoriDao;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriVisiMisiDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriVisiMisiDto;
import com.jasamedika.medifirst2000.entity.ProfileHistoriVisiMisi;
import com.jasamedika.medifirst2000.entity.StrukHistori;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriVisiMisiId;
import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;
import com.jasamedika.medifirst2000.util.CommonUtil;
import com.jasamedika.medifirst2000.util.DateUtil;

@Lazy
@Service
public class ProfileHistoriVisiMisiService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private ProfileHistoriVisiMisiDao profileHistoriVisiMisiDao;

	@Lazy
	@Autowired
	private StrukHistoriDao strukHistoriDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	private Integer kdKelompokTransaksi = 7;

	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriVisiMisiDaoFindAllList", "ProfileHistoriVisiMisiDaoFindAllListPage",
			"ProfileHistoriVisiMisiDaofindOneBy", "ProfileHistoriVisiMisiDaoFindByPeriod" }, allEntries = true)
	public Map<String, Object> save(ProfileHistoriVisiMisiDto profileHistoriVisiMisiDto) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriVisiMisi profileHistoriVisiMisi = new ProfileHistoriVisiMisi();
		BeanUtils.copyProperties(profileHistoriVisiMisiDto, profileHistoriVisiMisi);
		profileHistoriVisiMisi.setStatusEnabled(true);
		profileHistoriVisiMisi.setNoRec(generateUuid());
		profileHistoriVisiMisi.setVersion(1);
		profileHistoriVisiMisi.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
		ProfileHistoriVisiMisiId id = new ProfileHistoriVisiMisiId();
		id.setKdProfile(loginUserService.getSession().getKdProfile());
		id.setKdMisi(profileHistoriVisiMisiDto.getKdMisi());
		id.setNoHistori(profileHistoriVisiMisiDto.getNoHistori());
		profileHistoriVisiMisi.setId(id);
		profileHistoriVisiMisi = profileHistoriVisiMisiDao.save(profileHistoriVisiMisi);
		data.put("noHistori", profileHistoriVisiMisi.getId().getNoHistori());
		data.put("message", "berhasil.ditambah");
		return data;
	}

	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriVisiMisiDaofindOneBy", "ProfileHistoriVisiMisiDaoFindAllList",
			"ProfileHistoriVisiMisiDaoFindAllListPage", "ProfileHistoriVisiMisiDaoFindByKode",
			"ProfileHistoriVisiMisiDaoFindByPeriod","ProfileHistoriVisiMisiDaoFindAllGroup","ProfileHistoriVisiMisiDaoFindAllPeriod","ProfileHistoriVisiMisiDaoFindPerPeriod" }, allEntries = true)
	public Map<String, Object> saveList(DataProfileHistoriVisiMisiDto dto) {
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> result = CommonUtil.createList();
		StrukHistori strukHistori = saveStrukHistori(dto);
		for (ProfileHistoriVisiMisiDto profileHistoriVisiMisiDto : dto.getProfileHistoriVisiMisi()) {
			Map<String, Object> dt = CommonUtil.createMap();
			ProfileHistoriVisiMisi profileHistoriVisiMisi = new ProfileHistoriVisiMisi();
			BeanUtils.copyProperties(profileHistoriVisiMisiDto, profileHistoriVisiMisi);
			profileHistoriVisiMisi.setStatusEnabled(true);
			profileHistoriVisiMisi.setNoRec(generateUuid());
			profileHistoriVisiMisi.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			
			ProfileHistoriVisiMisiId id = new ProfileHistoriVisiMisiId();
			id.setKdMisi(profileHistoriVisiMisiDto.getKdMisi());
			id.setNoHistori(strukHistori.getId().getNoHistori());
			id.setKdProfile(strukHistori.getId().getKdProfile());
			profileHistoriVisiMisi.setId(id);
			profileHistoriVisiMisi = profileHistoriVisiMisiDao.save(profileHistoriVisiMisi);

			dt.put("KdMisi", profileHistoriVisiMisi.getId().getKdMisi());
			result.add(dt);
		}
		if (result.size() > 0) {
			data.put("ProfileHistoriVisiMisi", result);
			data.put("message", "berhasil.ditambah");
		}

		return data;
	}
	

	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriVisiMisiDaoFindAllList", "ProfileHistoriVisiMisiDaoFindAllListPage",
	"ProfileHistoriVisiMisiDaofindOneBy", "ProfileHistoriVisiMisiDaoFindByPeriod","ProfileHistoriVisiMisiDaoFindAllGroup",
	"ProfileHistoriVisiMisiDaoFindAllPeriod","ProfileHistoriVisiMisiDaoFindPerPeriod"}, allEntries = true)
	public Map<String, Object> update(DataProfileHistoriVisiMisiDto dto,Integer version) {
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> result = CommonUtil.createList();
		StrukHistori strukHistori = updateStrukHistori(dto,version);
		
		List<ProfileHistoriVisiMisi> profileHistoriVisiMisiList = profileHistoriVisiMisiDao.findOneByIdKdProfileAndIdNoHistori(loginUserService.getSession().getKdProfile(),dto.getNoHistori());
		for (int i = 0 ; i < profileHistoriVisiMisiList.size() ; i++) {
			ProfileHistoriVisiMisi oProfileHistoriVisiMisi = profileHistoriVisiMisiList.get(i);
			ProfileHistoriVisiMisi allFalse = profileHistoriVisiMisiDao
					.findOneByIdKdProfileAndIdNoHistoriAndIdKdMisi(loginUserService.getSession().getKdProfile(),
							dto.getNoHistori(), oProfileHistoriVisiMisi.getId().getKdMisi());
			allFalse.setStatusEnabled(false);
			allFalse.setVersion(version);
			profileHistoriVisiMisiDao.save(allFalse);	
		}
		for (ProfileHistoriVisiMisiDto profileHistoriVisiMisiDto : dto.getProfileHistoriVisiMisi()) {
			Map<String, Object> dt = CommonUtil.createMap();
			ProfileHistoriVisiMisi profileHistoriVisiMisi = new ProfileHistoriVisiMisi();
			BeanUtils.copyProperties(profileHistoriVisiMisiDto, profileHistoriVisiMisi);
			profileHistoriVisiMisi.setStatusEnabled(true);
			profileHistoriVisiMisi.setNoRec(generateUuid());
			profileHistoriVisiMisi.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			
			ProfileHistoriVisiMisiId id = new ProfileHistoriVisiMisiId();
			id.setKdMisi(profileHistoriVisiMisiDto.getKdMisi());
			id.setNoHistori(strukHistori.getId().getNoHistori());
			id.setKdProfile(strukHistori.getId().getKdProfile());
			profileHistoriVisiMisi.setId(id);
			
			ProfileHistoriVisiMisi newProfileHistoriVisiMisi = profileHistoriVisiMisiDao
					.findOneByIdKdProfileAndIdNoHistoriAndIdKdMisi(loginUserService.getSession().getKdProfile(),
							dto.getNoHistori(), profileHistoriVisiMisiDto.getKdMisi());
			if (CommonUtil.isNotNullOrEmpty(newProfileHistoriVisiMisi)) {
				String noRec = newProfileHistoriVisiMisi.getNoRec();
				BeanUtils.copyProperties(profileHistoriVisiMisiDto, newProfileHistoriVisiMisi);
				newProfileHistoriVisiMisi.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
				newProfileHistoriVisiMisi.setNoRec(noRec);
				newProfileHistoriVisiMisi.setVersion(version);
				newProfileHistoriVisiMisi.setStatusEnabled(true);
				newProfileHistoriVisiMisi = profileHistoriVisiMisiDao.save(newProfileHistoriVisiMisi);				
			}
			else			
			profileHistoriVisiMisi = profileHistoriVisiMisiDao.save(profileHistoriVisiMisi);

			dt.put("KdMisi", profileHistoriVisiMisi.getId().getKdMisi());
			result.add(dt);
		}
		if (result.size() > 0) {
			data.put("ProfileHistoriVisiMisi", result);
			data.put("message", "berhasil.diubah");
		}

		return data;
	}

	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriVisiMisiDaoFindAllList", "ProfileHistoriVisiMisiDaoFindAllListPage",
			"ProfileHistoriVisiMisiDaofindOneBy", "ProfileHistoriVisiMisiDaoFindByPeriod",
			"ProfileHistoriVisiMisiDaoFindAllGroup","ProfileHistoriVisiMisiDaoFindAllPeriod","ProfileHistoriVisiMisiDaoFindPerPeriod"}, allEntries = true)
	public Map<String, Object> deleteByKode(String noHistori) {
		Map<String, Object> data = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		StrukHistori strukHistori=strukHistoriDao.findOneByIdNoHistoriAndIdKdProfile(noHistori,kdProfile);
		
		if (CommonUtil.isNotNullOrEmpty(strukHistori)) {
			strukHistori.setStatusEnabled(false);
			strukHistori = strukHistoriDao.save(strukHistori);	
			data.put("message", "berhasil.dihapus");
			data.put("noHistori", strukHistori.getId().getNoHistori());
		}
		
		return data;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> findByPeriod(Integer page, Integer limit, String sort, String dir, Long startDate,
			Long endDate) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		pageRes = profileHistoriVisiMisiDao.findByPeriod(kdProfile, startDate, endDate, pageReq);
		result.put("ProfileHistoriVisiMisi", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> findPeriodGroup(Integer page, Integer limit, String sort, String dir,Long startDates,Long endDates) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		List<Map<String, Object>> periodGroup;
		if (CommonUtil.isNullOrEmpty(startDates) || CommonUtil.isNullOrEmpty(endDates)) {
			pageRes = profileHistoriVisiMisiDao.findAllPeriod(kdProfile, pageReq);
		}else{
			pageRes = profileHistoriVisiMisiDao.findPerPeriod(kdProfile, pageReq,startDates,endDates);
		}
		
		
		List<Map<String, Object>> listDates = pageRes.getContent();
		List<Map<String, Object>> listRes = new ArrayList<Map<String, Object>>();
		Long startDate, endDate; String noHistori;
		
		for (int i = 0 ; i < listDates.size() ; i++) {
            Map<String, Object> dateMap = listDates.get(i);
            Map<String, Object> newMap = new HashMap<String, Object>();
            for (Entry<String, Object> entrySet : dateMap.entrySet()) {            	
            	newMap.put(entrySet.getKey(), entrySet.getValue());
            }
            
            startDate = (Long) newMap.get("tglAwal");
            endDate = (Long) newMap.get("tglAkhir");
            noHistori = (String) newMap.get("noHistori");
            
            periodGroup = profileHistoriVisiMisiDao.findListByKode(kdProfile,noHistori);
            
            newMap.put("visiMisi", periodGroup);
            
            listRes.add(newMap);
        }

		result.put("ProfileHistoriVisiMisi", listRes);
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object>  findByKode(String noHistori){
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		List<Map<String, Object>> periodGroup;
		List<Map<String, Object>> listDates = profileHistoriVisiMisiDao.findByKode(kdProfile, noHistori);
		List<Map<String, Object>> listRes = new ArrayList<Map<String, Object>>();
		
		for (int i = 0 ; i < listDates.size() ; i++) {
            Map<String, Object> dateMap = listDates.get(i);
            Map<String, Object> newMap = new HashMap<String, Object>();
            for (Entry<String, Object> entrySet : dateMap.entrySet()) {            	
            	newMap.put(entrySet.getKey(), entrySet.getValue());
            }
            
            periodGroup = profileHistoriVisiMisiDao.findListByKode(kdProfile,noHistori);
            
            newMap.put("visiMisi", periodGroup);
            
            listRes.add(newMap);
        }

		result.put("ProfileHistoriVisiMisi", listRes);
		
		return result;
	}
	
	StrukHistori saveStrukHistori(DataProfileHistoriVisiMisiDto dto){
		StrukHistori strukHistori = new StrukHistori();
		strukHistori.setTglAwal(dto.getTglAwal());
		strukHistori.setTglAkhir(dto.getTglAkhir());
		strukHistori.setTglHistori(dto.getTglHistori());			
		String  kdRuangan = loginUserService.getSession().getKdRuangan();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		Map<String, Object> nomorHistori = generateNomor(StrukHistori.class.getName(), kdKelompokTransaksi,
				kdRuangan,strukHistori.getTglHistori() );
		String noHistori = (String) nomorHistori.get("nomorUrut");
		String noUrutRuangaHistori = (String) nomorHistori.get("nomorUrutRuangan");
		
		// save strukHistori
		StrukHistoriId strukHistoriId = new StrukHistoriId();
		strukHistoriId.setKdProfile(kdProfile);
		strukHistoriId.setNoHistori(noHistori);
		strukHistori.setId(strukHistoriId);
		strukHistori.setNoUrutRuangan(noUrutRuangaHistori);
		strukHistori.setNoRec(generateUuid());
		strukHistori.setKdRuangan(kdRuangan);
		strukHistori.setVersion(1);
		strukHistori.setStatusEnabled(true);
		strukHistori.setKdKelompokTransaksi(kdKelompokTransaksi);
		strukHistori = strukHistoriDao.save(strukHistori);
	
		return strukHistori;
	}
	
	StrukHistori updateStrukHistori(DataProfileHistoriVisiMisiDto dto,Integer version){
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		StrukHistori strukHistori=strukHistoriDao.findOneByIdNoHistoriAndIdKdProfile(dto.getNoHistori(),kdProfile);
		
		if (CommonUtil.isNotNullOrEmpty(strukHistori)) {
			strukHistori.setTglAwal(dto.getTglAwal());
			strukHistori.setTglAkhir(dto.getTglAkhir());
			strukHistori.setTglHistori(dto.getTglHistori());
			strukHistori.setVersion(version);
			strukHistori.setStatusEnabled(dto.getStatusEnabled());
			strukHistori = strukHistoriDao.save(strukHistori);			
		}
		
		return strukHistori ;
		
	}
	

}
