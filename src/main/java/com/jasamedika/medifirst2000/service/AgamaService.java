package com.jasamedika.medifirst2000.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.AgamaDao;
import com.jasamedika.medifirst2000.dto.AgamaDto;
import com.jasamedika.medifirst2000.entity.Agama;
import com.jasamedika.medifirst2000.entity.vo.AgamaId;
import com.jasamedika.medifirst2000.util.CommonUtil;

@Lazy
@Service
public class AgamaService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private AgamaDao agamaDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	@Transactional(readOnly = false)
	@CacheEvict(value = { "AgamaDaoFindAllList", "AgamaDaoFindAllListPage", "AgamaDaofindOneBy" ,"AgamaDaoFindByKode" }, allEntries = true)
	public Map<String, Object> save(AgamaDto agamaDto) {
		Map<String, Object> data = new HashMap<String, Object>();
		Agama agama = new Agama();
		BeanUtils.copyProperties(agamaDto, agama);
		agama.setStatusEnabled(true);
		agama.setNoRec(generateUuid());
		agama.setVersion(1);
		agama.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
		AgamaId id = new AgamaId();
		id.setKode(getIdTerahir(Agama.class));
		id.setKdProfile(loginUserService.getSession().getKdProfile());
		agama.setId(id);
		agama = agamaDao.save(agama);
		data.put("kdAgama", agama.getId().getKode());
		data.put("message", "berhasil.ditambah");
		return data;
	}

	@Transactional(readOnly = false)
	@CacheEvict(value = { "AgamaDaoFindAllList", "AgamaDaoFindAllListPage", "AgamaDaofindOneBy",
			"AgamaDaoFindByKode" }, allEntries = true)
	public Map<String, Object> update(AgamaDto agamaDto, Integer version) {
		Map<String, Object> data = new HashMap<String, Object>();
		Agama agama = agamaDao.findOneByIdKodeAndIdKdProfileAndKdDepartemen(agamaDto.getKode(),
				loginUserService.getSession().getKdProfile(), loginUserService.getSession().getKdDepartemen());
		if (CommonUtil.isNotNullOrEmpty(agama)) {
			String noRec = agama.getNoRec();
			BeanUtils.copyProperties(agamaDto, agama);
			agama.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			agama.setNoRec(noRec);
			agama.setVersion(version);
			agama = agamaDao.save(agama);
			data.put("kdAgama", agama.getId().getKode());
			data.put("message", "berhasil.diubah");
		} else {
			data.put("message", "data.tidak.ada");
		}

		return data;
	}

	@Transactional(readOnly = false)
	@CacheEvict(value = { "AgamaDaoFindAllList", "AgamaDaoFindAllListPage", "AgamaDaofindOneBy",
			"AgamaDaoFindByKode" }, allEntries = true)
	public Map<String, Object> deleteByKode(Integer kode) {
		Map<String, Object> data = new HashMap<String, Object>();
		Agama agama = agamaDao.findOneByIdKodeAndIdKdProfileAndKdDepartemen(kode,
				loginUserService.getSession().getKdProfile(), loginUserService.getSession().getKdDepartemen());

		if (CommonUtil.isNotNullOrEmpty(agama)) {
			agama.setStatusEnabled(false);
			agamaDao.save(agama);
			data.put("message", "berhasil.dihapus");
			data.put("kode", agama.getId().getKode());
		}

		return data;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String namaagama) {
		Map<String, Object> result = new HashMap<String, Object>();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namaagama)) {
			pageRes = agamaDao.findAllList(kdProfile, pageReq, loginUserService.getSession().getKdDepartemen());
		} else {
			pageRes = agamaDao.findAllList(kdProfile, namaagama, pageReq,
					loginUserService.getSession().getKdDepartemen());
		}
		result.put("Agama", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> findByKode(Integer kode) {
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Object> agama = agamaDao.findByKode(loginUserService.getSession().getKdProfile(), kode,
				loginUserService.getSession().getKdDepartemen());
		data.put("Agama", agama);
		return data;
	}

}
