package com.jasamedika.medifirst2000.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.KPIDao;
import com.jasamedika.medifirst2000.dto.KPIDto;
import com.jasamedika.medifirst2000.entity.KPI;
import com.jasamedika.medifirst2000.entity.vo.KPIId;
import com.jasamedika.medifirst2000.util.CommonUtil;

@Lazy
@Service
public class KPIService extends BaseServiceImpl {
	
	@Lazy @Autowired
	private KPIDao kpiDao;

	@Lazy @Autowired
	private LoginUserService loginUserService;

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "KPIDaoFindAllList", "KPIDaoFindAllListPage", "KPIDaofindOneBy" }, allEntries = true)
	public Map<String, Object> save(KPIDto kpiDto) {
		Map<String, Object> data = CommonUtil.createMap();
		KPI kpi = new KPI();
		BeanUtils.copyProperties(kpiDto, kpi);
		kpi.setStatusEnabled(true);
		kpi.setNoRec(generateUuid());
		kpi.setVersion(1);
		kpi.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
		KPIId id = new KPIId();
		id.setKode(getIdTerahir(KPI.class));
		id.setKdProfile(loginUserService.getSession().getKdProfile());
		kpi.setId(id);
		kpi = kpiDao.save(kpi);
		data.put("kdKPI", kpi.getId().getKode());
		data.put("message", "berhasil.ditambah");
		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "KPIDaoFindAllList", "KPIDaoFindAllListPage", "KPIDaofindOneBy" }, allEntries = true)
	public Map<String, Object> update(KPIDto kpiDto, Integer version) {
		Map<String, Object> data = CommonUtil.createMap();
		KPI kpi = kpiDao.findOneByIdKodeAndIdKdProfileAndKdDepartemen(kpiDto.getKode(),
				loginUserService.getSession().getKdProfile(), loginUserService.getSession().getKdDepartemen());
		if (CommonUtil.isNotNullOrEmpty(kpi)) {
			String noRec = kpi.getNoRec();
			BeanUtils.copyProperties(kpiDto, kpi);
			kpi.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			kpi.setNoRec(noRec);
			kpi.setVersion(version);
			kpi = kpiDao.save(kpi);
			data.put("kdKPI", kpi.getId().getKode());
			data.put("message", "berhasil.diubah");
		} else {
			data.put("message", "data.tidak.ada");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "KPIDaoFindAllList", "KPIDaoFindAllListPage", "KPIDaofindOneBy" }, allEntries = true)
	public Map<String, Object> deleteByKode(Integer kode) {
		Map<String, Object> data = CommonUtil.createMap();
		KPI kpi = kpiDao.findOneByIdKodeAndIdKdProfileAndKdDepartemen(kode,
				loginUserService.getSession().getKdProfile(), loginUserService.getSession().getKdDepartemen());

		if (CommonUtil.isNotNullOrEmpty(kpi)) {
			kpi.setStatusEnabled(false);
			kpiDao.save(kpi);
			data.put("message", "berhasil.dihapus");
			data.put("kode", kpi.getId().getKode());
		}

		return data;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String namakpi) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namakpi)) {
			pageRes = kpiDao.findAllList(kdProfile, pageReq);
		} else {
			pageRes = kpiDao.findAllList(kdProfile, namakpi, pageReq);
		}
		result.put("KPI", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	
	@Transactional(readOnly = true)
	public Map<String, Object> findByKode(Integer kode) {
		Map<String, Object> data = CommonUtil.createMap();
		Map<String, Object> kpi = kpiDao.findByKode(loginUserService.getSession().getKdProfile(), kode);
		data.put("KPI", kpi);
		return data;
	}

}
