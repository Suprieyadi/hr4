package com.jasamedika.medifirst2000.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.ProfileHistoriStrategyMapDao;
import com.jasamedika.medifirst2000.dao.StrukHistoriDao;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriStrategyMapDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriStrategyMapDto;
import com.jasamedika.medifirst2000.entity.ProfileHistoriStrategyMap;
import com.jasamedika.medifirst2000.entity.StrukHistori;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapId;
import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;
import com.jasamedika.medifirst2000.util.CommonUtil;
import com.jasamedika.medifirst2000.util.DateUtil;

@Lazy
@Service
public class ProfileHistoriStrategyMapService extends BaseServiceImpl {
	
	@Lazy @Autowired
	private ProfileHistoriStrategyMapDao profileHistoriStrategyMapDao;
	
	@Lazy
	@Autowired
	private StrukHistoriDao strukHistoriDao;

	@Lazy @Autowired
	private LoginUserService loginUserService;
	
	private Integer kdKelompokTransaksi=7;
	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDaofindOneBy", "ProfileHistoriStrategyMapDaoFindAllList", "ProfileHistoriStrategyMapDaoFindAllListPage",
			"ProfileHistoriStrategyMapDaoFindByKode", "ProfileHistoriStrategyMapDaoFindByPeriod","ProfileHistoriStrategyMapDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> saveList(DataProfileHistoriStrategyMapDto dto) {
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> result = CommonUtil.createList();
		for (ProfileHistoriStrategyMapDto profileHistoriStrategyMapDto : dto.getProfileHistoriStrategyMap()) {
			Map<String, Object> dt = CommonUtil.createMap();
			ProfileHistoriStrategyMap profileHistoriStrategyMap = new ProfileHistoriStrategyMap();
			BeanUtils.copyProperties(profileHistoriStrategyMapDto, profileHistoriStrategyMap);
			profileHistoriStrategyMap.setStatusEnabled(true);
			profileHistoriStrategyMap.setNoRec(generateUuid());
			profileHistoriStrategyMap.setVersion(1);
			profileHistoriStrategyMap.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			
			String kdRuangan=loginUserService.getSession().getKdRuangan();
			Integer kdProfile=loginUserService.getSession().getKdProfile();
			Map<String,Object> nomorHistori=generateNomor(StrukHistori.class.getName(), kdKelompokTransaksi, kdRuangan, DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			String noHistori = (String)nomorHistori.get("nomorUrut");
			String noUrutRuangaHistori=(String)nomorHistori.get("nomorUrutRuangan");

			ProfileHistoriStrategyMapId id = new ProfileHistoriStrategyMapId();
			id.setKdPerspective(profileHistoriStrategyMapDto.getKdPerspective());
			id.setKdStrategy(profileHistoriStrategyMapDto.getKdStrategy());
			id.setNoHistori(noHistori);
			id.setKdProfile(kdProfile);
			profileHistoriStrategyMap.setId(id);
			profileHistoriStrategyMap = profileHistoriStrategyMapDao.save(profileHistoriStrategyMap);
			
			//save strukHistori			
			StrukHistori strukHistori = new StrukHistori();
			StrukHistoriId strukHistoriId = new StrukHistoriId();
			strukHistoriId.setKdProfile(kdProfile);
			strukHistoriId.setNoHistori(noHistori);
			strukHistori.setId(strukHistoriId);
			strukHistori.setNoUrutRuangan(noUrutRuangaHistori);
			strukHistori.setNoRec(generateUuid());
			strukHistori.setKdRuangan(kdRuangan);
			strukHistori.setTglAwal(profileHistoriStrategyMapDto.getTglAwal());
			strukHistori.setTglAkhir(profileHistoriStrategyMapDto.getTglAkhir());
			strukHistori.setTglHistori(DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			strukHistori.setStatusEnabled(true);
			strukHistori.setKdKelompokTransaksi(kdKelompokTransaksi);
			strukHistori = strukHistoriDao.save(strukHistori);
			
			dt.put("KdPerspective", profileHistoriStrategyMap.getId().getKdPerspective());
			dt.put("KdStrategy", profileHistoriStrategyMap.getId().getKdStrategy());
			result.add(dt);
		}
		if (result.size() > 0) {
			data.put("ProfileHistoriStrategyMap", result);
			data.put("message", "berhasil.ditambah");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDaofindOneBy", "ProfileHistoriStrategyMapDaoFindAllList", "ProfileHistoriStrategyMapDaoFindAllListPage",
			"ProfileHistoriStrategyMapDaoFindByKode", "ProfileHistoriStrategyMapDaoFindByPeriod","ProfileHistoriStrategyMapDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> update(ProfileHistoriStrategyMapDto profileHistoriStrategyMapDto, Integer version) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriStrategyMap profileHistoriStrategyMap = profileHistoriStrategyMapDao.findOneByIdKdProfileAndIdNoHistoriAndIdKdPerspectiveAndIdKdStrategy(loginUserService.getSession().getKdProfile(), profileHistoriStrategyMapDto.getNoHistori(), profileHistoriStrategyMapDto.getKdPerspective(), profileHistoriStrategyMapDto.getKdStrategy());
		if (CommonUtil.isNotNullOrEmpty(profileHistoriStrategyMap)) {
			String noRec = profileHistoriStrategyMap.getNoRec();
			BeanUtils.copyProperties(profileHistoriStrategyMapDto, profileHistoriStrategyMap);
			profileHistoriStrategyMap.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			profileHistoriStrategyMap.setNoRec(noRec);
			profileHistoriStrategyMap.setVersion(version);
			profileHistoriStrategyMap = profileHistoriStrategyMapDao.save(profileHistoriStrategyMap);
			data.put("noHistori", profileHistoriStrategyMap.getId().getNoHistori());
			data.put("kdPerspective", profileHistoriStrategyMap.getId().getKdPerspective());
			data.put("kdStrategy", profileHistoriStrategyMap.getId().getKdStrategy());
			data.put("message", "berhasil.diubah");
		} else {
			data.put("message", "data.tidak.ada");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDaofindOneBy", "ProfileHistoriStrategyMapDaoFindAllList", "ProfileHistoriStrategyMapDaoFindAllListPage",
			"ProfileHistoriStrategyMapDaoFindByKode", "ProfileHistoriStrategyMapDaoFindByPeriod","ProfileHistoriStrategyMapDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> deleteByKode(String noHistori, Integer kdPerspective, Integer kdStrategy) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriStrategyMap profileHistoriStrategyMap = profileHistoriStrategyMapDao.findOneByIdKdProfileAndIdNoHistoriAndIdKdPerspectiveAndIdKdStrategy(loginUserService.getSession().getKdProfile(), noHistori, kdPerspective, kdStrategy);

		if (CommonUtil.isNotNullOrEmpty(profileHistoriStrategyMap)) {
			profileHistoriStrategyMap.setStatusEnabled(false);
			profileHistoriStrategyMapDao.save(profileHistoriStrategyMap);
			data.put("message", "berhasil.dihapus");
			data.put("noHistori", profileHistoriStrategyMap.getId().getNoHistori());
			data.put("kdPerspective", profileHistoriStrategyMap.getId().getKdPerspective());
			data.put("kdStrategy", profileHistoriStrategyMap.getId().getKdStrategy());
		}

		return data;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, Integer kdPerspective) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(kdPerspective)) {
			pageRes = profileHistoriStrategyMapDao.findAllList(kdProfile, pageReq);
		} else {
			pageRes = profileHistoriStrategyMapDao.findAllList(kdProfile, kdPerspective, pageReq);
		}
		result.put("ProfileHistoriStrategyMap", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findByPeriod(Integer page, Integer limit, String sort, String dir, Long startDate, Long endDate, Integer kdPerspective) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		
		if (CommonUtil.isNullOrEmpty(kdPerspective)) {
			pageRes = profileHistoriStrategyMapDao.findByPeriod(kdProfile, startDate, endDate, pageReq);
		} else {
			pageRes = profileHistoriStrategyMapDao.findByPeriod(kdProfile, startDate, endDate, kdPerspective, pageReq);
		}
		
		result.put("ProfileHistoriStrategyMap", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	
	@Transactional(readOnly = true)
	public Map<String, Object> findByKode(String noHistori) {
		Map<String, Object> data = CommonUtil.createMap();
		Map<String, Object> profileHistoriStrategyMap = profileHistoriStrategyMapDao.findByKode(loginUserService.getSession().getKdProfile(), noHistori);
		data.put("ProfileHistoriStrategyMap", profileHistoriStrategyMap);
		return data;
	}

}
