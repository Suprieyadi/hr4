package com.jasamedika.medifirst2000.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.ProfileHistoriStrategyMapDDao;
import com.jasamedika.medifirst2000.dao.StrukHistoriDao;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriStrategyMapDDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriStrategyMapDDto;
import com.jasamedika.medifirst2000.entity.ProfileHistoriStrategyMapD;
import com.jasamedika.medifirst2000.entity.StrukHistori;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapDId;
import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;
import com.jasamedika.medifirst2000.util.CommonUtil;
import com.jasamedika.medifirst2000.util.DateUtil;

@Lazy
@Service
public class ProfileHistoriStrategyMapDService extends BaseServiceImpl {
	
	@Lazy @Autowired
	private ProfileHistoriStrategyMapDDao profileHistoriStrategyMapDDao;
	
	@Lazy
	@Autowired
	private StrukHistoriDao strukHistoriDao;
	
	@Lazy @Autowired
	private LoginUserService loginUserService;
	
	private Integer kdKelompokTransaksi=7;
	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDDaofindOneBy", "ProfileHistoriStrategyMapDDaoFindAllList", "ProfileHistoriStrategyMapDDaoFindAllListPage",
			"ProfileHistoriStrategyMapDDaoFindByKode", "ProfileHistoriStrategyMapDDaoFindByPeriod", "ProfileHistoriStrategyMapDDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> saveList(DataProfileHistoriStrategyMapDDto dto) {
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> result = CommonUtil.createList();
		for (ProfileHistoriStrategyMapDDto profileHistoriStrategyMapDDto : dto.getProfileHistoriStrategyMapD()) {
			Map<String, Object> dt = CommonUtil.createMap();
			ProfileHistoriStrategyMapD profileHistoriStrategyMapD = new ProfileHistoriStrategyMapD();
			BeanUtils.copyProperties(profileHistoriStrategyMapDDto, profileHistoriStrategyMapD);
			profileHistoriStrategyMapD.setStatusEnabled(true);
			profileHistoriStrategyMapD.setNoRec(generateUuid());
			profileHistoriStrategyMapD.setVersion(1);
			profileHistoriStrategyMapD.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			
			String kdRuangan=loginUserService.getSession().getKdRuangan();
			Integer kdProfile=loginUserService.getSession().getKdProfile();
			Map<String,Object> nomorHistori=generateNomor(StrukHistori.class.getName(), kdKelompokTransaksi, kdRuangan, DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			String noHistori = (String)nomorHistori.get("nomorUrut");
			String noUrutRuangaHistori=(String)nomorHistori.get("nomorUrutRuangan");

			ProfileHistoriStrategyMapDId id = new ProfileHistoriStrategyMapDId();
			id.setKdPerspectiveD(profileHistoriStrategyMapDDto.getKdPerspectiveD());
			id.setKdStrategyD(profileHistoriStrategyMapDDto.getKdStrategyD());
			id.setKdDepartemenD(profileHistoriStrategyMapDDto.getKdDepartemenD());
			id.setNoHistori(noHistori);
			id.setKdProfile(kdProfile);
			profileHistoriStrategyMapD.setId(id);
			profileHistoriStrategyMapD = profileHistoriStrategyMapDDao.save(profileHistoriStrategyMapD);
			
			//save strukHistori			
			StrukHistori strukHistori = new StrukHistori();
			StrukHistoriId strukHistoriId = new StrukHistoriId();
			strukHistoriId.setKdProfile(kdProfile);
			strukHistoriId.setNoHistori(noHistori);
			strukHistori.setId(strukHistoriId);
			strukHistori.setNoUrutRuangan(noUrutRuangaHistori);
			strukHistori.setNoRec(generateUuid());
			strukHistori.setKdRuangan(kdRuangan);
			strukHistori.setTglAwal(profileHistoriStrategyMapDDto.getTglAwal());
			strukHistori.setTglAkhir(profileHistoriStrategyMapDDto.getTglAkhir());
			strukHistori.setTglHistori(DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			strukHistori.setStatusEnabled(true);
			strukHistori.setKdKelompokTransaksi(kdKelompokTransaksi);
			strukHistori = strukHistoriDao.save(strukHistori);
			
			dt.put("KdDepartemenD", profileHistoriStrategyMapD.getId().getKdDepartemenD());
			dt.put("KdPerspectiveD", profileHistoriStrategyMapD.getId().getKdPerspectiveD());
			dt.put("KdStrategyD", profileHistoriStrategyMapD.getId().getKdStrategyD());
			result.add(dt);
		}
		if (result.size() > 0) {
			data.put("ProfileHistoriStrategyMapD", result);
			data.put("message", "berhasil.ditambah");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDDaofindOneBy", "ProfileHistoriStrategyMapDDaoFindAllList", "ProfileHistoriStrategyMapDDaoFindAllListPage",
			"ProfileHistoriStrategyMapDDaoFindByKode", "ProfileHistoriStrategyMapDDaoFindByPeriod", "ProfileHistoriStrategyMapDDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> update(ProfileHistoriStrategyMapDDto profileHistoriStrategyMapDDto, Integer version) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriStrategyMapD profileHistoriStrategyMapD = profileHistoriStrategyMapDDao.findOneByIdKdProfileAndIdNoHistori(loginUserService.getSession().getKdProfile(), profileHistoriStrategyMapDDto.getNoHistori());
		if (CommonUtil.isNotNullOrEmpty(profileHistoriStrategyMapD)) {
			String noRec = profileHistoriStrategyMapD.getNoRec();
			BeanUtils.copyProperties(profileHistoriStrategyMapDDto, profileHistoriStrategyMapD);
			profileHistoriStrategyMapD.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			profileHistoriStrategyMapD.setNoRec(noRec);
			profileHistoriStrategyMapD.setVersion(version);
			profileHistoriStrategyMapD = profileHistoriStrategyMapDDao.save(profileHistoriStrategyMapD);
			data.put("noHistori", profileHistoriStrategyMapD.getId().getNoHistori());
			data.put("kdDepartemenD", profileHistoriStrategyMapD.getId().getKdDepartemenD());
			data.put("kdPerspectiveD", profileHistoriStrategyMapD.getId().getKdPerspectiveD());
			data.put("kdStrategyD", profileHistoriStrategyMapD.getId().getKdStrategyD());
			data.put("message", "berhasil.diubah");
		} else {
			data.put("message", "data.tidak.ada");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDDaofindOneBy", "ProfileHistoriStrategyMapDDaoFindAllList", "ProfileHistoriStrategyMapDDaoFindAllListPage",
			"ProfileHistoriStrategyMapDDaoFindByKode", "ProfileHistoriStrategyMapDDaoFindByPeriod", "ProfileHistoriStrategyMapDDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> deleteByKode(String noHistori) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriStrategyMapD profileHistoriStrategyMapD = profileHistoriStrategyMapDDao.findOneByIdKdProfileAndIdNoHistori(loginUserService.getSession().getKdProfile(), noHistori);

		if (CommonUtil.isNotNullOrEmpty(profileHistoriStrategyMapD)) {
			profileHistoriStrategyMapD.setStatusEnabled(false);
			profileHistoriStrategyMapDDao.save(profileHistoriStrategyMapD);
			data.put("message", "berhasil.dihapus");
			data.put("noHistori", profileHistoriStrategyMapD.getId().getNoHistori());
			data.put("kdDepartemenD", profileHistoriStrategyMapD.getId().getKdDepartemenD());
			data.put("kdPerspectiveD", profileHistoriStrategyMapD.getId().getKdPerspectiveD());
			data.put("kdStrategyD", profileHistoriStrategyMapD.getId().getKdStrategyD());
		}

		return data;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String kdDepartemenD, Integer kdPerspectiveD) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(kdDepartemenD)) {
			pageRes = profileHistoriStrategyMapDDao.findAllList(kdProfile, pageReq);
		} else {
			
			if (CommonUtil.isNullOrEmpty(kdPerspectiveD)) {				
				pageRes = profileHistoriStrategyMapDDao.findAllList(kdProfile, kdDepartemenD, pageReq);
			} else {
				pageRes = profileHistoriStrategyMapDDao.findAllList(kdProfile, kdDepartemenD, kdPerspectiveD, pageReq);
			}
			
			
		}
		result.put("ProfileHistoriStrategyMapD", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findByPeriod(Integer page, Integer limit, String sort, String dir, Long startDate, Long endDate, String kdDepartemenD) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		
		if (CommonUtil.isNullOrEmpty(kdDepartemenD)) {
			pageRes = profileHistoriStrategyMapDDao.findByPeriod(kdProfile, startDate, endDate, pageReq);
		} else {
			pageRes = profileHistoriStrategyMapDDao.findByPeriod(kdProfile, startDate, endDate, kdDepartemenD, pageReq);
		}
		
		result.put("ProfileHistoriStrategyMapD", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	
	@Transactional(readOnly = true)
	public Map<String, Object> findByKode(String noHistori) {
		Map<String, Object> data = CommonUtil.createMap();
		Map<String, Object> profileHistoriStrategyMapD = profileHistoriStrategyMapDDao.findByKode(loginUserService.getSession().getKdProfile(), noHistori);
		data.put("ProfileHistoriStrategyMapD", profileHistoriStrategyMapD);
		return data;
	}

}
