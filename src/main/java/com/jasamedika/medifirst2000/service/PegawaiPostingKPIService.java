package com.jasamedika.medifirst2000.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.PegawaiPostingKPIDao;
import com.jasamedika.medifirst2000.dao.StrukHistoriDao;
import com.jasamedika.medifirst2000.dao.StrukPostingDao;
import com.jasamedika.medifirst2000.dto.DataPegawaiPostingKPIDto;
import com.jasamedika.medifirst2000.dto.PegawaiPostingKPIDto;
import com.jasamedika.medifirst2000.entity.PegawaiPostingKPI;
import com.jasamedika.medifirst2000.entity.StrukHistori;
import com.jasamedika.medifirst2000.entity.StrukPosting;
import com.jasamedika.medifirst2000.entity.StrukVerifikasi;
import com.jasamedika.medifirst2000.entity.vo.PegawaiPostingKPIId;
import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;
import com.jasamedika.medifirst2000.entity.vo.StrukPostingId;
import com.jasamedika.medifirst2000.util.CommonUtil;
import com.jasamedika.medifirst2000.util.DateUtil;

@Lazy
@Service
public class PegawaiPostingKPIService extends BaseServiceImpl {
	
	@Lazy @Autowired
	private PegawaiPostingKPIDao pegawaiPostingKPIDao;
	
	@Lazy
	@Autowired
	private StrukHistoriDao strukHistoriDao;
	
	@Lazy
	@Autowired
	private StrukPostingDao strukPostingDao;
	
	@Lazy @Autowired
	private LoginUserService loginUserService;
	
	private Integer kdKelompokTransaksi=7;
	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "PegawaiPostingKPIDaoFindOneBy", "PegawaiPostingKPIDaoFindAllList", "PegawaiPostingKPIDaoFindAllListPage", "PegawaiPostingKPIDaoFindByKode",
			"PegawaiPostingKPIDaoFindByPeriod", "PegawaiPostingKPIDaoFindByPeriodDepartemen", "PegawaiPostingKPIDaoFindByPeriodPegawai"}, allEntries = true)
	public Map<String, Object> saveList(DataPegawaiPostingKPIDto dto) {
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> result = CommonUtil.createList();
		for (PegawaiPostingKPIDto pegawaiPostingKPIDto : dto.getPegawaiPostingKPIDto()) {
			Map<String, Object> dt = CommonUtil.createMap();
			PegawaiPostingKPI pegawaiPostingKPI = new PegawaiPostingKPI();
			BeanUtils.copyProperties(pegawaiPostingKPIDto, pegawaiPostingKPI);
			pegawaiPostingKPI.setStatusEnabled(true);
			pegawaiPostingKPI.setNoRec(generateUuid());
			pegawaiPostingKPI.setVersion(1);
			
			String kdRuangan=loginUserService.getSession().getKdRuangan();
			Integer kdProfile=loginUserService.getSession().getKdProfile();
			Map<String,Object> nomorHistori=generateNomor(StrukHistori.class.getName(), kdKelompokTransaksi, kdRuangan, DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			String noHistori = (String)nomorHistori.get("nomorUrut");
			String noUrutRuangaHistori=(String)nomorHistori.get("nomorUrutRuangan");
			
			Map<String,Object> nomorVerif=generateNomor(StrukVerifikasi.class.getName(), kdKelompokTransaksi, kdRuangan, DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			String noVerifikasi = (String)nomorVerif.get("nomorUrut");
			String noUrutRuanganVerifikasi=(String)nomorVerif.get("nomorUrutRuangan");
			
			Map<String, Object> nomorPosting = generateNomor(StrukPosting.class.getName(), kdKelompokTransaksi, kdRuangan, DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			String noPosting = (String)nomorPosting.get("nomorUrut");
			String noUrutRuanganPosting = (String)nomorPosting.get("nomorUrutRuangan");

			PegawaiPostingKPIId id = new PegawaiPostingKPIId();
			id.setKdProfile(kdProfile);
			id.setNoPosting(noPosting);
			id.setKdPegawai(pegawaiPostingKPIDto.getKdPegawai());
			id.setNoHistori(noHistori);
			id.setKdDepartemenD(pegawaiPostingKPIDto.getKdDepartemenD());
			id.setKdPerspectiveD(pegawaiPostingKPIDto.getKdPerspectiveD());
			id.setKdStrategyD(pegawaiPostingKPIDto.getKdStrategyD());
			id.setKdKPI(pegawaiPostingKPIDto.getKdKPI());
			pegawaiPostingKPI.setId(id);
			pegawaiPostingKPI = pegawaiPostingKPIDao.save(pegawaiPostingKPI);
			
			//save strukHistori			
			StrukHistori strukHistori = new StrukHistori();
			StrukHistoriId strukHistoriId = new StrukHistoriId();
			strukHistoriId.setKdProfile(kdProfile);
			strukHistoriId.setNoHistori(noHistori);
			strukHistori.setId(strukHistoriId);
			strukHistori.setNoUrutRuangan(noUrutRuangaHistori);
			strukHistori.setNoRec(generateUuid());
			strukHistori.setKdRuangan(kdRuangan);
			strukHistori.setTglAwal(pegawaiPostingKPIDto.getTglAwal());
			strukHistori.setTglAkhir(pegawaiPostingKPIDto.getTglAkhir());
			strukHistori.setTglHistori(DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			strukHistori.setStatusEnabled(true);
			strukHistori.setKdKelompokTransaksi(kdKelompokTransaksi);
			strukHistori = strukHistoriDao.save(strukHistori);
			
			//save strukPosting
			StrukPosting strukPosting = new StrukPosting();
			StrukPostingId strukPostingId = new StrukPostingId();
			strukPostingId.setKdProfile(kdProfile);
			strukPostingId.setNoPosting(noPosting);
			strukPosting.setId(strukPostingId);
			strukPosting.setKdKelompokTransaksi(kdKelompokTransaksi);
			strukPosting.setKdRuangan(kdRuangan);
			strukPosting.setKdRuanganTujuan(kdRuangan);
			strukPosting.setNoUrutRuangan(noUrutRuanganPosting);
			strukPosting.setTglPosting(DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			strukPosting.setNoRec(generateUuid());
			strukPosting.setStatusEnabled(true);
			strukPosting.setVersion(1);
			strukPosting = strukPostingDao.save(strukPosting);
			
			dt.put("NoPosting", pegawaiPostingKPI.getId().getNoPosting());
			dt.put("KdPegawai", pegawaiPostingKPI.getId().getKdPegawai());
			dt.put("KdKPI", pegawaiPostingKPI.getId().getKdKPI());
			result.add(dt);
		}
		if (result.size() > 0) {
			data.put("pegawaiPostingKPI", result);
			data.put("message", "berhasil.ditambah");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "PegawaiPostingKPIDaoFindOneBy", "PegawaiPostingKPIDaoFindAllList", "PegawaiPostingKPIDaoFindAllListPage", "PegawaiPostingKPIDaoFindByKode",
			"PegawaiPostingKPIDaoFindByPeriod", "PegawaiPostingKPIDaoFindByPeriodDepartemen", "PegawaiPostingKPIDaoFindByPeriodPegawai" }, allEntries = true)
	public Map<String, Object> update(PegawaiPostingKPIDto pegawaiPostingKPIDto, Integer version) {
		Map<String, Object> data = CommonUtil.createMap();
		PegawaiPostingKPI pegawaiPostingKPI = pegawaiPostingKPIDao.findOneByIdKdProfileAndIdNoPosting(loginUserService.getSession().getKdProfile(), pegawaiPostingKPIDto.getNoPosting());
		if (CommonUtil.isNotNullOrEmpty(pegawaiPostingKPI)) {
			String noRec = pegawaiPostingKPI.getNoRec();
			BeanUtils.copyProperties(pegawaiPostingKPIDto, pegawaiPostingKPI);
			pegawaiPostingKPI.setNoRec(noRec);
			pegawaiPostingKPI.setVersion(version);
			pegawaiPostingKPI = pegawaiPostingKPIDao.save(pegawaiPostingKPI);
			data.put("noHistori", pegawaiPostingKPI.getId().getNoHistori());
			data.put("NoPosting", pegawaiPostingKPI.getId().getNoPosting());
			data.put("KdPegawai", pegawaiPostingKPI.getId().getKdPegawai());
			data.put("KdKPI", pegawaiPostingKPI.getId().getKdKPI());
			data.put("message", "berhasil.diubah");
		} else {
			data.put("message", "data.tidak.ada");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = {"PegawaiPostingKPIDaoFindOneBy", "PegawaiPostingKPIDaoFindAllList", "PegawaiPostingKPIDaoFindAllListPage", "PegawaiPostingKPIDaoFindByKode",
			"PegawaiPostingKPIDaoFindByPeriod", "PegawaiPostingKPIDaoFindByPeriodDepartemen", "PegawaiPostingKPIDaoFindByPeriodPegawai" }, allEntries = true)
	public Map<String, Object> deleteByKode(String noPosting) {
		Map<String, Object> data = CommonUtil.createMap();
		PegawaiPostingKPI pegawaiPostingKPI = pegawaiPostingKPIDao.findOneByIdKdProfileAndIdNoPosting(loginUserService.getSession().getKdProfile(), noPosting);

		if (CommonUtil.isNotNullOrEmpty(pegawaiPostingKPI)) {
			pegawaiPostingKPI.setStatusEnabled(false);
			pegawaiPostingKPIDao.save(pegawaiPostingKPI);
			data.put("message", "berhasil.dihapus");
			data.put("noHistori", pegawaiPostingKPI.getId().getNoHistori());
			data.put("NoPosting", pegawaiPostingKPI.getId().getNoPosting());
			data.put("KdPegawai", pegawaiPostingKPI.getId().getKdPegawai());
			data.put("KdKPI", pegawaiPostingKPI.getId().getKdKPI());
		}

		return data;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String kdDepartemenD) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(kdDepartemenD)) {
			pageRes = pegawaiPostingKPIDao.findAllList(kdProfile, pageReq);
		} else {
			pageRes = pegawaiPostingKPIDao.findAllList(kdProfile, kdDepartemenD, pageReq);
		}
		result.put("PegawaiPostingKPI", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findByPeriod(Integer page, Integer limit, String sort, String dir, Long startDate, Long endDate, String kdDepartemenD, String kdPegawai) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		
		if (CommonUtil.isNullOrEmpty(kdDepartemenD)) {
			pageRes = pegawaiPostingKPIDao.findByPeriod(kdProfile, startDate, endDate, pageReq);
		} else {
			
			if (CommonUtil.isNullOrEmpty(kdPegawai)) {
				pageRes = pegawaiPostingKPIDao.findByPeriod(kdProfile, startDate, endDate, kdDepartemenD, pageReq);
			} else {
				pageRes = pegawaiPostingKPIDao.findByPeriod(kdProfile, startDate, endDate, kdDepartemenD, kdPegawai, pageReq);
			}
		}
		
		result.put("PegawaiPostingKPI", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	
	@Transactional(readOnly = true)
	public Map<String, Object> findByKode(String noPosting) {
		Map<String, Object> data = CommonUtil.createMap();
		Map<String, Object> pegawaiPostingKPI = pegawaiPostingKPIDao.findByKode(loginUserService.getSession().getKdProfile(), noPosting);
		data.put("PegawaiPostingKPI", pegawaiPostingKPI);
		return data;
	}

}
