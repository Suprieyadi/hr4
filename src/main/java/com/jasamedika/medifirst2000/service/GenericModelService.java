package com.jasamedika.medifirst2000.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.custom.GenericModelDaoCustom;
import com.jasamedika.medifirst2000.util.CommonUtil;

@Lazy
@Service
public class GenericModelService extends BaseServiceImpl{
	
	@Lazy
	@Autowired
	private GenericModelDaoCustom genericModelDaoCustom;
	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	/*
	 * 
	 * 
	 * @Transactional(readOnly = false) public Map<String, Object> getAllData(String
	 * entity, String field, Integer take, Integer skip, Integer page, Integer
	 * pageSize, String criteria, String values) { int rowStart = 0; int rowEnd = 0;
	 * int totalRow = genericModelDaoCustom.dataCount(entity, criteria, values,
	 * loginUserService.getSession().getKdProfile()); int totalPages = 0; if (take
	 * != null) rowEnd = take; if (take != null && skip != null && page != null &&
	 * pageSize != null) { int pageRequested = page; if (totalRow > 0) { totalPages
	 * = (int) Math.ceil((double) totalRow / (double) pageSize); } else { totalPages
	 * = 0; }
	 * 
	 * if (pageRequested > totalPages) pageRequested = totalPages; rowStart =
	 * (pageRequested * pageSize) - pageSize; if (rowStart < 0) { rowStart = 0; }
	 * rowEnd = take; } List<Object> listEntity = null; Map<String, Object> result =
	 * CommonUtil.createMap();
	 * 
	 * try { listEntity = genericModelDaoCustom.getData(entity, field,
	 * Math.abs(rowStart), rowEnd, criteria, values,
	 * loginUserService.getSession().getKdProfile()); } catch
	 * (IllegalArgumentException e) { e.printStackTrace(); } result.put("data",
	 * listEntity); result.put("totalPages", totalPages); result.put("totalRow",
	 * totalRow); return result; }
	 */
	
	@Transactional(readOnly = false)
	public Map<String, Object> getAllData(String entity, String field, Integer page, Integer limit, String criteria,
			String values, String condition, String useProfile) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		// String kdDepartemen = loginUserService.getSession().getKdDepartemen();
		int totalRow = genericModelDaoCustom.dataCount(entity, criteria, values,
				loginUserService.getSession().getKdProfile(), condition, useProfile);
		int totalPages = 0;

		int pageRequested = page;

		if (totalRow > 0) {
			totalPages = (int) Math.ceil((double) totalRow / (double) limit);
		} else {
			totalPages = 0;
		}

		if (pageRequested > totalPages)
			pageRequested = totalPages;
		int rowStart = pageRequested * limit - limit;
		if (rowStart < 0) {
			rowStart = 0;
		}
		int rowEnd = limit;

		result.put("data", genericModelDaoCustom.getData(entity, field, Math.abs(rowStart), rowEnd, criteria, values,
				kdProfile, condition, useProfile));
		result.put("totalPages", totalPages);
		result.put("totalRow", totalRow);
		return result;
	}

	
	@Transactional(readOnly = false)
	public Map<String, Object> getAllDataPage(String entity, String field, Integer page, Integer limit, String criteria,
			String values, String condition) {
		Map<String, Object> result = CommonUtil.createMap();				
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		int totalRow = genericModelDaoCustom.dataCount(entity, criteria, values,
				loginUserService.getSession().getKdProfile(), condition,"y");
		int totalPages = 0;

		int pageRequested = page;

		if (totalRow > 0) {
			totalPages = (int) Math.ceil((double) totalRow / (double) limit);
		} else {
			totalPages = 0;
		}

		if (pageRequested > totalPages)
			pageRequested = totalPages;
		int rowStart = pageRequested;// * limit - limit;
		if (rowStart < 0) {
			rowStart = 0;
		}
		int rowEnd = limit;

		result.put("data", genericModelDaoCustom.getData(entity, field, Math.abs(rowStart), rowEnd, criteria, values,
				kdProfile, condition,"y"));
		result.put("totalPages", totalPages);
		result.put("totalRow", totalRow);
		return result;
	}

}
