package com.jasamedika.medifirst2000.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasamedika.medifirst2000.base.BaseServiceImpl;
import com.jasamedika.medifirst2000.dao.ProfileHistoriStrategyMapKPIDao;
import com.jasamedika.medifirst2000.dao.StrukHistoriDao;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriStrategyMapDKPIDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriStrategyMapDKPIDto;
import com.jasamedika.medifirst2000.entity.ProfileHistoriStrategyMapDKPI;
import com.jasamedika.medifirst2000.entity.StrukHistori;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapDKPIId;
import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;
import com.jasamedika.medifirst2000.util.CommonUtil;
import com.jasamedika.medifirst2000.util.DateUtil;

@Lazy
@Service
public class ProfileHistoriStrategyMapKPIService extends BaseServiceImpl {
	
	@Lazy @Autowired
	private ProfileHistoriStrategyMapKPIDao profileHistoriStrategyMapKPIDao;
	
	@Lazy
	@Autowired
	private StrukHistoriDao strukHistoriDao;
	
	@Lazy @Autowired
	private LoginUserService loginUserService;
	
	private Integer kdKelompokTransaksi=7;
	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDKPIDaoFindOneBy", "ProfileHistoriStrategyMapDKPIDaoFindAllList", "ProfileHistoriStrategyMapDKPIDaoFindAllListPage",
			"ProfileHistoriStrategyMapDKPIDaoFindByKode", "ProfileHistoriStrategyMapDKPIDaoFindByPeriod", "ProfileHistoriStrategyMapDKPIDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> saveList(DataProfileHistoriStrategyMapDKPIDto dto) {
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> result = CommonUtil.createList();
		for (ProfileHistoriStrategyMapDKPIDto profileHistoriStrategyMapDKPIDto : dto.getProfileHistoriStrategyMapDKPI()) {
			Map<String, Object> dt = CommonUtil.createMap();
			ProfileHistoriStrategyMapDKPI profileHistoriStrategyMapDKPI = new ProfileHistoriStrategyMapDKPI();
			BeanUtils.copyProperties(profileHistoriStrategyMapDKPIDto, profileHistoriStrategyMapDKPI);
			profileHistoriStrategyMapDKPI.setStatusEnabled(true);
			profileHistoriStrategyMapDKPI.setNoRec(generateUuid());
			profileHistoriStrategyMapDKPI.setVersion(1);
			profileHistoriStrategyMapDKPI.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			
			String kdRuangan=loginUserService.getSession().getKdRuangan();
			Integer kdProfile=loginUserService.getSession().getKdProfile();
			Map<String,Object> nomorHistori=generateNomor(StrukHistori.class.getName(), kdKelompokTransaksi, kdRuangan, DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			String noHistori = (String)nomorHistori.get("nomorUrut");
			String noUrutRuangaHistori=(String)nomorHistori.get("nomorUrutRuangan");

			ProfileHistoriStrategyMapDKPIId id = new ProfileHistoriStrategyMapDKPIId();
			id.setKdPerspectiveD(profileHistoriStrategyMapDKPIDto.getKdPerspectiveD());
			id.setKdStrategyD(profileHistoriStrategyMapDKPIDto.getKdStrategyD());
			id.setKdDepartemenD(profileHistoriStrategyMapDKPIDto.getKdDepartemenD());
			id.setKdKPI(profileHistoriStrategyMapDKPIDto.getKdKPI());
			id.setNoHistori(noHistori);
			id.setKdProfile(kdProfile);
			profileHistoriStrategyMapDKPI.setId(id);
			profileHistoriStrategyMapDKPI = profileHistoriStrategyMapKPIDao.save(profileHistoriStrategyMapDKPI);
			
			//save strukHistori			
			StrukHistori strukHistori = new StrukHistori();
			StrukHistoriId strukHistoriId = new StrukHistoriId();
			strukHistoriId.setKdProfile(kdProfile);
			strukHistoriId.setNoHistori(noHistori);
			strukHistori.setId(strukHistoriId);
			strukHistori.setNoUrutRuangan(noUrutRuangaHistori);
			strukHistori.setNoRec(generateUuid());
			strukHistori.setKdRuangan(kdRuangan);
			strukHistori.setTglAwal(profileHistoriStrategyMapDKPIDto.getTglAwal());
			strukHistori.setTglAkhir(profileHistoriStrategyMapDKPIDto.getTglAkhir());
			strukHistori.setTglHistori(DateUtil.dateNow(DateUtil.DEFAULT_TIMEZONE));
			strukHistori.setStatusEnabled(true);
			strukHistori.setKdKelompokTransaksi(kdKelompokTransaksi);
			strukHistori = strukHistoriDao.save(strukHistori);
			
			dt.put("KdDepartemenD", profileHistoriStrategyMapDKPI.getId().getKdDepartemenD());
			dt.put("KdPerspectiveD", profileHistoriStrategyMapDKPI.getId().getKdPerspectiveD());
			dt.put("KdStrategyD", profileHistoriStrategyMapDKPI.getId().getKdStrategyD());
			result.add(dt);
		}
		if (result.size() > 0) {
			data.put("ProfileHistoriStrategyMapDKPI", result);
			data.put("message", "berhasil.ditambah");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDKPIDaoFindOneBy", "ProfileHistoriStrategyMapDKPIDaoFindAllList", "ProfileHistoriStrategyMapDKPIDaoFindAllListPage",
			"ProfileHistoriStrategyMapDKPIDaoFindByKode", "ProfileHistoriStrategyMapDKPIDaoFindByPeriod", "ProfileHistoriStrategyMapDKPIDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> update(ProfileHistoriStrategyMapDKPIDto profileHistoriStrategyMapDKPIDto, Integer version) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriStrategyMapDKPI profileHistoriStrategyMapDKPI = profileHistoriStrategyMapKPIDao.findOneByIdKdProfileAndIdNoHistori(loginUserService.getSession().getKdProfile(), profileHistoriStrategyMapDKPIDto.getNoHistori());
		if (CommonUtil.isNotNullOrEmpty(profileHistoriStrategyMapDKPI)) {
			String noRec = profileHistoriStrategyMapDKPI.getNoRec();
			BeanUtils.copyProperties(profileHistoriStrategyMapDKPIDto, profileHistoriStrategyMapDKPI);
			profileHistoriStrategyMapDKPI.setKdDepartemen(loginUserService.getSession().getKdDepartemen());
			profileHistoriStrategyMapDKPI.setNoRec(noRec);
			profileHistoriStrategyMapDKPI.setVersion(version);
			profileHistoriStrategyMapDKPI = profileHistoriStrategyMapKPIDao.save(profileHistoriStrategyMapDKPI);
			data.put("noHistori", profileHistoriStrategyMapDKPI.getId().getNoHistori());
			data.put("kdDepartemenD", profileHistoriStrategyMapDKPI.getId().getKdDepartemenD());
			data.put("kdPerspectiveD", profileHistoriStrategyMapDKPI.getId().getKdPerspectiveD());
			data.put("kdStrategyD", profileHistoriStrategyMapDKPI.getId().getKdStrategyD());
			data.put("message", "berhasil.diubah");
		} else {
			data.put("message", "data.tidak.ada");
		}

		return data;
	}

	
	@Transactional(readOnly = false)
	@CacheEvict(value = { "ProfileHistoriStrategyMapDKPIDaoFindOneBy", "ProfileHistoriStrategyMapDKPIDaoFindAllList", "ProfileHistoriStrategyMapDKPIDaoFindAllListPage",
			"ProfileHistoriStrategyMapDKPIDaoFindByKode", "ProfileHistoriStrategyMapDKPIDaoFindByPeriod", "ProfileHistoriStrategyMapDKPIDaoFindByPeriodPage" }, allEntries = true)
	public Map<String, Object> deleteByKode(String noHistori) {
		Map<String, Object> data = CommonUtil.createMap();
		ProfileHistoriStrategyMapDKPI profileHistoriStrategyMapDKPI = profileHistoriStrategyMapKPIDao.findOneByIdKdProfileAndIdNoHistori(loginUserService.getSession().getKdProfile(), noHistori);

		if (CommonUtil.isNotNullOrEmpty(profileHistoriStrategyMapDKPI)) {
			profileHistoriStrategyMapDKPI.setStatusEnabled(false);
			profileHistoriStrategyMapKPIDao.save(profileHistoriStrategyMapDKPI);
			data.put("message", "berhasil.dihapus");
			data.put("noHistori", profileHistoriStrategyMapDKPI.getId().getNoHistori());
			data.put("kdDepartemenD", profileHistoriStrategyMapDKPI.getId().getKdDepartemenD());
			data.put("kdPerspectiveD", profileHistoriStrategyMapDKPI.getId().getKdPerspectiveD());
			data.put("kdStrategyD", profileHistoriStrategyMapDKPI.getId().getKdStrategyD());
		}

		return data;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String kdDepartemenD) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(kdDepartemenD)) {
			pageRes = profileHistoriStrategyMapKPIDao.findAllList(kdProfile, pageReq);
		} else {
			pageRes = profileHistoriStrategyMapKPIDao.findAllList(kdProfile, kdDepartemenD, pageReq);
		}
		result.put("ProfileHistoriStrategyMapDKPI", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> findByPeriod(Integer page, Integer limit, String sort, String dir, Long startDate, Long endDate, String kdDepartemenD) {
		Map<String, Object> result = CommonUtil.createMap();
		Integer kdProfile = loginUserService.getSession().getKdProfile();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		
		if (CommonUtil.isNullOrEmpty(kdDepartemenD)) {
			pageRes = profileHistoriStrategyMapKPIDao.findByPeriod(kdProfile, startDate, endDate, pageReq);
		} else {
			pageRes = profileHistoriStrategyMapKPIDao.findByPeriod(kdProfile, startDate, endDate, kdDepartemenD, pageReq);
		}
		
		result.put("ProfileHistoriStrategyMapDKPI", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	
	@Transactional(readOnly = true)
	public Map<String, Object> findByKode(String noHistori) {
		Map<String, Object> data = CommonUtil.createMap();
		Map<String, Object> profileHistoriStrategyMapDKPI = profileHistoriStrategyMapKPIDao.findByKode(loginUserService.getSession().getKdProfile(), noHistori);
		data.put("ProfileHistoriStrategyMapDKPI", profileHistoriStrategyMapDKPI);
		return data;
	}

}
