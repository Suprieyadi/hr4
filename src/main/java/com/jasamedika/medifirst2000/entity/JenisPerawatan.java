/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/08/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.base.entity.BaseMaster;
import com.jasamedika.medifirst2000.entity.vo.JenisPerawatanId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
@Entity
@Table(name = "JenisPerawatan_M")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor  @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class JenisPerawatan extends BaseMaster {
	@EmbeddedId
	private JenisPerawatanId id;

	 

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "jenisperawatan.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefKdJenisPerawatanHead = TINYINT;
	@Column(name = "KdJenisPerawatanHead", columnDefinition = cDefKdJenisPerawatanHead)
	private Integer kdJenisPerawatanHead;

	private static final String cDefJenisPerawatan = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "JenisPerawatan", columnDefinition = cDefJenisPerawatan)
	@NotNull(message = "jenisperawatan.jenisperawatan.notnull")
	private String namaJenisPerawatan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	//@ForeignKey(name="none")//
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisPerawatanHead", referencedColumnName = "KdJenisPerawatan", insertable = false, updatable = false), })
	//@ForeignKey(name="none")//
	private JenisPerawatan jenisPerawatanHead;
}


