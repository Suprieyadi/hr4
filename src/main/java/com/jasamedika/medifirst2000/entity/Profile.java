package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.AgamaId;
import com.jasamedika.medifirst2000.entity.vo.ProfileId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Version;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;

import java.math.BigDecimal;

@Entity
@Table(name = "Profile_M", indexes = { @Index(name = "Profile_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Profile {

	private static final String cDefKdProfile = SMALLINT;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@Id
	@GeneratedValue
	private Integer kode;

	private static final String cDefNamaLengkap = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaLengkap", columnDefinition = cDefNamaLengkap)
	@NotNull(message = "profile.namalengkap.notnull")
	private String namaLengkap;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "profile.reportDisplay.notnull")
	private String reportDisplay;

	private static final String cDefTglRegistrasi = INTEGER;
	@Column(name = "TglRegistrasi", columnDefinition = cDefTglRegistrasi)
	private Long tglRegistrasi;

	private static final String cDefLuasTanah = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "LuasTanah", columnDefinition = cDefLuasTanah)
	private BigDecimal luasTanah;

	private static final String cDefLuasBangunan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "LuasBangunan", columnDefinition = cDefLuasBangunan)
	private BigDecimal luasBangunan;

	private static final String cDefKdLevelTingkat = TINYINT;
	@Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
	private Integer kdLevelTingkat;

	private static final String cDefKdTahapanAkreditasiLast = TINYINT;
	@Column(name = "KdTahapanAkreditasiLast", columnDefinition = cDefKdTahapanAkreditasiLast)
	private Integer kdTahapanAkreditasiLast;

	private static final String cDefKdStatusAkreditasiLast = TINYINT;
	@Column(name = "KdStatusAkreditasiLast", columnDefinition = cDefKdStatusAkreditasiLast)
	private Integer kdStatusAkreditasiLast;

	private static final String cDefTglAkreditasiLast = INTEGER;
	@Column(name = "TglAkreditasiLast", columnDefinition = cDefTglAkreditasiLast)
	private Long tglAkreditasiLast;

	private static final String cDefKdAlamat = INTEGER;
	@Column(name = "KdAlamat", columnDefinition = cDefKdAlamat)
	private Integer kdAlamat;

	private static final String cDefMottoLast = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "MottoLast", columnDefinition = cDefMottoLast)
	private String mottoLast;

	private static final String cDefSemboyanLast = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "SemboyanLast", columnDefinition = cDefSemboyanLast)
	private String semboyanLast;

	private static final String cDefSloganLast = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "SloganLast", columnDefinition = cDefSloganLast)
	private String sloganLast;

	private static final String cDefTaglineLast = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "TaglineLast", columnDefinition = cDefTaglineLast)
	private String taglineLast;

	private static final String cDefKdPegawaiKepala = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiKepala", columnDefinition = cDefKdPegawaiKepala)
	private String kdPegawaiKepala;

	private static final String cDefMessageToPasien = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG;
	@Column(name = "MessageToPasien", columnDefinition = cDefMessageToPasien)
	private String messageToPasien;

	private static final String cDefGambarLogo = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "GambarLogo", columnDefinition = cDefGambarLogo)
	private String gambarLogo;

	private static final String cDefNPWP = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG;
	@Column(name = "NPWP", columnDefinition = cDefNPWP)
	private String nPWP;

	private static final String cDefNoPKP = VARCHAR + AWAL_KURUNG + 40 + AKHIR_KURUNG;
	@Column(name = "NoPKP", columnDefinition = cDefNoPKP)
	private String noPKP;

	private static final String cDefKdAccount = INTEGER;
	@Column(name = "KdAccount", columnDefinition = cDefKdAccount)
	private Integer kdAccount;

	private static final String cDefKdJenisProfile = TINYINT;
	@Column(name = "KdJenisProfile", columnDefinition = cDefKdJenisProfile)
	@NotNull(message = "profile.kdjenisprofile.notnull")
	private Integer kdJenisProfile;
	
	private static final String cDefNoSuratIjinLast = VARCHAR + AWAL_KURUNG + 20 + AKHIR_KURUNG;
	@Column(name = "NoSuratIjinLast", columnDefinition = cDefNoSuratIjinLast)
	private String noSuratIjinLast;

	private static final String cDefTglSuratIjinLast = INTEGER;
	@Column(name = "TglSuratIjinLast", columnDefinition = cDefTglSuratIjinLast)
	private Long tglSuratIjinLast;

	private static final String cDefSignatureByLast = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "SignatureByLast", columnDefinition = cDefSignatureByLast)
	private String signatureByLast;

	private static final String cDefKdStatusSuratIjinLast = TINYINT;
	@Column(name = "KdStatusSuratIjinLast", columnDefinition = cDefKdStatusSuratIjinLast)
	private Integer kdStatusSuratIjinLast;

	private static final String cDefTglSuratIjinExpiredLast = INTEGER;
	@Column(name = "TglSuratIjinExpiredLast", columnDefinition = cDefTglSuratIjinExpiredLast)
	private Long tglSuratIjinExpiredLast;

	private static final String cDefKdSatuanKerja = TINYINT;
	@Column(name = "KdSatuanKerja", columnDefinition = cDefKdSatuanKerja)
	private Integer kdSatuanKerja;

	private static final String cDefKdJenisTarif = TINYINT;
	@Column(name = "KdJenisTarif", columnDefinition = cDefKdJenisTarif)
	private Integer kdJenisTarif;

	private static final String cDefKdProfileHead = SMALLINT;
	@Column(name = "KdProfileHead", columnDefinition = cDefKdProfileHead)
	private Integer kdProfileHead;

	private static final String cDefKdNegara = TINYINT;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	private Integer kdNegara;

	private static final String cDefKdMataUang = TINYINT;
	@Column(name = "KdMataUang", columnDefinition = cDefKdMataUang)
	private Integer kdMataUang;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	private String kdDepartemen;

	private static final String cDefKdDokumenSejarah = INTEGER;
	@Column(name = "KdDokumenSejarah", columnDefinition = cDefKdDokumenSejarah)
	private Integer kdDokumenSejarah;

	private static final String cDefTglDaftar = INTEGER;
	@Column(name = "TglDaftar", columnDefinition = cDefTglDaftar)
	private Long tglDaftar;


	private static final String cDefHostname1 = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "Hostname1", columnDefinition = cDefHostname1)
	private String hostname1;
	
	private static final String cDefHostname2 = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "Hostname2", columnDefinition = cDefHostname2)
	private String hostname2; 
	
	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "NoRec Harus Diisi")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;

	

}
