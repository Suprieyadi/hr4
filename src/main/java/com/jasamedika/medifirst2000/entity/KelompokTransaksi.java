package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "KelompokTransaksi_M",indexes = {@Index(name = "KelompokTransaksi_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class KelompokTransaksi {
    
	private static final String cDefKdKelompokTransaksi = TINYINT;
	@Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
	@NotNull(message = "kelompoktransaksiid.kdkelompoktransaksi.notnull")
	@Id
	private Integer kdKelompokTransaksi;
    
    private static final String cDefKelompokTransaksi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "KelompokTransaksi", columnDefinition = cDefKelompokTransaksi)
    @NotNull(message = "kelompoktransaksi.kelompoktransaksi.notnull")
    private String namaKelompokTransaksi;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "kelompoktransaksi.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefisCostInOut =TINYINT;
    @Column(name = "isCostInOut", columnDefinition = cDefisCostInOut)
    private Boolean isCostInOut;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "kelompoktransaksi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "kelompoktransaksi.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "kelompoktransaksi.version.notnull")
    private Integer version;
    

}
