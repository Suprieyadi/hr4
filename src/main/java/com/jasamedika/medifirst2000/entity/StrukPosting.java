package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.StrukPostingId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "StrukPosting_T",
	indexes = {
			@Index(name = "StrukPosting_T_Index1",  
	  			columnList="TglPosting, NoUrutLogin, NoUrutRuangan, KdKelompokTransaksi", unique = false),
			@Index(name = "StrukPosting_T_Index2",  
				columnList="TglAwal", unique = false),
			@Index(name = "StrukPosting_T_Index3",  
				columnList="TglAkhir", unique = false),
			@Index(name = "StrukPosting_T_Index4",  
				columnList="TglPostingExpired", unique = false),			
			@Index(name = "StrukPosting_T_Index5",  
				columnList="statusEnabled", unique = false) 
			}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukPosting {
    
    @EmbeddedId
    private StrukPostingId id ;
    
    private static final String cDefTglPosting = INTEGER ;
    @Column(name = "TglPosting", columnDefinition = cDefTglPosting)
    @NotNull(message = "strukposting.tglposting.notnull")
    private Long  tglPosting;
    
    private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuangan", columnDefinition = cDefKdRuangan)
    @NotNull(message = "strukposting.kdruangan.notnull")
    private String kdRuangan;
    
    private static final String cDefTglAwal = INTEGER ;
    @Column(name = "TglAwal", columnDefinition = cDefTglAwal)
    private Long  tglAwal;
    
    private static final String cDefTglAkhir = INTEGER ;
    @Column(name = "TglAkhir", columnDefinition = cDefTglAkhir)
    private Long  tglAkhir;
    
    private static final String cDefKdRuanganTujuan = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuanganTujuan", columnDefinition = cDefKdRuanganTujuan)
    private String kdRuanganTujuan;
    
    private static final String cDefTglPostingExpired = INTEGER ;
    @Column(name = "TglPostingExpired", columnDefinition = cDefTglPostingExpired)
    private Long  tglPostingExpired;
    
    private static final String cDefKdJenisOrder =TINYINT;
    @Column(name = "KdJenisOrder", columnDefinition = cDefKdJenisOrder)
    private Integer kdJenisOrder;
    
    private static final String cDefKdPegawaiPosting = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawaiPosting", columnDefinition = cDefKdPegawaiPosting)
    private String kdPegawaiPosting;
    
    private static final String cDefKdPegawaiTujuan = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawaiTujuan", columnDefinition = cDefKdPegawaiTujuan)
    private String kdPegawaiTujuan;
    
    private static final String cDefKeteranganPosting = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganPosting", columnDefinition = cDefKeteranganPosting)
    private String keteranganPosting;
    
    private static final String cDefKdKelompokTransaksi =TINYINT;
    @Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
    @NotNull(message = "strukposting.kdkelompoktransaksi.notnull")
    private Integer kdKelompokTransaksi;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukposting.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoUrutLogin = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutLogin", columnDefinition = cDefNoUrutLogin)
    private String noUrutLogin;
    
    private static final String cDefNoUrutRuangan = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutRuangan", columnDefinition = cDefNoUrutRuangan)
    private String noUrutRuangan;
    
    private static final String cDefNoPostingBefore = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoPostingBefore", columnDefinition = cDefNoPostingBefore)
    private String noPostingBefore;
    
    private static final String cDefKdLevelTingkat =TINYINT;
    @Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
    private Integer kdLevelTingkat;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukposting.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukposting.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoPosting", referencedColumnName="NoPosting",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukPosting strukPosting;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdRuangan", referencedColumnName="KdRuangan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Ruangan ruangan;
    
  /*  @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdJenisOrder", referencedColumnName="KdJenisOrder",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private JenisOrder jenisOrder;
    */
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdKelompokTransaksi", referencedColumnName="KdKelompokTransaksi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KelompokTransaksi kelompokTransaksi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdLevelTingkat", referencedColumnName="KdLevelTingkat",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private LevelTingkat levelTingkat;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
            @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
            @JoinColumn(name="KdRuanganTujuan", referencedColumnName="KdRuangan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
    private Ruangan ruanganTujuan;
}
