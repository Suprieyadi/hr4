package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.FLOAT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.PegawaiPostingKPIId;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapDKPIId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PegawaiPostingKPI_T",indexes = {@Index(name = "PegawaiPostingKPI_T_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PegawaiPostingKPI {
	
	@EmbeddedId
	private PegawaiPostingKPIId id;
	
	private static final String cDefBobotKPI = FLOAT ;
    @Column(name = "BobotKPI", columnDefinition = cDefBobotKPI)
    @NotNull(message = "pegawaipostingkpi.bobotkpi.notnull")
    private Float bobotKPI;
    
    private static final String cDefTargetKPI = FLOAT ;
    @Column(name = "TargetKPI", columnDefinition = cDefTargetKPI)
    @NotNull(message = "pegawaipostingkpi.targetkpi.notnull")
    private Float targetKPI;
    
    private static final String cDefKdMetodeHitungActual =TINYINT;
    @Column(name = "KdMetodeHitungActual", columnDefinition = cDefKdMetodeHitungActual)
    @NotNull(message = "pegawaipostingkpi.kdmetodehitungactual.notnull")
    private Integer kdMetodeHitungActual;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefNoRetur = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoRetur", columnDefinition = cDefNoRetur)
    private String noRetur;
    
    private static final String cDefNoVerifikasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoVerifikasi", columnDefinition = cDefNoVerifikasi)
    private String noVerifikasi;
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK", columnDefinition = cDefNoSK)
    private String noSK;
    
    private static final String cDefKdKPISK =SMALLINT;
    @Column(name = "KdKPISK",   columnDefinition = cDefKdKPISK)
    private Integer kdKPISK;
    
    private static final String cDefBobotKPISK = FLOAT ;
    @Column(name = "BobotKPISK", columnDefinition = cDefBobotKPISK)
    private Float bobotKPISK;
    
    private static final String cDefTargetKPIMinSK = FLOAT ;
    @Column(name = "TargetKPIMinSK", columnDefinition = cDefTargetKPIMinSK)
    private Float targetKPIMinSK;
    
    private static final String cDefTargetKPIMaxSK = FLOAT ;
    @Column(name = "TargetKPIMaxSK", columnDefinition = cDefTargetKPIMaxSK)
    private Float targetKPIMaxSK;
    
    private static final String cDefKdMetodeHitungActualSK =TINYINT;
    @Column(name = "KdMetodeHitungActualSK", columnDefinition = cDefKdMetodeHitungActualSK)
    private Integer kdMetodeHitungActualSK;
    
    private static final String cDefKdPeriodeDataSK =TINYINT;
    @Column(name = "KdPeriodeDataSK", columnDefinition = cDefKdPeriodeDataSK)
    private Integer kdPeriodeDataSK;
    
    private static final String cDefNoHistoriActual = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoHistoriActual",   columnDefinition = cDefNoHistoriActual)
    private String noHistoriActual;
    
    private static final String cDefTotalActualKPI = FLOAT ;
    @Column(name = "TotalActualKPI", columnDefinition = cDefTotalActualKPI)
    private Float totalActualKPI;
    
    private static final String cDefTotalScoreKPI = FLOAT ;
    @Column(name = "TotalScoreKPI", columnDefinition = cDefTotalScoreKPI)
    private Float totalScoreKPI;
    
    private static final String cDefNoVerifikasiActual = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoVerifikasiActual", columnDefinition = cDefNoVerifikasiActual)
    private String noVerifikasiActual;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "profilehistoristrategymapdkpi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    private Integer version;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoPosting", referencedColumnName="NoPosting",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukPosting strukPosting;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPegawai", referencedColumnName="KdPegawai",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Pegawai pegawai;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoHistori", referencedColumnName="NoHistori",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukHistori strukHistori;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemenD", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemenD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPerspectiveD", referencedColumnName="KdPerspective",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Perspective perspectiveD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrategyD", referencedColumnName="KdStrategy",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Strategy strategyD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKPI", referencedColumnName="KdKPI",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KPI kpi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdMetodeHitungActual", referencedColumnName="KdMetodeHitung",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private MetodePerhitungan metodePerhitungan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoRetur", referencedColumnName="NoRetur",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukRetur strukRetur;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoVerifikasi", referencedColumnName="NoVerifikasi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukVerifikasi strukVerifikasi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoSK", referencedColumnName="NoSK",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private SuratKeputusan suratKeputusan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKPISK", referencedColumnName="KdKPI",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KPI kpiSK;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdMetodeHitungActualSK", referencedColumnName="KdMetodeHitung",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private MetodePerhitungan metodePerhitunganSK;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPeriodeDataSK", referencedColumnName="KdPeriode",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Periode periodeSK;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoHistoriActual", referencedColumnName="NoHistori",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukHistori strukHistoriActual;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoVerifikasiActual", referencedColumnName="NoVerifikasi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukVerifikasi strukVerifikasiActual;

}
