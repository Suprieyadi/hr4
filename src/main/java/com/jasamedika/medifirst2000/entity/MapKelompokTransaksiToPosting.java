package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.MapKelompokTransaksiToPostingId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;

@Entity
@Table(name = "MapKelompokTransaksiToPosting_M", indexes = {
		@Index(name = "MapKelompokTransaksiToPosting_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MapKelompokTransaksiToPosting {

	@EmbeddedId
	private MapKelompokTransaksiToPostingId id;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "mapkelompoktransaksitoposting.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "mapkelompoktransaksitoposting.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "mapkelompoktransaksitoposting.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdKelompokTransaksi", referencedColumnName = "KdKelompokTransaksi", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KelompokTransaksi kelompokTransaksi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRuanganTujuan", referencedColumnName = "KdRuangan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private Ruangan ruanganTujuan;
}
