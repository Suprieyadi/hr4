package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.FLOAT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapDKPIId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ProfileHistoriStrategyMapDKPI_M",indexes = {@Index(name = "ProfileHistoriStrategyMapDKPI_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProfileHistoriStrategyMapDKPI {
	
	@EmbeddedId
    private ProfileHistoriStrategyMapDKPIId id ;
	
	private static final String cDefKdPerspective =TINYINT;
    @Column(name = "KdPerspective", columnDefinition = cDefKdPerspective)
    @NotNull(message = "profilehistoristrategymapdkpi.kdperspective.notnull")
    private Integer kdPerspective;
    
    private static final String cDefKdStrategy =SMALLINT;
    @Column(name = "KdStrategy",   columnDefinition = cDefKdStrategy)
    @NotNull(message = "profilehistoristrategymapdkpi.kdstrategy.notnull")
    private Integer kdStrategy;
    
    private static final String cDefBobotKPI = FLOAT ;
    @Column(name = "BobotKPI", columnDefinition = cDefBobotKPI)
    @NotNull(message = "profilehistoristrategymapdkpi.bobotkpi.notnull")
    private Float bobotKPI;
    
    private static final String cDefTargetKPIMin = FLOAT ;
    @Column(name = "TargetKPIMin", columnDefinition = cDefTargetKPIMin)
    @NotNull(message = "profilehistoristrategymapdkpi.targetkpimin.notnull")
    private Float targetKPIMin;
    
    private static final String cDefTargetKPIMax = FLOAT ;
    @Column(name = "TargetKPIMax", columnDefinition = cDefTargetKPIMax)
    private Float targetKPIMax;
    
    private static final String cDefKdMetodeHitungActual =TINYINT;
    @Column(name = "KdMetodeHitungActual", columnDefinition = cDefKdMetodeHitungActual)
    @NotNull(message = "profilehistoristrategymapdkpi.kdmetodehitungactual.notnull")
    private Integer kdMetodeHitungActual;
    
    private static final String cDefKdAsalData =TINYINT;
    @Column(name = "KdAsalData", columnDefinition = cDefKdAsalData)
    private Integer kdAsalData;
    
    private static final String cDefKdPeriodeData =TINYINT;
    @Column(name = "KdPeriodeData", columnDefinition = cDefKdPeriodeData)
    private Integer kdPeriodeData;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "profilehistoristrategymapdkpi.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "profilehistoristrategymapdkpi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    private Integer version;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoHistori", referencedColumnName="NoHistori",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukHistori strukHistori;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPerspective", referencedColumnName="KdPerspective",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Perspective perspective;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrategy", referencedColumnName="KdStrategy",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Strategy strategy;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemenD", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemenD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPerspectiveD", referencedColumnName="KdPerspective",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Perspective perspectiveD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrategyD", referencedColumnName="KdStrategy",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Strategy strategyD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKPI", referencedColumnName="KdKPI",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KPI kpi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdMetodeHitungActual", referencedColumnName="KdMetodeHitung",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private MetodePerhitungan metodePerhitungan;


}
