/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 26/03/2018
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.ProfileSKHargaPoinId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ProfileSKHargaPoin_M", indexes = {
		@Index(name = "ProfileSKHargaPoin_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProfileSKHargaPoin {

	@EmbeddedId
	private ProfileSKHargaPoinId id;

	private static final String cDefQtyPoin = FLOAT;
	@Column(name = "QtyPoin", columnDefinition = cDefQtyPoin)
	@NotNull(message = "profileskhargapoin.qtypoin.notnull")
	private Float qtyPoin;

	private static final String cDefHargaSatuan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "HargaSatuan", columnDefinition = cDefHargaSatuan)
	@NotNull(message = "profileskhargapoin.hargasatuan.notnull")
	private Double hargaSatuan;

	private static final String cDefKdMataUang = TINYINT;
	@Column(name = "KdMataUang", columnDefinition = cDefKdMataUang)
	@NotNull(message = "profileskhargapoin.kdmatauang.notnull")
	private Integer kdMataUang;

	private static final String cDefFactorRate = FLOAT;
	@Column(name = "FactorRate", columnDefinition = cDefFactorRate)
	@NotNull(message = "profileskhargapoin.factorrate.notnull")
	private Float factorRate;

	private static final String cDefOperatorFactorRate = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "OperatorFactorRate", columnDefinition = cDefOperatorFactorRate)
	@NotNull(message = "profileskhargapoin.operatorfactorrate.notnull")
	private String operatorFactorRate;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "profileskhargapoin.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "profileskhargapoin.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "profileskhargapoin.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoSK", referencedColumnName = "NoSK", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private SuratKeputusan suratKeputusan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKategoryPegawai", referencedColumnName = "KdKategoryPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KategoryPegawai kategoryPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganPegawai", referencedColumnName = "KdGolonganPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private GolonganPegawai golonganPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKomponenHarga", referencedColumnName = "KdKomponen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Komponen komponen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPangkat", referencedColumnName = "KdPangkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pangkat pangkat;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdMataUang", referencedColumnName = "KdMataUang", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private MataUang mataUang;
}
