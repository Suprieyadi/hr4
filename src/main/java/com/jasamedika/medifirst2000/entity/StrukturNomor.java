package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.StrukturNomorId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "StrukturNomor_M", 
indexes = {
  		@Index(name = "StrukturNomor_M_Index1",  
  			columnList="KdProfile, isDefault, StrukturNomor", unique = false),
		@Index(name = "StrukturNomor_M_Index2",  
			columnList="StatusEnabled", unique = false) 
  		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukturNomor {
    
    @EmbeddedId
    private StrukturNomorId id ;
    
    private static final String cDefStrukturNomor = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "StrukturNomor", columnDefinition = cDefStrukturNomor)
    @NotNull(message = "strukturnomor.strukturnomor.notnull")
    private String namaStrukturNomor;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "strukturnomor.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefDeskripsiDetail = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "DeskripsiDetail", columnDefinition = cDefDeskripsiDetail)
    private String deskripsiDetail;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    private String kdDepartemen;
    
    private static final String cDefisDefault =TINYINT;
    @Column(name = "isDefault", columnDefinition = cDefisDefault)
    @NotNull(message = "strukturnomor.isdefault.notnull")
    private Integer isDefault;
    
    private static final String cDefQtyDigitNomor =TINYINT;
    @Column(name = "QtyDigitNomor", columnDefinition = cDefQtyDigitNomor)
    @NotNull(message = "strukturnomor.qtydigitnomor.notnull")
    private Integer qtyDigitNomor;
    
    private static final String cDefFormatNomor = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "FormatNomor", columnDefinition = cDefFormatNomor)
    @NotNull(message = "strukturnomor.formatnomor.notnull")
    private String formatNomor;
    
    private static final String cDefisAutoIncrement =TINYINT;
    @Column(name = "isAutoIncrement", columnDefinition = cDefisAutoIncrement)
    @NotNull(message = "strukturnomor.isautoincrement.notnull")
    private Integer isAutoIncrement;
    
    private static final String cDefKdStrukturNomorHead =TINYINT;
    @Column(name = "KdStrukturNomorHead", columnDefinition = cDefKdStrukturNomorHead)
    private Integer kdStrukturNomorHead;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukturnomor.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukturnomor.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukturnomor.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrukturNomorHead", referencedColumnName="KdStrukturNomor",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukturNomor strukturNomorHead;
}
