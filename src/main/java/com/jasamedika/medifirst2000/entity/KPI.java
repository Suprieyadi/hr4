package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.KPIId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "KPI_M",indexes = {@Index(name = "KPI_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class KPI {
	
	@EmbeddedId
	private KPIId id;
	
	private static final String cDefNamaKPI = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "NamaKPI", columnDefinition = cDefNamaKPI)
    @NotNull(message = "kpi.namaKPI.notnull")
    private String namaKPI;
    
    private static final String cDefKdKPIHead = SMALLINT;
    @Column(name = "KdKPIHead", columnDefinition = cDefKdKPIHead)
    private Integer kdKPIHead;
    
    private static final String cDefNoUrut = SMALLINT;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    private Integer noUrut;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    private String kdDepartemen;
    
    private static final String cDefKdTypeDataObjek = SMALLINT;
    @Column(name = "KdTypeDataObjek", columnDefinition = cDefKdTypeDataObjek)
    @NotNull(message = "kpi.kdtypedataobjek.notnull")
    private Integer kdTypeDataObjek;
    
    private static final String cDefKdSatuanHasil = TINYINT;
    @Column(name = "KdSatuanHasil", columnDefinition = cDefKdSatuanHasil)
    @NotNull(message = "kpi.kdsatuanhasil.notnull")
    private Integer kdSatuanHasil;
    
    private static final String cDefKdMetodeHitungScore =TINYINT;
    @Column(name = "KdMetodeHitungScore", columnDefinition = cDefKdMetodeHitungScore)
    @NotNull(message = "kpi.kdmetodehitungscore.notnull")
    private Integer kdMetodeHitungScore;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "kpi.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "kpi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "kpi.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "kpi.version.notnull")
    private Integer version;
    
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdTypeDataObjek", referencedColumnName="KdTypeDataObjek",insertable=false, updatable=false),
    })
     private TypeDataObjek typeDataObjek;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdSatuanHasil", referencedColumnName="KdSatuanHasil",insertable=false, updatable=false),
    })
     private SatuanHasil satuanHasil;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdMetodeHitungScore", referencedColumnName="KdMetodeHitung",insertable=false, updatable=false),
    })
     private MetodePerhitungan metodePerhitungan;

}
