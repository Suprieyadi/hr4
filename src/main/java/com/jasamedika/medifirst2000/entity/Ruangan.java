/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 31/10/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.RuanganId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Ruangan_M", indexes = { @Index(name = "Ruangan_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Ruangan {

	@EmbeddedId
	private RuanganId id;

	private static final String cDefNamaRuangan = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaRuangan", columnDefinition = cDefNamaRuangan)
	@NotNull(message = "ruangan.namaruangan.notnull")
	private String namaRuangan;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "ruangan.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefNoRuangan = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG;
	@Column(name = "NoRuangan", columnDefinition = cDefNoRuangan)
	private String noRuangan;

	private static final String cDefLokasiRuangan = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "LokasiRuangan", columnDefinition = cDefLokasiRuangan)
	private String lokasiRuangan;

	private static final String cDefKdKelasHead = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdKelasHead", columnDefinition = cDefKdKelasHead)
	private String kdKelasHead;

	private static final String cDefStatusViewData = BIT;
	@Column(name = "StatusViewData", columnDefinition = cDefStatusViewData)
	private Boolean statusViewData;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "ruangan.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefKdAlamat = INTEGER;
	@Column(name = "KdAlamat", columnDefinition = cDefKdAlamat)
	private Integer kdAlamat;

	/*
	 * private static final String cDefKdModulAplikasi = VARCHAR + AWAL_KURUNG + 2 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "KdModulAplikasi", columnDefinition = cDefKdModulAplikasi)
	 * private String kdModulAplikasi;
	 * 
	 * private static final String cDefFixedPhone = VARCHAR + AWAL_KURUNG + 30 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "FixedPhone", columnDefinition = cDefFixedPhone) private
	 * String fixedPhone;
	 * 
	 * private static final String cDefMobilePhone = VARCHAR + AWAL_KURUNG + 30 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "MobilePhone", columnDefinition = cDefMobilePhone) private
	 * String mobilePhone;
	 * 
	 * private static final String cDefFaksimile = VARCHAR + AWAL_KURUNG + 30 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "Faksimile", columnDefinition = cDefFaksimile) private String
	 * faksimile;
	 * 
	 * private static final String cDefAlamatEmail = VARCHAR + AWAL_KURUNG + 50 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "AlamatEmail", columnDefinition = cDefAlamatEmail) private
	 * String alamatEmail;
	 * 
	 * private static final String cDefWebsite = VARCHAR + AWAL_KURUNG + 80 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "Website", columnDefinition = cDefWebsite) private String
	 * website;
	 */

	private static final String cDefKdPegawaiKepala = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiKepala", columnDefinition = cDefKdPegawaiKepala)
	private String kdPegawaiKepala;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefNoCounter = INTEGER;
	@Column(name = "NoCounter", columnDefinition = cDefNoCounter)
	private Integer noCounter;

	private static final String cDefPrefixNoAntrian = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG;
	@Column(name = "PrefixNoAntrian", columnDefinition = cDefPrefixNoAntrian)
	private String prefixNoAntrian;

	private static final String cDefJamBuka = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "JamBuka", columnDefinition = cDefJamBuka)
	private String jamBuka;

	private static final String cDefJamTutup = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "JamTutup", columnDefinition = cDefJamTutup)
	private String jamTutup;

	private static final String cDefQtyPasienMax = SMALLINT;
	@Column(name = "QtyPasienMax", columnDefinition = cDefQtyPasienMax)
	private Integer qtyPasienMax;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "ruangan.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "ruangan.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "ruangan.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKelasHead", referencedColumnName = "KdKelas", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Kelas kelasHead;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			// @JoinColumn(name="KdProfile",
			// referencedColumnName="KdProfile",insertable=false, updatable=false),
			@JoinColumn(name = "KdModulAplikasi", referencedColumnName = "KdModulAplikasi", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private ModulAplikasi modulAplikasi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiKepala", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pegawai pegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdAlamat", referencedColumnName = "KdAlamat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Alamat alamat;
}
