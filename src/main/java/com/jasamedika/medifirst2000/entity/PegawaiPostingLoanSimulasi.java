package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.BIGDECIMAL;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.BIGINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.PegawaiPostingLoanSimulasiId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PegawaiPostingLoanSimulasi_T",indexes = {@Index(name = "PegawaiPostingLoanSimulasi_T_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PegawaiPostingLoanSimulasi {
	
	@EmbeddedId
    private PegawaiPostingLoanSimulasiId id ;
	
	private static final String cDefHargaSatuanCicilanBunga = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanBunga", columnDefinition = cDefHargaSatuanCicilanBunga)
    @NotNull(message = "pegawaipostingloansimulasi.hargasatuancicilanbunga.notnull")
    private java.math.BigDecimal hargaSatuanCicilanBunga;
    
    private static final String cDefHargaSatuanCicilanBungaSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanBungaSK", columnDefinition = cDefHargaSatuanCicilanBungaSK)
    private java.math.BigDecimal hargaSatuanCicilanBungaSK;
    
    private static final String cDefHargaSatuanCicilanPokok = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanPokok", columnDefinition = cDefHargaSatuanCicilanPokok)
    @NotNull(message = "pegawaipostingloansimulasi.hargasatuancicilanpokok.notnull")
    private java.math.BigDecimal hargaSatuanCicilanPokok;
    
    private static final String cDefHargaSatuanCicilanPokokSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanPokokSK", columnDefinition = cDefHargaSatuanCicilanPokokSK)
    private java.math.BigDecimal hargaSatuanCicilanPokokSK;
    
    private static final String cDefHargaSatuanCicilanTotal = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanTotal", columnDefinition = cDefHargaSatuanCicilanTotal)
    @NotNull(message = "pegawaipostingloansimulasi.hargasatuancicilantotal.notnull")
    private java.math.BigDecimal hargaSatuanCicilanTotal;
    
    private static final String cDefHargaSatuanCicilanTotalSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanTotalSK", columnDefinition = cDefHargaSatuanCicilanTotalSK)
    private java.math.BigDecimal hargaSatuanCicilanTotalSK;
    
    private static final String cDefNoRetur = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoRetur", columnDefinition = cDefNoRetur)
    private String noRetur;
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK", columnDefinition = cDefNoSK)
    private String noSK;
    
    private static final String cDefNoStruk = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoStruk", columnDefinition = cDefNoStruk)
    private String noStruk;
    
    private static final String cDefNoVerifikasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoVerifikasi", columnDefinition = cDefNoVerifikasi)
    private String noVerifikasi;
    
    private static final String cDefTglAkhirBayarCicilan = BIGINT ;
    @Column(name = "TglAkhirBayarCicilan", columnDefinition = cDefTglAkhirBayarCicilan)
    @NotNull(message = "pegawaipostingloansimulasi.tglakhirbayarcicilan.notnull")
    private Long  tglAkhirBayarCicilan;
    
    private static final String cDefTglAkhirBayarCicilanSK = BIGINT ;
    @Column(name = "TglAkhirBayarCicilanSK", columnDefinition = cDefTglAkhirBayarCicilanSK)
    private Long  tglAkhirBayarCicilanSK;
    
    private static final String cDefTglAwalBayarCicilan = BIGINT ;
    @Column(name = "TglAwalBayarCicilan", columnDefinition = cDefTglAwalBayarCicilan)
    @NotNull(message = "pegawaipostingloansimulasi.tglawalbayarcicilan.notnull")
    private Long  tglAwalBayarCicilan;
    
    private static final String cDefTglAwalBayarCicilanSK = BIGINT ;
    @Column(name = "TglAwalBayarCicilanSK", columnDefinition = cDefTglAwalBayarCicilanSK)
    private Long  tglAwalBayarCicilanSK;
    
    private static final String cDefTglJatuhTempo = BIGINT ;
    @Column(name = "TglJatuhTempo", columnDefinition = cDefTglJatuhTempo)
    @NotNull(message = "pegawaipostingloansimulasi.tgljatuhtempo.notnull")
    private Long  tglJatuhTempo;
    
    private static final String cDefTglJatuhTempoSK = BIGINT ;
    @Column(name = "TglJatuhTempoSK", columnDefinition = cDefTglJatuhTempoSK)
    private Long  tglJatuhTempoSK;
    
    private static final String cDefTotalHargaSatuan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuan", columnDefinition = cDefTotalHargaSatuan)
    @NotNull(message = "pegawaipostingloansimulasi.totalhargasatuan.notnull")
    private java.math.BigDecimal totalHargaSatuan;
    
    private static final String cDefTotalHargaSatuanSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuanSK", columnDefinition = cDefTotalHargaSatuanSK)
    private java.math.BigDecimal totalHargaSatuanSK;
    
    private static final String cDefNoPosting_R = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoPosting_R", columnDefinition = cDefNoPosting_R)
    private String noPostingR;
    
    private static final String cDefNoSK_R = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK_R", columnDefinition = cDefNoSK_R)
    private String noSKR;
    
    private static final String cDefNoSKClosed = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSKClosed", columnDefinition = cDefNoSKClosed)
    private String noSKClosed;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "pegawaipostingloansimulasi.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "pegawaipostingloansimulasi.version.notnull")
    private Integer version;
        
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "pegawaipostingloansimulasi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoPosting", referencedColumnName="NoPosting",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukPosting strukPosting;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPegawai", referencedColumnName="KdPegawai",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Pegawai pegawai;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdProduk", referencedColumnName="KdProduk",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Produk produk;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoRetur", referencedColumnName="NoRetur",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukRetur strukRetur;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoVerifikasi", referencedColumnName="NoVerifikasi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukVerifikasi strukVerifikasi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoSK", referencedColumnName="NoSK",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private SuratKeputusan suratKeputusan;

}
