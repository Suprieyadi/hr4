package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.ZodiakId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "Zodiak_M",indexes = {@Index(name = "Zodiak_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Zodiak {
    
    @EmbeddedId
    private ZodiakId id ;
    
    private static final String cDefNamaZodiak = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "NamaZodiak", columnDefinition = cDefNamaZodiak)
    @NotNull(message = "zodiak.namazodiak.notnull")
    private String namaZodiak;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "zodiak.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefNoUrut =TINYINT;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    @NotNull(message = "zodiak.nourut.notnull")
    private Integer noUrut;
    
    private static final String cDefJamLahirAwal = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "JamLahirAwal", columnDefinition = cDefJamLahirAwal)
    private String jamLahirAwal;
    
    private static final String cDefJamLahirAkhir = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "JamLahirAkhir", columnDefinition = cDefJamLahirAkhir)
    private String jamLahirAkhir;
    
    private static final String cDefKdNegara =TINYINT;
    @Column(name = "KdNegara", columnDefinition = cDefKdNegara)
    private Integer kdNegara;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "zodiak.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "zodiak.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "zodiak.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "zodiak.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdNegara", referencedColumnName="KdNegara",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Negara negara;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
}
