package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.BIGDECIMAL;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.REAL;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.jasamedika.medifirst2000.entity.vo.StrukHistoriId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "StrukHistori_T", 
indexes = {
  		@Index(name = "StrukHistori_T_Index1",  
  			columnList="TglHistori, NoUrutLogin, NoUrutRuangan, KdKelompokTransaksi", unique = false),
  		@Index(name = "StrukHistori_T_Index2",  
			columnList="TglAwal", unique = false),
  		@Index(name = "StrukHistori_T_Index3",  
  			columnList="TglAkhir", unique = false),
  		@Index(name = "StrukHistori_T_Index4",  
			columnList="TglBerlakuAwal", unique = false),
  		@Index(name = "StrukHistori_T_Index5",  
  			columnList="noSK", unique = false),
		@Index(name = "StrukHistori_T_Index6",  
			columnList="StatusEnabled", unique = false) 
  		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StrukHistori {
	
	@EmbeddedId
	private StrukHistoriId id;

	private static final String cDefTglHistori = INTEGER;
	@Column(name = "TglHistori", columnDefinition = cDefTglHistori)
	@NotNull(message = "strukhistori.tglhistori.notnull")
	private Long tglHistori;

	private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG;
	@Column(name = "KdRuangan", columnDefinition = cDefKdRuangan)
	@NotNull(message = "strukhistori.kdruangan.notnull")
	private String kdRuangan;

	private static final String cDefTglAwal = INTEGER;
	@Column(name = "TglAwal", columnDefinition = cDefTglAwal)
	private Long tglAwal;

	private static final String cDefTglAkhir = INTEGER;
	@Column(name = "TglAkhir", columnDefinition = cDefTglAkhir)
	private Long tglAkhir;

	private static final String cDefKdBankAccount = INTEGER;
	@Column(name = "KdBankAccount", columnDefinition = cDefKdBankAccount)
	private Integer kdBankAccount;

	private static final String cDefTotalSetorTarikDeposit = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "TotalSetorTarikDeposit", columnDefinition = cDefTotalSetorTarikDeposit)
	private java.math.BigDecimal totalSetorTarikDeposit;

	private static final String cDefTotalAdminSetorTarikDeposit = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "TotalAdminSetorTarikDeposit", columnDefinition = cDefTotalAdminSetorTarikDeposit)
	private java.math.BigDecimal totalAdminSetorTarikDeposit;

	private static final String cDefTglSetorTarikDeposit = INTEGER;
	@Column(name = "TglSetorTarikDeposit", columnDefinition = cDefTglSetorTarikDeposit)
	private Long tglSetorTarikDeposit;

	private static final String cDefKdPegawaiSetorTarikDeposit = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiSetorTarikDeposit", columnDefinition = cDefKdPegawaiSetorTarikDeposit)
	private String kdPegawaiSetorTarikDeposit;

	private static final String cDefNamaPegawaiSetorTarikDeposit = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "NamaPegawaiSetorTarikDeposit", columnDefinition = cDefNamaPegawaiSetorTarikDeposit)
	private String namaPegawaiSetorTarikDeposit;

	private static final String cDefSetoranTarikanDepositKe = TINYINT;
	@Column(name = "SetoranTarikanDepositKe", columnDefinition = cDefSetoranTarikanDepositKe)
	private Integer setoranTarikanDepositKe;

	private static final String cDefKdPegawaiTerima = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiTerima", columnDefinition = cDefKdPegawaiTerima)
	private String kdPegawaiTerima;

	private static final String cDefNamaPegawaiTerima = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "NamaPegawaiTerima", columnDefinition = cDefNamaPegawaiTerima)
	private String namaPegawaiTerima;

	private static final String cDefKdRuanganTerima = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG;
	@Column(name = "KdRuanganTerima", columnDefinition = cDefKdRuanganTerima)
	private String kdRuanganTerima;

	private static final String cDefKdProdukHasil = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG;
	@Column(name = "KdProdukHasil", columnDefinition = cDefKdProdukHasil)
	private String kdProdukHasil;

	private static final String cDefNoStrukTextHasil = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoStrukTextHasil", columnDefinition = cDefNoStrukTextHasil)
	private String noStrukTextHasil;

	private static final String cDefKdAsalProdukHasil = TINYINT;
	@Column(name = "KdAsalProdukHasil", columnDefinition = cDefKdAsalProdukHasil)
	private Integer kdAsalProdukHasil;

	private static final String cDefTglBerlakuAwal = INTEGER;
	@Column(name = "TglBerlakuAwal", columnDefinition = cDefTglBerlakuAwal)
	private Long tglBerlakuAwal;

	private static final String cDefKeteranganAlasan = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG;
	@Column(name = "KeteranganAlasan", columnDefinition = cDefKeteranganAlasan)
	private String keteranganAlasan;

	private static final String cDefRangkumanHasilEvaluasi = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG;
	@Column(name = "RangkumanHasilEvaluasi", columnDefinition = cDefRangkumanHasilEvaluasi)
	private String rangkumanHasilEvaluasi;

	private static final String cDefTotalNilaiHasilEvaluasi = REAL;
	@Column(name = "TotalNilaiHasilEvaluasi", columnDefinition = cDefTotalNilaiHasilEvaluasi)
	private Float totalNilaiHasilEvaluasi;

	private static final String cDefKdRangeTotalNilaiHasilEvaluasi = TINYINT;
	@Column(name = "KdRangeTotalNilaiHasilEvaluasi", columnDefinition = cDefKdRangeTotalNilaiHasilEvaluasi)
	private Integer kdRangeTotalNilaiHasilEvaluasi;

	private static final String cDefRangkumanMemoRekomendasi = VARCHAR + AWAL_KURUNG + 500 + AKHIR_KURUNG;
	@Column(name = "RangkumanMemoRekomendasi", columnDefinition = cDefRangkumanMemoRekomendasi)
	private String rangkumanMemoRekomendasi;

	private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG;
	@Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
	private String keteranganLainnya;

	private static final String cDefNoClosing = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoClosing", columnDefinition = cDefNoClosing)
	private String noClosing;

	private static final String cDefNoVerifikasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoVerifikasi", columnDefinition = cDefNoVerifikasi)
	private String noVerifikasi;

	private static final String cDefNoUrutLogin = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoUrutLogin", columnDefinition = cDefNoUrutLogin)
	private String noUrutLogin;

	private static final String cDefNoUrutRuangan = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoUrutRuangan", columnDefinition = cDefNoUrutRuangan)
	private String noUrutRuangan;

	private static final String cDefKdKelompokTransaksi = TINYINT;
	@Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
	@NotNull(message = "strukhistori.kdkelompoktransaksi.notnull")
	private Integer kdKelompokTransaksi;

	private static final String cDefNoHistoriBefore = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoHistoriBefore", columnDefinition = cDefNoHistoriBefore)
	private String noHistoriBefore;

	private static final String cDefKdLevelTingkat = TINYINT;
	@Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
	private Integer kdLevelTingkat;

	private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoSK", columnDefinition = cDefNoSK)
	private String noSK;
	
	private static final String cDefVersion =SMALLINT;
	@Version
	@Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;

	private static final String cDefStatusEnabled = TINYINT ;// AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "StatusEnabled", length = 1, precision = 1, scale = 0, columnDefinition = cDefStatusEnabled)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public Boolean statusEnabled;
	
	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "NoRec Harus Diisi")
	private String noRec;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRuangan", referencedColumnName = "KdRuangan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private Ruangan ruangan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdBankAccount", referencedColumnName = "KdBankAccount", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private BankAccount bankAccount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "KdKelompokTransaksi", referencedColumnName = "KdKelompokTransaksi", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private KelompokTransaksi kelompokTransaksi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdLevelTingkat", referencedColumnName = "KdLevelTingkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private LevelTingkat levelTingkat;
}


