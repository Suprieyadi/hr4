package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.PegawaiPostingLoanRId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PegawaiPostingLoanR_T", indexes = {
		@Index(name = "PegawaiPostingLoanR_T_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PegawaiPostingLoanR {
	
	@EmbeddedId
	private PegawaiPostingLoanRId id;

	private static final String cDefKdKeteranganAlasan = SMALLINT;
	@Column(name = "KdKeteranganAlasan", columnDefinition = cDefKdKeteranganAlasan)
	private Integer kdKeteranganAlasan;

	private static final String cDefKeteranganAlasan = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "KeteranganAlasan", columnDefinition = cDefKeteranganAlasan)
	private String namaKeteranganAlasan;

	private static final String cDefDetailKeteranganAlasan = VARCHAR + AWAL_KURUNG + 500 + AKHIR_KURUNG;
	@Column(name = "DetailKeteranganAlasan", columnDefinition = cDefDetailKeteranganAlasan)
	private String detailKeteranganAlasan;

    private static final String cDefTotalHargaSatuan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuan_S", columnDefinition = cDefTotalHargaSatuan)
    private java.math.BigDecimal totalHargaSatuanS;
    
    private static final String cDefQtyCicilanS =SMALLINT;
    @Column(name = "QtyCicilan_S", columnDefinition = cDefQtyCicilanS)
    private Integer qtyCicilanS;
	
    private static final String cDefQtyCicilanPending =SMALLINT;
    @Column(name = "QtyCicilanPending", columnDefinition = cDefQtyCicilanPending)
    @NotNull(message = "pegawaipostingloan.qtycicilan.notnull")
    private Integer qtyCicilanPending;
    
    private static final String cDefTotalHargaSatuanBayar = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuanBayar", columnDefinition = cDefTotalHargaSatuanBayar)
    private java.math.BigDecimal totalHargaSatuanBayar;
    
    private static final String cDefTglAwalBayar = BIGINT ;
    @Column(name = "TglAwalBayar", columnDefinition = cDefTglAwalBayar)
    private Long  tglAwalBayar;
    
    private static final String cDefTglAkhirBayar = BIGINT ;
    @Column(name = "TglAkhirBayar", columnDefinition = cDefTglAkhirBayar)
    private Long  tglAkhirBayar;
    	
    private static final String cDefKdCaraBayar =TINYINT;
    @Column(name = "KdCaraBayar", columnDefinition = cDefKdCaraBayar)
    private Integer kdCaraBayar;
    
    private static final String cDefPersenBunga = REAL;
    @Column(name = "PersenBunga_R", columnDefinition = cDefPersenBunga)
    private Float persenBungaR;
    
    private static final String cDefTotalHargaSatuanR = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuan_R", columnDefinition = cDefTotalHargaSatuanR)
    private java.math.BigDecimal totalHargaSatuanR;

    private static final String cDefQtyCicilanR =SMALLINT;
    @Column(name = "QtyCicilan_R", columnDefinition = cDefQtyCicilanR)
    private Integer qtyCicilanR;
    
    private static final String cDefHargaSatuanCicilanTotalR = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanTotal_R", columnDefinition = cDefHargaSatuanCicilanTotalR)
    private java.math.BigDecimal hargaSatuanCicilanTotalR;
    
    private static final String cDefHargaSatuanCicilanPokokR = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanPokok_R", columnDefinition = cDefHargaSatuanCicilanPokokR)
    private java.math.BigDecimal hargaSatuanCicilanPokokR;
    
    private static final String cDefHargaSatuanCicilanBungaR = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanBunga_R", columnDefinition = cDefHargaSatuanCicilanBungaR)
    private java.math.BigDecimal hargaSatuanCicilanBungaR;    
    
    private static final String cDefTglJatuhTempoR = BIGINT ;
    @Column(name = "TglJatuhTempo_R", columnDefinition = cDefTglJatuhTempoR)
    private Long  tglJatuhTempoR;
    
    private static final String cDefTglAwalBayarCicilanR = BIGINT ;
    @Column(name = "TglAwalBayarCicilan_R", columnDefinition = cDefTglAwalBayarCicilanR)
    private Long  tglAwalBayarCicilanR;
    
    private static final String cDefTglAkhirBayarCicilanR = BIGINT ;
    @Column(name = "TglAkhirBayarCicilan_R", columnDefinition = cDefTglAkhirBayarCicilanR)
    private Long  tglAkhirBayarCicilanR;   
    
    private static final String cDefKdCaraBayarCicilanR =TINYINT;
    @Column(name = "KdCaraBayarCicilan_R", columnDefinition = cDefKdCaraBayarCicilanR)
    private Integer kdCaraBayarCicilanR;    
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefNoRetur = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoRetur", columnDefinition = cDefNoRetur)
    private String noRetur;
    
    private static final String cDefNoVerifikasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoVerifikasi", columnDefinition = cDefNoVerifikasi)
    private String noVerifikasi;    
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK_R", columnDefinition = cDefNoSK)
    private String noSKR;
    
    private static final String cDefNoStruk = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoStruk", columnDefinition = cDefNoStruk)
    private String noStruk;
    
    private static final String cDefQtyCicilanPendingSK =SMALLINT;
    @Column(name = "QtyCicilanPendingSK", columnDefinition = cDefQtyCicilanPendingSK)
    private Integer qtyCicilanPendingSK;
        
    private static final String cDefTotalHargaSatuanBayarSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuanBayarSK", columnDefinition = cDefTotalHargaSatuanBayarSK)
    private java.math.BigDecimal totalHargaSatuanBayarSK;
        
    private static final String cDefTglAwalBayarSK = BIGINT ;
    @Column(name = "TglAwalBayarSK", columnDefinition = cDefTglAwalBayarSK)
    private Long  tglAwalBayarSK; 
    
    private static final String cDefTglAkhirBayarSK = BIGINT ;
    @Column(name = "TglAkhirBayarSK", columnDefinition = cDefTglAkhirBayarSK)
    private Long  tglAkhirBayarSK; 
        
    private static final String cDefKdCaraBayarSK =TINYINT;
    @Column(name = "KdCaraBayarSK", columnDefinition = cDefKdCaraBayarSK)
    private Integer kdCaraBayarSK;
    
    private static final String cDefPersenBungaSK =REAL;
    @Column(name = "PersenBungaSK", columnDefinition = cDefPersenBungaSK)
    private Float persenBungaSK;
    
    private static final String cDefTotalHargaSatuanSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuanSK", columnDefinition = cDefTotalHargaSatuanSK)
    private java.math.BigDecimal totalHargaSatuanSK;
    
    private static final String cDefQtyCicilanSK =SMALLINT;
    @Column(name = "QtyCicilanSK", columnDefinition = cDefQtyCicilanSK)
    private Integer qtyCicilanSK;
    
    private static final String cDefHargaSatuanCicilanTotalSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanTotalSK", columnDefinition = cDefHargaSatuanCicilanTotalSK)
    private java.math.BigDecimal hargaSatuanCicilanTotalSK;
    
    private static final String cDefHargaSatuanCicilanPokokSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanPokokSK", columnDefinition = cDefHargaSatuanCicilanPokokSK)
    private java.math.BigDecimal hargaSatuanCicilanPokokSK;
    
    private static final String cDefHargaSatuanCicilanBungaSK = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "HargaSatuanCicilanBungaSK", columnDefinition = cDefHargaSatuanCicilanBungaSK)
    private java.math.BigDecimal hargaSatuanCicilanBungaSK;
    
    private static final String cDefTglJatuhTempoSK = BIGINT ;
    @Column(name = "TglJatuhTempoSK", columnDefinition = cDefTglJatuhTempoSK)
    private Long  tglJatuhTempoSK;
    
    private static final String cDefTglAwalBayarCicilanSK = BIGINT ;
    @Column(name = "TglAwalBayarCicilanSK", columnDefinition = cDefTglAwalBayarCicilanSK)
    private Long  tglAwalBayarCicilanSK;
    
    private static final String cDefTglAkhirBayarCicilanSK = BIGINT ;
    @Column(name = "TglAkhirBayarCicilanSK", columnDefinition = cDefTglAkhirBayarCicilanSK)
    private Long  tglAkhirBayarCicilanSK;
    
    private static final String cDefKdCaraBayarCicilanSK =TINYINT;
    @Column(name = "KdCaraBayarCicilanSK", columnDefinition = cDefKdCaraBayarCicilanSK)
    private Integer kdCaraBayarCicilanSK;   
    
    private static final String cDefRKe =TINYINT;
    @Column(name = "RKe", columnDefinition = cDefRKe)
    private Integer RKe;    
        
	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "pegawaipostingloan.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "pegawaipostingloan.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "pegawaipostingloan.version.notnull")
	private Integer version;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;   
   
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPegawai", referencedColumnName="KdPegawai",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Pegawai pegawai;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdProduk", referencedColumnName="KdProduk",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Produk produk;
    
//    @ManyToOne(fetch= FetchType.LAZY)
//    @JsonIgnore
//    @JoinColumns({
//        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
//        @JoinColumn(name="NoSK", referencedColumnName="NoSK",insertable=false, updatable=false),
//        @JoinColumn(name="KdPegawai", referencedColumnName="KdPegawai",insertable=false, updatable=false),        
//        @JoinColumn(name="KdProduk", referencedColumnName="KdProduk",insertable=false, updatable=false),
//    })
//    // @ForeignKey(name="none")
//     private PegawaiPostingLoan pegawaiPostingLoan;
    
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKeteranganAlasan", referencedColumnName="KdKeteranganAlasan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KeteranganAlasan keteranganAlasan;  
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoSK", referencedColumnName = "NoSK", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private SuratKeputusan suratKeputusan;   
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoSK_R", referencedColumnName = "NoSK", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private SuratKeputusan suratKeputusanR;   	
	   
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoPosting", referencedColumnName="NoPosting",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukPosting strukPosting;
    
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoVerifikasi", referencedColumnName="NoVerifikasi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukVerifikasi strukVerifikasi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoRetur", referencedColumnName="NoRetur",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukRetur strukRetur;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdCaraBayar", referencedColumnName="KdCaraBayar",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private CaraBayar caraBayar;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdCaraBayarSK", referencedColumnName="KdCaraBayar",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private CaraBayar caraBayarSK;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdCaraBayarCicilan_R", referencedColumnName="KdCaraBayar",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private CaraBayar caraBayarCicilanR;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdCaraBayarCicilanSK", referencedColumnName="KdCaraBayar",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private CaraBayar caraBayarCicilanSK;
	
}
