package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.StrukturNomorDetailId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;

@Entity
@Table(name = "StrukturNomorDetail_M", 
indexes = {
  		@Index(name = "StrukturNomorDetail_M_Index1",  
  				columnList="FormatKode", unique = false),
		@Index(name = "StrukturNomorDetail_M_Index2",  
			columnList="statusEnabled", unique = false) 
  		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukturNomorDetail {
    
    @EmbeddedId
    private StrukturNomorDetailId id ;
    
    private static final String cDefQtyDigitKode =TINYINT;
    @Column(name = "QtyDigitKode", columnDefinition = cDefQtyDigitKode)
    @NotNull(message = "strukturnomordetail.qtydigitkode.notnull")
    private Integer qtyDigitKode;
    
    private static final String cDefKodeUrutAwal = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "KodeUrutAwal", columnDefinition = cDefKodeUrutAwal)
    @NotNull(message = "strukturnomordetail.kodeurutawal.notnull")
    private String kodeUrutAwal;
    
    private static final String cDefKodeUrutAkhir = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "KodeUrutAkhir", columnDefinition = cDefKodeUrutAkhir)
    @NotNull(message = "strukturnomordetail.kodeurutakhir.notnull")
    private String kodeUrutAkhir;
    
    private static final String cDefisAutoIncrement =TINYINT;
    @Column(name = "isAutoIncrement", columnDefinition = cDefisAutoIncrement)
    @NotNull(message = "strukturnomordetail.isautoincrement.notnull")
    private Integer isAutoIncrement;
    
    private static final String cDefFormatKode = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "FormatKode", columnDefinition = cDefFormatKode)
    @NotNull(message = "strukturnomordetail.formatkode.notnull")
    private String formatKode;
    
    private static final String cDefStatusResetNomor = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG ;
    @Column(name = "StatusResetNomor", columnDefinition = cDefStatusResetNomor)
    private String statusResetNomor;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukturnomordetail.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukturnomordetail.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukturnomordetail.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrukturNomor", referencedColumnName="KdStrukturNomor",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukturNomor strukturNomor;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdLevelTingkat", referencedColumnName="KdLevelTingkat",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private LevelTingkat levelTingkat;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
}
