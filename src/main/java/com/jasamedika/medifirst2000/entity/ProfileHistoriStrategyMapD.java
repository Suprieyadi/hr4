package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapDId;
import com.jasamedika.medifirst2000.entity.vo.ProfileHistoriStrategyMapId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ProfileHistoriStrategyMapD_M",indexes = {@Index(name = "ProfileHistoriStrategyMapD_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProfileHistoriStrategyMapD {
	
	@EmbeddedId
    private ProfileHistoriStrategyMapDId id ;
	
	private static final String cDefKdPerspective =TINYINT;
    @Column(name = "KdPerspective", columnDefinition = cDefKdPerspective)
    @NotNull(message = "profilehistoristrategymapd.kdperspective.notnull")
    private Integer kdPerspective;
    
    private static final String cDefKdStrategy =SMALLINT;
    @Column(name = "KdStrategy",   columnDefinition = cDefKdStrategy)
    @NotNull(message = "profilehistoristrategymapd.kdstrategy.notnull")
    private Integer kdStrategy;
	
	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "profilehistoristrategymapd.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "profilehistoristrategymapd.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    private Integer version;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoHistori", referencedColumnName="NoHistori",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukHistori strukHistori;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPerspective", referencedColumnName="KdPerspective",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Perspective perspective;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrategy", referencedColumnName="KdStrategy",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Strategy strategy;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemenD", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemenD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPerspectiveD", referencedColumnName="KdPerspective",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Perspective perspectiveD;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrategyD", referencedColumnName="KdStrategy",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Strategy strategyD;

}
