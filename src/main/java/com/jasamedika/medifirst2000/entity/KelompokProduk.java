/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 03/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.KelompokProdukId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "KelompokProduk_M",indexes = {@Index(name = "KelompokProduk_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class KelompokProduk {
    
    @EmbeddedId
    private KelompokProdukId id ;
    
    private static final String cDefKelompokProduk = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "KelompokProduk", columnDefinition = cDefKelompokProduk)
    @NotNull(message = "kelompokproduk.kelompokproduk.notnull")
    private String namaKelompokProduk;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "kelompokproduk.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdJenisTransaksi =TINYINT;
    @Column(name = "KdJenisTransaksi", columnDefinition = cDefKdJenisTransaksi)
    private Integer kdJenisTransaksi;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "kelompokproduk.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefisHavingStok =TINYINT;
    @Column(name = "isHavingStok", columnDefinition = cDefisHavingStok)
    private Integer isHavingStok;
    
    private static final String cDefisHavingPrice =TINYINT;
    @Column(name = "isHavingPrice", columnDefinition = cDefisHavingPrice)
    private Integer isHavingPrice;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "kelompokproduk.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "kelompokproduk.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "kelompokproduk.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdJenisTransaksi", referencedColumnName="KdJenisTransaksi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private JenisTransaksi jenisTransaksi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
}
