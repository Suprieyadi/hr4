package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.PangkatId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "Pangkat_M",indexes = {@Index(name = "Pangkat_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Pangkat {
    
    @EmbeddedId
    private PangkatId id ;
    
    private static final String cDefNamaPangkat = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "NamaPangkat", columnDefinition = cDefNamaPangkat)
    @NotNull(message = "pangkat.namapangkat.notnull")
    private String namaPangkat;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "pangkat.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefNoUrut =TINYINT;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    @NotNull(message = "pangkat.nourut.notnull")
    private Integer noUrut;

    private static final String cDefRuang = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "Ruang", columnDefinition = cDefRuang)
    private String ruang;
    
    private static final String cDefKdPangkatHead =TINYINT;
    @Column(name = "KdPangkatHead", columnDefinition = cDefKdPangkatHead)
    private Integer kdPangkatHead;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "pangkat.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "pangkat.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "pangkat.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "pangkat.version.notnull")
    private Integer version;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPangkatHead", referencedColumnName="KdPangkat",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Pangkat pangkatHead;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
}
