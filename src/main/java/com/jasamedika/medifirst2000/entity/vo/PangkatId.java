package com.jasamedika.medifirst2000.entity.vo;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PangkatId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "pangkatid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefKdPangkat =TINYINT;
    @Column(name = "KdPangkat",   columnDefinition = cDefKdPangkat)
    @NotNull(message = "pangkatid.kdpangkat.notnull")
    private Integer kode;
    
    

}
