/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity.vo;

import  static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import  javax.validation.constraints.*;
import   javax.validation.constraints.Max;
import   javax.validation.constraints.Min;
import   javax.validation.constraints.NotNull;
import  javax.persistence.*;
import  lombok.AllArgsConstructor;
import  lombok.Getter;
import  lombok.NoArgsConstructor;
import  lombok.Setter;
import  lombok.EqualsAndHashCode;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PegawaiSKLoanId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "pegawaiskloanid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK",   columnDefinition = cDefNoSK)
    @NotNull(message = "pegawaiskloanid.nosk.notnull")
    private String noSK;
    
    private static final String cDefKdKategoryPegawai = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG ;
    @Column(name = "KdKategoryPegawai",   columnDefinition = cDefKdKategoryPegawai)
    @NotNull(message = "pegawaiskloanid.kdkategorypegawai.notnull")
    private String kdKategoryPegawai;
    
    private static final String cDefKdRangeMasaKerja =TINYINT;
    @Column(name = "KdRangeMasaKerja",   columnDefinition = cDefKdRangeMasaKerja)
    @NotNull(message = "pegawaiskloanid.kdrangemasakerja.notnull")
    private Integer kdRangeMasaKerja;
    
    private static final String cDefKdProdukLoan = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG ;
    @Column(name = "KdProdukLoan",   columnDefinition = cDefKdProdukLoan)
    @NotNull(message = "pegawaiskloanid.kdprodukloan.notnull")
    private String kdProdukLoan;
    
    

}
