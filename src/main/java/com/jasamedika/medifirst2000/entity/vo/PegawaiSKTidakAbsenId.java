package com.jasamedika.medifirst2000.entity.vo;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;

/**
 * @author Lukman Hakim
 * @Created 1/16/2018
 */
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PegawaiSKTidakAbsenId implements java.io.Serializable{

    private static final String cDefKdProfile = SMALLINT;
    @Column(name = "KdProfile", columnDefinition = cDefKdProfile)
    @NotNull(message = "pegawaisktidakkenaabsenid.kdprofile.notnull")
    private Integer kdProfile;

    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK",   columnDefinition = cDefNoSK)
    @NotNull(message = "pegawaisktidakkenaabsenid.nosk.notnull")
    private String noSK;

    private static final String cDefKdPegawai = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawai",   columnDefinition = cDefKdPegawai)
    @NotNull(message = "pegawaisktidakkenaabsenid.kdpegawai.notnull")
    private String kdPegawai;


}
