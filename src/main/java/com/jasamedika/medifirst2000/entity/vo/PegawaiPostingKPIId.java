package com.jasamedika.medifirst2000.entity.vo;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PegawaiPostingKPIId implements java.io.Serializable {
	
	private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "pegawaipostingkpiid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoPosting = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoPosting",   columnDefinition = cDefNoPosting)
    @NotNull(message = "pegawaipostingkpiid.noposting.notnull")
    private String noPosting;
    
    private static final String cDefKdPegawai = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawai",   columnDefinition = cDefKdPegawai)
    @NotNull(message = "pegawaipostingkpiid.kdpegawai.notnull")
    private String kdPegawai;
    
    private static final String cDefNoHistori = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoHistori",   columnDefinition = cDefNoHistori)
    @NotNull(message = "pegawaipostingkpiid.nohistori.notnull")
    private String noHistori;
    
    private static final String cDefKdDepartemenD = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemenD",   columnDefinition = cDefKdDepartemenD)
    @NotNull(message = "pegawaipostingkpiid.kddepartemend.notnull")
    private String kdDepartemenD;
    
    private static final String cDefKdPerspectiveD =TINYINT;
    @Column(name = "KdPerspectiveD",   columnDefinition = cDefKdPerspectiveD)
    @NotNull(message = "pegawaipostingkpiid.kdperspectived.notnull")
    private Integer kdPerspectiveD;
    
    private static final String cDefKdStrategyD =SMALLINT;
    @Column(name = "KdStrategyD",   columnDefinition = cDefKdStrategyD)
    @NotNull(message = "pegawaipostingkpiid.kdstrategyd.notnull")
    private Integer kdStrategyD;
    
    private static final String cDefKdKPI =SMALLINT;
    @Column(name = "KdKPI",   columnDefinition = cDefKdKPI)
    @NotNull(message = "pegawaipostingkpiid.kdkpi.notnull")
    private Integer kdKPI;

}
