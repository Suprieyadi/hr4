 package com.jasamedika.medifirst2000.entity.vo;

import  static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import  javax.validation.constraints.*;
import   javax.validation.constraints.Max;
import   javax.validation.constraints.Min;
import   javax.validation.constraints.NotNull;
import  javax.persistence.*;
import  lombok.AllArgsConstructor;
import  lombok.Getter;
import  lombok.NoArgsConstructor;
import  lombok.Setter;
import  lombok.EqualsAndHashCode;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PegawaiSKPayrollByStatusId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK",   columnDefinition = cDefNoSK)
    @NotNull(message = "pegawaiskpayrollbystatusid.nosk.notnull")
    private String noSK;
    
    private static final String cDefKdKategoryPegawai = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG ;
    @Column(name = "KdKategoryPegawai",   columnDefinition = cDefKdKategoryPegawai)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdkategorypegawai.notnull")
    private String kdKategoryPegawai;
    
    private static final String cDefKdGolonganPegawai =TINYINT;
    @Column(name = "KdGolonganPegawai",   columnDefinition = cDefKdGolonganPegawai)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdgolonganpegawai.notnull")
    private Integer kdGolonganPegawai;
    
    private static final String cDefKdPangkat =TINYINT;
    @Column(name = "KdPangkat",   columnDefinition = cDefKdPangkat)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdpangkat.notnull")
    private Integer kdPangkat;
    
    private static final String cDefKdStatusPegawai =TINYINT;
    @Column(name = "KdStatusPegawai",   columnDefinition = cDefKdStatusPegawai)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdstatuspegawai.notnull")
    private Integer kdStatusPegawai;
    
    private static final String cDefKdRangeMasaStatus =TINYINT;
    @Column(name = "KdRangeMasaStatus",   columnDefinition = cDefKdRangeMasaStatus)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdrangemasastatus.notnull")
    private Integer kdRangeMasaStatus;
    
    private static final String cDefKdKomponenHarga =SMALLINT;
    @Column(name = "KdKomponenHarga",   columnDefinition = cDefKdKomponenHarga)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdkomponenharga.notnull")
    private Integer kdKomponenHarga;
    
    private static final String cDefKdKomponenHargaTake =SMALLINT;
    @Column(name = "KdKomponenHargaTake",   columnDefinition = cDefKdKomponenHargaTake)
    @NotNull(message = "pegawaiskpayrollbystatusid.kdkomponenhargatake.notnull")
    private Integer kdKomponenHargaTake;
    
    

}
