/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity.vo;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class HistoriRekapPersonelPlanningDId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "historirekappersonelplanningdid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoHistori = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoHistori",   columnDefinition = cDefNoHistori)
    @NotNull(message = "historirekappersonelplanningdid.nohistori.notnull")
    private String noHistori;
    
    private static final String cDefKdJenisPegawai = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdJenisPegawai",   columnDefinition = cDefKdJenisPegawai)
    @NotNull(message = "historirekappersonelplanningdid.kdjenispegawai.notnull")
    private String kdJenisPegawai;
    
    private static final String cDefKdJabatan = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdJabatan",   columnDefinition = cDefKdJabatan)
    @NotNull(message = "historirekappersonelplanningdid.kdjabatan.notnull")
    private String kdJabatan;
    
    private static final String cDefKdPangkat =TINYINT;
    @Column(name = "KdPangkat",   columnDefinition = cDefKdPangkat)
    @NotNull(message = "historirekappersonelplanningdid.kdpangkat.notnull")
    private Integer kdPangkat;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen",   columnDefinition = cDefKdDepartemen)
    @NotNull(message = "historirekappersonelplanningdid.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefKdDeskripsi =SMALLINT;
    @Column(name = "KdDeskripsi",   columnDefinition = cDefKdDeskripsi)
    @NotNull(message = "historirekappersonelplanningdid.kddeskripsi.notnull")
    private Integer kdDeskripsi;

    private static final String cDefKdGolonganPegawai = TINYINT;
    @Column(name = "KdGolonganPegawai", columnDefinition = cDefKdGolonganPegawai)
    @NotNull(message = "pegawaihistorijobdescdid.kdgolonganpegawai.notnull")
    private Integer kdGolonganPegawai;

}
