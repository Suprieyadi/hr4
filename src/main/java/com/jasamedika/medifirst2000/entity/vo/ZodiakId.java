
package com.jasamedika.medifirst2000.entity.vo;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ZodiakId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "zodiakid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefKdZodiak =TINYINT;
    @Column(name = "KdZodiak",   columnDefinition = cDefKdZodiak)
    @NotNull(message = "zodiakid.kdzodiak.notnull")
    private Integer kode;
    
    

}
