package com.jasamedika.medifirst2000.entity.vo;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PegawaiPostingLoanRId implements Serializable{

    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "pegawaipostingloanrid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK",   columnDefinition = cDefNoSK)
    @NotNull(message = "pegawaipostingloanrid.nosk.notnull")
    private String noSK;
    
    private static final String cDefNoPosting = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoPosting",   columnDefinition = cDefNoPosting)
    @NotNull(message = "pegawaipostingloanrid.noposting.notnull")
    private String noPosting;
    
    private static final String cDefKdPegawai = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawai",   columnDefinition = cDefKdPegawai)
    @NotNull(message = "pegawaipostingloanrid.kdpegawai.notnull")
    private String kdPegawai;
    
    private static final String cDefKdProduk = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG ;
    @Column(name = "KdProduk",   columnDefinition = cDefKdProduk)
    @NotNull(message = "pegawaipostingloanrid.kdproduk.notnull")
    private String kdProduk;
}
