package com.jasamedika.medifirst2000.entity.vo;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LevelTingkatId implements java.io.Serializable {
	/**
	* 
	*/
	private static final String cDefKdProfile = SMALLINT;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@NotNull(message = "leveltingkatid.kdprofile.notnull")
	private Integer kdProfile;

	private static final String cDefKdLevelTingkat = TINYINT;
	@Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
	@NotNull(message = "leveltingkatid.kdleveltingkat.notnull")
	private Integer kode;

}
