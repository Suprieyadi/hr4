package com.jasamedika.medifirst2000.entity.vo;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ProfileHistoriVisiMisiId implements java.io.Serializable {
	
	private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "profilehistorivisimisiid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoHistori = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoHistori",   columnDefinition = cDefNoHistori)
    @NotNull(message = "profilehistorivisimisiid.nohistori.notnull")
    private String noHistori;
    
    private static final String cDefKdMisi =SMALLINT;
    @Column(name = "KdMisi",   columnDefinition = cDefKdMisi)
    @NotNull(message = "profilehistorivisimisiid.kdmisi.notnull")
    private Integer kdMisi;

}
