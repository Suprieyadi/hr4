/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity.vo;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ProfileStrukturGajiByGPId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "profilestrukturgajibygpid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefNoSK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSK",   columnDefinition = cDefNoSK)
    @NotNull(message = "profilestrukturgajibygpid.nosk.notnull")
    private String noSK;
    
    private static final String cDefKdKategoryPegawai = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG ;
    @Column(name = "KdKategoryPegawai",   columnDefinition = cDefKdKategoryPegawai)
    @NotNull(message = "profilestrukturgajibygpid.kdkategorypegawai.notnull")
    private String kdKategoryPegawai;
    
    private static final String cDefKdGolonganPegawai =TINYINT;
    @Column(name = "KdGolonganPegawai",   columnDefinition = cDefKdGolonganPegawai)
    @NotNull(message = "profilestrukturgajibygpid.kdgolonganpegawai.notnull")
    private Integer kdGolonganPegawai;
    
    private static final String cDefKdPangkat =TINYINT;
    @Column(name = "KdPangkat",   columnDefinition = cDefKdPangkat)
    @NotNull(message = "profilestrukturgajibygpid.kdpangkat.notnull")
    private Integer kdPangkat;
    
    private static final String cDefKdKomponenHarga =SMALLINT;
    @Column(name = "KdKomponenHarga",   columnDefinition = cDefKdKomponenHarga)
    @NotNull(message = "profilestrukturgajibygpid.kdkomponenharga.notnull")
    private Integer kdKomponenHarga;
    
    

}
