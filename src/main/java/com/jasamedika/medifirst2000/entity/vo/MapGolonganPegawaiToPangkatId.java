package com.jasamedika.medifirst2000.entity.vo;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class MapGolonganPegawaiToPangkatId implements java.io.Serializable {

    private static final String cDefKdProfile =SMALLINT;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "mapgolonganpegawaitopangkatid.kdprofile.notnull")
    private Integer kdProfile;
    
	private static final String cDefKdGolonganPegawai = TINYINT;
	@Column(name = "KdGolonganPegawai", columnDefinition = cDefKdGolonganPegawai)
	@NotNull(message = "mapgolonganpegawaitopangkatid.kdgolonganpegawai.notnull")
	private Integer kdGolonganPegawai;
	
	private static final String cDefKdPangkat = TINYINT;
	@Column(name = "KdPangkat", columnDefinition = cDefKdPangkat)
	@NotNull(message = "mapgolonganpegawaitopangkatid.kdpangkat.notnull")
	private Integer kdPangkat;
    
    
}
