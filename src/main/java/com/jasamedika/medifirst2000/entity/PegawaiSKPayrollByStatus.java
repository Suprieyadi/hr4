package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.BIGDECIMAL;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.CHAR;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.FLOAT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.PegawaiSKPayrollByStatusId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PegawaiSKPayrollByStatus_M", indexes = {
		@Index(name = "PegawaiSKPayrollByStatus_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PegawaiSKPayrollByStatus {

	@EmbeddedId
	private PegawaiSKPayrollByStatusId id;

	private static final String cDefHargaSatuan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "HargaSatuan", columnDefinition = cDefHargaSatuan)
	private BigDecimal hargaSatuan;

	private static final String cDefPersenHargaSatuan = FLOAT;
	@Column(name = "PersenHargaSatuan", columnDefinition = cDefPersenHargaSatuan)
	private Float persenHargaSatuan;

	private static final String cDefFactorRate = FLOAT;
	@Column(name = "FactorRate", columnDefinition = cDefFactorRate)
	@NotNull(message = "pegawaiskpayrollbystatus.factorrate.notnull")
	private Float factorRate;

	private static final String cDefOperatorFactorRate = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "OperatorFactorRate", columnDefinition = cDefOperatorFactorRate)
	private String operatorFactorRate;

	private static final String cDefisByDay = TINYINT;
	@Column(name = "isByDay", columnDefinition = cDefisByDay)
	private Integer isByDay;

	private static final String cDefFactorRateDivide = FLOAT;
	@Column(name = "FactorRateDivide", columnDefinition = cDefFactorRateDivide)
	private Float factorRateDivide;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "pegawaiskpayrollbystatus.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "pegawaiskpayrollbystatus.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "pegawaiskpayrollbystatus.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKomponenHargaTake", referencedColumnName = "KdKomponen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Komponen komponenHargaTake;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKomponenHarga", referencedColumnName = "KdKomponen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Komponen komponen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRangeMasaStatus", referencedColumnName = "KdRange", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Range range;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdStatusPegawai", referencedColumnName = "KdStatus", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Status status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganPegawai", referencedColumnName = "KdGolonganPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private GolonganPegawai golonganPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPangkat", referencedColumnName = "KdPangkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pangkat pangkat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKategoryPegawai", referencedColumnName = "KdKategoryPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KategoryPegawai kategoryPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoSK", referencedColumnName = "NoSK", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private SuratKeputusan suratKeputusan;

}
