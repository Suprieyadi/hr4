/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.JenisTransaksiId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "JenisTransaksi_M",indexes = {@Index(name = "JenisTransaksi_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class JenisTransaksi {
    
    @EmbeddedId
    private JenisTransaksiId id ;
    
    private static final String cDefJenisTransaksi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "JenisTransaksi", columnDefinition = cDefJenisTransaksi)
    @NotNull(message = "jenistransaksi.jenistransaksi.notnull")
    private String namaJenisTransaksi;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "jenistransaksi.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefMetodeHargaNetto =TINYINT;
    @Column(name = "MetodeHargaNetto", columnDefinition = cDefMetodeHargaNetto)
    @NotNull(message = "jenistransaksi.metodeharganetto.notnull")
    private Integer metodeHargaNetto;
    
    private static final String cDefMetodeStokHargaNetto =TINYINT;
    @Column(name = "MetodeStokHargaNetto", columnDefinition = cDefMetodeStokHargaNetto)
    @NotNull(message = "jenistransaksi.metodestokharganetto.notnull")
    private Integer metodeStokHargaNetto;
    
    private static final String cDefMetodeAmbilHargaNetto =TINYINT;
    @Column(name = "MetodeAmbilHargaNetto", columnDefinition = cDefMetodeAmbilHargaNetto)
    @NotNull(message = "jenistransaksi.metodeambilharganetto.notnull")
    private Integer metodeAmbilHargaNetto;
    
    private static final String cDefSistemHargaNetto =TINYINT;
    @Column(name = "SistemHargaNetto", columnDefinition = cDefSistemHargaNetto)
    @NotNull(message = "jenistransaksi.sistemharganetto.notnull")
    private Integer sistemHargaNetto;
    
    private static final String cDefJenisPersenCito = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG ;
    @Column(name = "JenisPersenCito", columnDefinition = cDefJenisPersenCito)
    @NotNull(message = "jenistransaksi.jenispersencito.notnull")
    private String jenisPersenCito;
    
    private static final String cDefKdProdukCito = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG ;
    @Column(name = "KdProdukCito", columnDefinition = cDefKdProdukCito)
    private String kdProdukCito;
    
    private static final String cDefKdProdukRetur = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG ;
    @Column(name = "KdProdukRetur", columnDefinition = cDefKdProdukRetur)
    private String kdProdukRetur;
    
    private static final String cDefKdKelasDefault = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdKelasDefault", columnDefinition = cDefKdKelasDefault)
    private String kdKelasDefault;
    
    private static final String cDefKdProdukDeposit = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG ;
    @Column(name = "KdProdukDeposit", columnDefinition = cDefKdProdukDeposit)
    private String kdProdukDeposit;
    
    private static final String cDefTglBerlakuTarif = INTEGER ;
    @Column(name = "TglBerlakuTarif", columnDefinition = cDefTglBerlakuTarif)
    private Long  tglBerlakuTarif;
    
    private static final String cDefKdKelompokPelayanan =TINYINT;
    @Column(name = "KdKelompokPelayanan", columnDefinition = cDefKdKelompokPelayanan)
    private Integer kdKelompokPelayanan;
    
    private static final String cDefSistemDiscount =TINYINT;
    @Column(name = "SistemDiscount", columnDefinition = cDefSistemDiscount)
    private Integer sistemDiscount;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 20 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "jenistransaksi.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "jenistransaksi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "jenistransaksi.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "jenistransaksi.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKelompokPelayanan", referencedColumnName="KdKelompokPelayanan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KelompokPelayanan kelompokPelayanan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdProdukCito", referencedColumnName="KdProduk",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Produk produkCito;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdProdukRetur", referencedColumnName="KdProduk",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Produk produkRetur;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdProdukDeposit", referencedColumnName="KdProduk",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Produk produkDeposit;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKelasDefault", referencedColumnName="KdKelas",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Kelas kelasDefault;
}
