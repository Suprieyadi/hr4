/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.JenisPetugasPelaksanaId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "JenisPetugasPelaksana_M",indexes = {@Index(name = "JenisPetugasPelaksana_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class JenisPetugasPelaksana {
    
    @EmbeddedId
    private JenisPetugasPelaksanaId id ;
    
    private static final String cDefJenisPetugasPe = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "JenisPetugasPe", columnDefinition = cDefJenisPetugasPe)
    @NotNull(message = "jenispetugaspelaksana.jenispetugaspe.notnull")
    private String jenisPetugasPe;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "jenispetugaspelaksana.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdKomponenHarga =SMALLINT;
    @Column(name = "KdKomponenHarga", columnDefinition = cDefKdKomponenHarga)
    private Integer kdKomponenHarga;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefKdJenisPetugasPeHead =TINYINT;
    @Column(name = "KdJenisPetugasPeHead", columnDefinition = cDefKdJenisPetugasPeHead)
    private Integer kdJenisPetugasPeHead;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "jenispetugaspelaksana.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "jenispetugaspelaksana.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "jenispetugaspelaksana.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "jenispetugaspelaksana.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdJenisPetugasPeHead", referencedColumnName="KdJenisPetugasPe",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private JenisPetugasPelaksana jenisPetugasPeHead;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdKomponenHarga", referencedColumnName="KdKomponen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Komponen komponen;
}
