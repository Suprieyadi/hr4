package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.SettingDataFixedId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "SettingDataFixed_M",indexes = {@Index(name = "SettingDataFixed_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class SettingDataFixed {
    
    @EmbeddedId
    private SettingDataFixedId id ;
    
    private static final String cDefTypeField = VARCHAR + AWAL_KURUNG + 20 + AKHIR_KURUNG ;
    @Column(name = "TypeField", columnDefinition = cDefTypeField)
    @NotNull(message = "settingdatafixed.typefield.notnull")
    private String typeField;
    
    private static final String cDefNilaiField = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NilaiField", columnDefinition = cDefNilaiField)
    @NotNull(message = "settingdatafixed.nilaifield.notnull")
    private String nilaiField;
    
    private static final String cDefTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "TabelRelasi", columnDefinition = cDefTabelRelasi)
    private String tabelRelasi;
    
    private static final String cDefFieldKeyTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "FieldKeyTabelRelasi", columnDefinition = cDefFieldKeyTabelRelasi)
    private String fieldKeyTabelRelasi;
    
    private static final String cDefFieldReportDisplayTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "FieldReportDisplayTabelRelasi", columnDefinition = cDefFieldReportDisplayTabelRelasi)
    private String fieldReportDisplayTabelRelasi;
    
    private static final String cDefKeteranganFungsi = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganFungsi", columnDefinition = cDefKeteranganFungsi)
    private String keteranganFungsi;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "settingdatafixed.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "settingdatafixed.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "settingdatafixed.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
}
