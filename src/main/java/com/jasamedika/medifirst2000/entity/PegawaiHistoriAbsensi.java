package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.PegawaiHistoriAbsensiId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "PegawaiHistoriAbsensi_T", indexes = {
		@Index(name = "PegawaiHistoriAbsensi_T_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PegawaiHistoriAbsensi {

	@EmbeddedId
	private PegawaiHistoriAbsensiId id;

	private static final String cDefKdStatusAbsensi = TINYINT;
	@Column(name = "KdStatusAbsensi", columnDefinition = cDefKdStatusAbsensi)
	@NotNull(message = "pegawaihistoriabsensi.kdstatusabsensi.notnull")
	private Integer kdStatusAbsensi;

	private static final String cDefTglMasuk = INTEGER;
	@Column(name = "TglMasuk", columnDefinition = cDefTglMasuk)
	// @NotNull(message = "pegawaihistoriabsensi.tglmasuk.notnull")
	private Long tglMasuk;

	private static final String cDefTglKeluar = INTEGER;
	@Column(name = "TglKeluar", columnDefinition = cDefTglKeluar)
	private Long tglKeluar;

	private static final String cDefTglIstirahatAwal = INTEGER;
	@Column(name = "TglIstirahatAwal", columnDefinition = cDefTglIstirahatAwal)
	private Long tglIstirahatAwal;

	private static final String cDefTglIstirahatAkhir = INTEGER;
	@Column(name = "TglIstirahatAkhir", columnDefinition = cDefTglIstirahatAkhir)
	private Long tglIstirahatAkhir;

	private static final String cDefKdPegawaiPJawab = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiPJawab", columnDefinition = cDefKdPegawaiPJawab)
	private String kdPegawaiPJawab;

	private static final String cDefKdRuanganKerja = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG;
	@Column(name = "KdRuanganKerja", columnDefinition = cDefKdRuanganKerja)
	private String kdRuanganKerja;

	private static final String cDefKeteranganAlasan = VARCHAR + AWAL_KURUNG + 300 + AKHIR_KURUNG;
	@Column(name = "KeteranganAlasan", columnDefinition = cDefKeteranganAlasan)
	private String keteranganAlasan;

	private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG;
	@Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
	private String keteranganLainnya;

	private static final String cDefFingerPrintID = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "FingerPrintID", columnDefinition = cDefFingerPrintID)
	private String fingerPrintID;

	private static final String cDefKdAlat = SMALLINT;
	@Column(name = "KdAlat", columnDefinition = cDefKdAlat)
	private Integer kdAlat;

	private static final String cDefImagePegawaiIn = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Max(value = 100, message = "pegawaihistoriabsensi.imagepegawaiin.max")
	@Column(name = "ImagePegawaiIn", columnDefinition = cDefImagePegawaiIn)
	private String imagePegawaiIn;

	private static final String cDefImagePegawaiOut = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Max(value = 100, message = "pegawaihistoriabsensi.imagepegawaiout.max")
	@Column(name = "ImagePegawaiOut", columnDefinition = cDefImagePegawaiOut)
	private String imagePegawaiOut;

	private static final String cDefQtyMenitOverHK = FLOAT;
	@Column(name = "QtyMenitOverHK", columnDefinition = cDefQtyMenitOverHK)
	private Float qtyMenitOverHK;

	private static final String cDefQtyMenitOverHL = FLOAT;
	@Column(name = "QtyMenitOverHL", columnDefinition = cDefQtyMenitOverHL)
	private Float qtyMenitOverHL;

	private static final String cDefNoSKStatusPegawai = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoSKStatusPegawai", columnDefinition = cDefNoSKStatusPegawai)
	private String noSKStatusPegawai;

	private static final String cDefKdStatusPegawai = TINYINT;
	@Column(name = "KdStatusPegawai", columnDefinition = cDefKdStatusPegawai)
	private Integer kdStatusPegawai;

	private static final String cDefTglAwalStatusPegawai = INTEGER;
	@Column(name = "TglAwalStatusPegawai", columnDefinition = cDefTglAwalStatusPegawai)
	private Long tglAwalStatusPegawai;

	private static final String cDefTglAkhirStatusPegawai = INTEGER;
	@Column(name = "TglAkhirStatusPegawai", columnDefinition = cDefTglAkhirStatusPegawai)
	private Long tglAkhirStatusPegawai;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "pegawaihistoriabsensi.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoVerifikasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoVerifikasi", columnDefinition = cDefNoVerifikasi)
	private String noVerifikasi;

	private static final String cDefNoClosing = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoClosing", columnDefinition = cDefNoClosing)
	private String noClosing;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "pegawaihistoriabsensi.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "pegawaihistoriabsensi.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoHistori", referencedColumnName = "NoHistori", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private StrukHistori strukHistori;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawai", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pegawai pegawai;

	/*
	 * @ManyToOne(fetch= FetchType.LAZY)
	 * 
	 * @JsonIgnore
	 * 
	 * @JoinColumns({
	 * 
	 * @JoinColumn(name="KdProfile",
	 * referencedColumnName="KdProfile",insertable=false, updatable=false),
	 * 
	 * @JoinColumn(name="KdAlat", referencedColumnName="KdAlat",insertable=false,
	 * updatable=false), }) // @ForeignKey(name="none") private Alat alat;
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoVerifikasi", referencedColumnName = "NoVerifikasi", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private StrukVerifikasi strukVerifikasi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoClosing", referencedColumnName = "NoClosing", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private StrukClosing strukClosing;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "kdStatusAbsensi", referencedColumnName = "KdStatus", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Status statusAbsensi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "kdStatusPegawai", referencedColumnName = "KdStatus", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Status statusPegawai;

}
