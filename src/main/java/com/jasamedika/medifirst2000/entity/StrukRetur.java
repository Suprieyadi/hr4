package com.jasamedika.medifirst2000.entity;

import  static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import  javax.persistence.*;
import  javax.validation.constraints.*;
import  lombok.AllArgsConstructor;
import  lombok.Getter;
import  lombok.NoArgsConstructor;
import  lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.StrukReturId;


@Entity
@Table(name = "StrukRetur_T", 
indexes = {
  		@Index(name = "StrukRetur_T_Index1",  
  				columnList="TglRetur, NoUrutLogin, NoUrutRuangan, KdKelompokTransaksi", unique = false),
		@Index(name = "StrukRetur_T_Index2",  
			columnList="StatusEnabled", unique = false) 
  		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukRetur {
    
    @EmbeddedId
    private StrukReturId id ;
    
    private static final String cDefTglRetur = INTEGER ;
    @Column(name = "TglRetur", columnDefinition = cDefTglRetur)
    @NotNull(message = "strukretur.tglretur.notnull")
    private Long  tglRetur;
    
    private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuangan", columnDefinition = cDefKdRuangan)
    @NotNull(message = "strukretur.kdruangan.notnull")
    private String kdRuangan;
    
    private static final String cDefKeteranganAlasan = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganAlasan", columnDefinition = cDefKeteranganAlasan)
    private String keteranganAlasan;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefKdPegawaiPJawab = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawaiPJawab", columnDefinition = cDefKdPegawaiPJawab)
    private String kdPegawaiPJawab;
    
    private static final String cDefNoSBK = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSBK", columnDefinition = cDefNoSBK)
    private String noSBK;
    
    private static final String cDefKdKelompokTransaksi =TINYINT;
    @Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
    private Integer kdKelompokTransaksi;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukretur.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoUrutLogin = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutLogin", columnDefinition = cDefNoUrutLogin)
    private String noUrutLogin;
    
    private static final String cDefNoUrutRuangan = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutRuangan", columnDefinition = cDefNoUrutRuangan)
    private String noUrutRuangan;
    
    private static final String cDefNoReturBefore = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoReturBefore", columnDefinition = cDefNoReturBefore)
    private String noReturBefore;
    
    private static final String cDefKdLevelTingkat =TINYINT;
    @Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
    private Integer kdLevelTingkat;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukretur.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukretur.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoRetur", referencedColumnName="NoRetur",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukRetur strukRetur;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdRuangan", referencedColumnName="KdRuangan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Ruangan ruangan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdKelompokTransaksi", referencedColumnName="KdKelompokTransaksi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KelompokTransaksi kelompokTransaksi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdLevelTingkat", referencedColumnName="KdLevelTingkat",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private LevelTingkat levelTingkat;
}
