/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.MerkProdukId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "MerkProduk_M",indexes = {@Index(name = "MerkProduk_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MerkProduk {
    
    @EmbeddedId
    private MerkProdukId id ;
    
    private static final String cDefMerkProduk = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "MerkProduk", columnDefinition = cDefMerkProduk)
    @NotNull(message = "merkproduk.merkproduk.notnull")
    private String namaMerkProduk;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "merkproduk.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "merkproduk.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 30 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "merkproduk.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "merkproduk.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "merkproduk.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Departemen departemen;
}
