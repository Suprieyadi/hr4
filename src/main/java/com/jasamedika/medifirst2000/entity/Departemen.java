/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 31/10/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import com.jasamedika.medifirst2000.entity.vo.DepartemenId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Departemen_M", indexes = {
		@Index(name = "Departemen_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Departemen {

	@EmbeddedId
	private DepartemenId id;

	private static final String cDefNamaDepartemen = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaDepartemen", columnDefinition = cDefNamaDepartemen)
	@NotNull(message = "departemen.namadepartemen.notnull")
	private String namaDepartemen;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "departemen.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefKdJenisPerawatan = TINYINT;
	@Column(name = "KdJenisPerawatan", columnDefinition = cDefKdJenisPerawatan)
	private Integer kdJenisPerawatan;

	////
	private static final String cDefKdAlamat = INTEGER;
	@Column(name = "KdAlamat", columnDefinition = cDefKdAlamat)
	private Integer kdAlamat;

	/*
	 * private static final String cDefFixedPhone = VARCHAR + AWAL_KURUNG + 30 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "FixedPhone", columnDefinition = cDefFixedPhone) private
	 * String fixedPhone;
	 * 
	 * private static final String cDefMobilePhone = VARCHAR + AWAL_KURUNG + 30 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "MobilePhone", columnDefinition = cDefMobilePhone) private
	 * String mobilePhone;
	 * 
	 * private static final String cDefFaksimile = VARCHAR + AWAL_KURUNG + 30 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "Faksimile", columnDefinition = cDefFaksimile) private String
	 * faksimile;
	 * 
	 * private static final String cDefAlamatEmail = VARCHAR + AWAL_KURUNG + 50 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "AlamatEmail", columnDefinition = cDefAlamatEmail) private
	 * String alamatEmail;
	 * 
	 * private static final String cDefWebsite = VARCHAR + AWAL_KURUNG + 80 +
	 * AKHIR_KURUNG ;
	 * 
	 * @Column(name = "Website", columnDefinition = cDefWebsite) private String
	 * website;
	 */

	private static final String cDefKdPegawaiKepala = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiKepala", columnDefinition = cDefKdPegawaiKepala)
	private String kdPegawaiKepala;

	private static final String cDefPrefixNoAntrian = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG;
	@Column(name = "PrefixNoAntrian", columnDefinition = cDefPrefixNoAntrian)
	private String prefixNoAntrian;

	private static final String cDefKdDepartemenHead = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemenHead", columnDefinition = cDefKdDepartemenHead)
	private String kdDepartemenHead;

	private static final String cDefisProfitCostCenter = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "isProfitCostCenter", columnDefinition = cDefisProfitCostCenter)
	private String isProfitCostCenter;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "departemen.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "departemen.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "departemen.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisPerawatan", referencedColumnName = "KdJenisPerawatan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private JenisPerawatan jenisPerawatan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiKepala", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pegawai pegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemenHead", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemenHead;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdAlamat", referencedColumnName = "KdAlamat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Alamat alamat;
}
