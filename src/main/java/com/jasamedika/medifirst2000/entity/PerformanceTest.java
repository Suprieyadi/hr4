package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Performance_Test_T")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PerformanceTest {

	@Id
	@GeneratedValue
	Integer nomor;
	
	Integer kdProfile;
	
	Long tglLahir;
	Double umur;
	Long tglHitung;
	
	
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    private Integer version;
	
}
