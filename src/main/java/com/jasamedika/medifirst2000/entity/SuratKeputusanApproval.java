package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.SuratKeputusanApprovalId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;

@Entity
@Table(name = "SuratKeputusanApproval_M", indexes = {
		@Index(name = "SuratKeputusanApproval_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class SuratKeputusanApproval {

	@EmbeddedId
	private SuratKeputusanApprovalId id;
	
	private static final String cDefKdLevelTingkat = TINYINT;
	@Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
	private Integer kdLevelTingkat;

	private static final String cDefNoUrutApproval = TINYINT;
	@Column(name = "NoUrutApproval", columnDefinition = cDefNoUrutApproval)
	@NotNull(message = "suratkeputusanapproval.nourutapproval.notnull")
	private Integer noUrutApproval;

	private static final String cDefisCanByPass = TINYINT;
	@Column(name = "isCanByPass", columnDefinition = cDefisCanByPass)
	@NotNull(message = "suratkeputusanapproval.iscanbypass.notnull")
	private Integer isCanByPass;

	private static final String cDefKdDepartemenDelegasi = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemenDelegasi", columnDefinition = cDefKdDepartemenDelegasi)
	private String kdDepartemenDelegasi;

	private static final String cDefKdJabatanDelegasi = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdJabatanDelegasi", columnDefinition = cDefKdJabatanDelegasi)
	private String kdJabatanDelegasi;

	private static final String cDefisDelegatorActivated = TINYINT;
	@Column(name = "isDelegatorActivated", columnDefinition = cDefisDelegatorActivated)
	@NotNull(message = "suratkeputusanapproval.isdelegatoractivated.notnull")
	private Integer isDelegatorActivated;

	private static final String cDefisProviderSK = TINYINT;
	@Column(name = "isProviderSK", columnDefinition = cDefisProviderSK)
	private Integer isProviderSK;

	private static final String cDefStatusEnabled = TINYINT;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "suratkeputusanapproval.statusenabled.notnull")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "suratkeputusanapproval.norec.notnull")
	private String noRec;

	private static final String cDefVersion = SMALLINT;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "suratkeputusanapproval.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoSK", referencedColumnName = "NoSK", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private SuratKeputusan suratKeputusan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdKelompokTransaksi", referencedColumnName = "KdKelompokTransaksi", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KelompokTransaksi kelompokTransaksi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemenDelegasi", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemenDelegasi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJabatan", referencedColumnName = "KdJabatan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Jabatan jabatan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJabatanDelegasi", referencedColumnName = "KdJabatan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private Jabatan jabatanDelegasi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemenAsal", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemenAsal;

}
