package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.base.entity.BaseMaster;
import com.jasamedika.medifirst2000.entity.vo.PegawaiId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;

@Entity
@Table(name = "Pegawai_M", indexes = { @Index(columnList = "StatusEnabled", name = "statusEnabledPegawai_M") })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Pegawai extends BaseMaster {
	@EmbeddedId
	private PegawaiId id;

	private static final String cDefFingerPrintID = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "FingerPrintID", columnDefinition = cDefFingerPrintID)
	private String fingerPrintID;

	private static final String cDefImageSignature = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "ImageSignature", columnDefinition = cDefImageSignature)
	private String imageSignature;

	private static final String cDefKdAgama = TINYINT;
	@Column(name = "KdAgama", columnDefinition = cDefKdAgama)
	private Integer kdAgama;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "pegawai.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefKdEselon = TINYINT;
	@Column(name = "KdEselon", columnDefinition = cDefKdEselon)
	private Integer kdEselon;

	private static final String cDefKdGolonganDarah = TINYINT;
	@Column(name = "KdGolonganDarah", columnDefinition = cDefKdGolonganDarah)
	private Integer kdGolonganDarah;

	private static final String cDefKdHubunganKeluarga = TINYINT;
	@Column(name = "KdHubunganKeluarga", columnDefinition = cDefKdHubunganKeluarga)
	private Integer kdHubunganKeluarga;

	private static final String cDefKdJabatan = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdJabatan", columnDefinition = cDefKdJabatan)
	private String kdJabatan;

	private static final String cDefKdJabatanLamar = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdJabatanLamar", columnDefinition = cDefKdJabatanLamar)
	private String kdJabatanLamar;

	private static final String cDefKdJenisKelamin = TINYINT;
	@Column(name = "KdJenisKelamin", columnDefinition = cDefKdJenisKelamin)
	@NotNull(message = "pegawai.kdjeniskelamin.notnull")
	private Integer kdJenisKelamin;

	private static final String cDefKdJenisPegawai = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdJenisPegawai", columnDefinition = cDefKdJenisPegawai)
	@NotNull(message = "pegawai.kdjenispegawai.notnull")
	private String kdJenisPegawai;

	private static final String cDefKdJenisPegawaiLamar = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdJenisPegawaiLamar", columnDefinition = cDefKdJenisPegawaiLamar)
	private String kdJenisPegawaiLamar;

	private static final String cDefKdKategoryPegawai = CHAR + AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "KdKategoryPegawai", columnDefinition = cDefKdKategoryPegawai)
	private String kdKategoryPegawai;

//	private static final String cDefKdKelompokUser = TINYINT;
//	@Column(name = "KdKelompokUser", columnDefinition = cDefKdKelompokUser)
//	private Integer kdKelompokUser;

	private static final String cDefKdKualifikasiJurusan = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG;
	@Column(name = "KdKualifikasiJurusan", columnDefinition = cDefKdKualifikasiJurusan)
	private String kdKualifikasiJurusan;

	private static final String cDefKdLevelTingkat = TINYINT;
	@Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
	private Integer kdLevelTingkat;

	private static final String cDefKdNegara = TINYINT;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "pegawai.kdnegara.notnull")
	private Integer kdNegara;

	private static final String cDefKdPTKP = TINYINT;
	@Column(name = "KdPTKP", columnDefinition = cDefKdPTKP)
	private Integer kdPTKP;

	private static final String cDefKdGolonganPegawai = TINYINT;
	@Column(name = "KdGolonganPegawai", columnDefinition = cDefKdGolonganPegawai)
	private Integer kdGolonganPegawai;

	private static final String cDefKdPangkat = TINYINT;
	@Column(name = "KdPangkat", columnDefinition = cDefKdPangkat)
	private Integer kdPangkat;

	private static final String cDefKdPegawaiHead = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiHead", columnDefinition = cDefKdPegawaiHead)
	private String kdPegawaiHead;

	private static final String cDefKdPegawaiPembimbing = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiPembimbing", columnDefinition = cDefKdPegawaiPembimbing)
	private String kdPegawaiPembimbing;

	private static final String cDefKdPendidikanTerakhir = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdPendidikanTerakhir", columnDefinition = cDefKdPendidikanTerakhir)
	private String kdPendidikanTerakhir;

	private static final String cDefKdRangeTotalNilaiScore = TINYINT;
	@Column(name = "KdRangeTotalNilaiScore", columnDefinition = cDefKdRangeTotalNilaiScore)
	private Integer kdRangeTotalNilaiScore;

	private static final String cDefKdRekananInstitusiAsal = SMALLINT;
	@Column(name = "KdRekananInstitusiAsal", columnDefinition = cDefKdRekananInstitusiAsal)
	private Integer kdRekananInstitusiAsal;

	private static final String cDefKdRuanganKerja = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG;
	@Column(name = "KdRuanganKerja", columnDefinition = cDefKdRuanganKerja)
	private String kdRuanganKerja;

	private static final String cDefKdStatusPegawai = TINYINT;
	@Column(name = "KdStatusPegawai", columnDefinition = cDefKdStatusPegawai)
	private Integer kdStatusPegawai;

	private static final String cDefKdStatusPerkawinan = TINYINT;
	@Column(name = "KdStatusPerkawinan", columnDefinition = cDefKdStatusPerkawinan)
	private Integer kdStatusPerkawinan;

	private static final String cDefKdSuku = TINYINT;
	@Column(name = "KdSuku", columnDefinition = cDefKdSuku)
	private Integer kdSuku;

	private static final String cDefKdTitle = TINYINT;
	@Column(name = "KdTitle", columnDefinition = cDefKdTitle)
	private Integer kdTitle;

	private static final String cDefKdTypePegawai = TINYINT;
	@Column(name = "KdTypePegawai", columnDefinition = cDefKdTypePegawai)
	private Integer kdTypePegawai;

	private static final String cDefNIKGroup = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NIKGroup", columnDefinition = cDefNIKGroup)
	private String nIKGroup;

	private static final String cDefNIKIntern = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NIKIntern", columnDefinition = cDefNIKIntern)
	private String nIKIntern;

	private static final String cDefNIPPNS = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NIPPNS", columnDefinition = cDefNIPPNS)
	private String nIPPNS;

	private static final String cDefNPWP = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NPWP", columnDefinition = cDefNPWP)
	private String nPWP;

	private static final String cDefNamaKeluarga = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaKeluarga", columnDefinition = cDefNamaKeluarga)
	private String namaKeluarga;

	private static final String cDefNamaLengkap = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaLengkap", columnDefinition = cDefNamaLengkap)
	@NotNull(message = "pegawai.namalengkap.notnull")
	private String namaLengkap;
    
    private static final String cDefNamaAwal = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaAwal", columnDefinition = cDefNamaAwal)
	@NotNull(message = "pegawai.namaawal.notnull")
	private String namaAwal;

	private static final String cDefNamaTengah = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaTengah", columnDefinition = cDefNamaTengah)
	private String namaTengah;

	private static final String cDefNamaAkhir = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaAkhir", columnDefinition = cDefNamaAkhir)
	private String namaAkhir;
    
	private static final String cDefNamaPanggilan = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaPanggilan", columnDefinition = cDefNamaPanggilan)
	private String namaPanggilan;

	private static final String cDefNoHistori = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoHistori", columnDefinition = cDefNoHistori)
	private String noHistori;

	private static final String cDefNoSKHasil = CHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NoSKHasil", columnDefinition = cDefNoSKHasil)
	private String noSKHasil;

	private static final String cDefNoStrukTTujuanLast = CHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NoStrukTTujuanLast", columnDefinition = cDefNoStrukTTujuanLast)
	private String noStrukTTujuanLast;

	private static final String cDefNoVerifikasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG;
	@Column(name = "NoVerifikasi", columnDefinition = cDefNoVerifikasi)
	private String noVerifikasi;

	private static final String cDefPhotoDiri = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "PhotoDiri", columnDefinition = cDefPhotoDiri)
	private String photoDiri;

	private static final String cDefQtyAnak = TINYINT;
	@Column(name = "QtyAnak", columnDefinition = cDefQtyAnak)
	private Integer qtyAnak;

	private static final String cDefQtyTanggungan = TINYINT;
	@Column(name = "QtyTanggungan", columnDefinition = cDefQtyTanggungan)
	private Integer qtyTanggungan;

	private static final String cDefQtyTotalJiwa = TINYINT;
	@Column(name = "QtyTotalJiwa", columnDefinition = cDefQtyTotalJiwa)
	private Integer qtyTotalJiwa;

	private static final String cDefStatusLogin = TINYINT;
	@Column(name = "StatusLogin", columnDefinition = cDefStatusLogin)
	private Integer statusLogin;

	private static final String cDefStatusRhesus = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "StatusRhesus", columnDefinition = cDefStatusRhesus)
	private String statusRhesus;

	private static final String cDefTempatLahir = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "TempatLahir", columnDefinition = cDefTempatLahir)
	private String tempatLahir;

	private static final String cDefTglDaftar = INTEGER;
	@Column(name = "TglDaftar", columnDefinition = cDefTglDaftar)
	@NotNull(message = "pegawai.tgldaftar.notnull")
	private Long tglDaftar;

	private static final String cDefTglDaftarFingerPrint = INTEGER;
	@Column(name = "TglDaftarFingerPrint", columnDefinition = cDefTglDaftarFingerPrint)
	private Long tglDaftarFingerPrint;

	private static final String cDefTglKeluar = INTEGER;
	@Column(name = "TglKeluar", columnDefinition = cDefTglKeluar)
	private Long tglKeluar;

	private static final String cDefTglLahir = INTEGER;
	@Column(name = "TglLahir", columnDefinition = cDefTglLahir)
	@NotNull(message = "pegawai.tgllahir.notnull")
	private Long tglLahir;

	private static final String cDefTglMasuk = INTEGER;
	@Column(name = "TglMasuk", columnDefinition = cDefTglMasuk)
	private Long tglMasuk;

	private static final String cDefTotalNilaiScore = DECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG;
	@Column(name = "TotalNilaiScore", columnDefinition = cDefTotalNilaiScore)
	private BigDecimal totalNilaiScore;

	private static final String cDefKdZodiak = TINYINT;
	@Column(name = "KdZodiak", columnDefinition = cDefKdZodiak)
	private Integer kdZodiak;

	private static final String cDefKdZodiakUnsur = TINYINT;
	@Column(name = "KdZodiakUnsur", columnDefinition = cDefKdZodiakUnsur)
	private Integer kdZodiakUnsur;

	private static final String cDefZodiakSifat = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "ZodiakSifat", columnDefinition = cDefZodiakSifat)
	private String zodiakSifat;

	private static final String cDefNamaUser = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NamaUser", columnDefinition = cDefNamaUser)
	private String namaUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdAgama", referencedColumnName = "KdAgama", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Agama agama;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "kdZodiak", referencedColumnName = "kdZodiak", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Zodiak zodiak;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdZodiakUnsur", referencedColumnName = "KdZodiakUnsur", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private ZodiakUnsur zodiakUnsur;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdEselon", referencedColumnName = "KdEselon", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Eselon eselon;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganDarah", referencedColumnName = "KdGolonganDarah", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private GolonganDarah golonganDarah;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdHubunganKeluarga", referencedColumnName = "KdHubunganKeluarga", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private HubunganKeluarga hubunganKeluarga;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJabatan", referencedColumnName = "KdJabatan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Jabatan jabatan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisKelamin", referencedColumnName = "KdJenisKelamin", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private JenisKelamin jenisKelamin;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisPegawai", referencedColumnName = "KdJenisPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private JenisPegawai jenisPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKategoryPegawai", referencedColumnName = "KdKategoryPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KategoryPegawai kategoryPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKelompokUser", referencedColumnName = "KdKelompokUser", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KelompokUser kelompokUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKualifikasiJurusan", referencedColumnName = "KdKualifikasiJurusan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private KualifikasiJurusan kualifikasiJurusan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdLevelTingkat", referencedColumnName = "KdLevelTingkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private LevelTingkat levelTingkat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Negara negara;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganPegawai", referencedColumnName = "KdGolonganPegawai", insertable = false, updatable = false),
			@JoinColumn(name = "KdPangkat", referencedColumnName = "KdPangkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private MapGolonganPegawaiToPangkat mapGolonganPegawaiToPangkat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdSuku", referencedColumnName = "KdSuku", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Suku suku;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdTypePegawai", referencedColumnName = "KdTypePegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private TypePegawai typePegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRekananInstitusiAsal", referencedColumnName = "KdRekanan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Rekanan rekananInstitusiAsal;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPendidikanTerakhir", referencedColumnName = "KdPendidikan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pendidikan pendidikanTerakhir;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisPegawaiLamar", referencedColumnName = "KdJenisPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private JenisPegawai jenisPegawaiLamar;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJabatanLamar", referencedColumnName = "KdJabatan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Jabatan jabatanLamar;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRangeTotalNilaiScore", referencedColumnName = "KdRange", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Range rangeTotalNilaiScore;

	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiHead", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private Pegawai pegawaiHead;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiPembimbing", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pegawai pegawaiPembimbing;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRuanganKerja", referencedColumnName = "kdRuangan", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Ruangan ruanganKerja;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdTitle", referencedColumnName = "KdTitle", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private TitlePegawai titlePegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false, nullable = true),
			@JoinColumn(name = "KdStatusPerkawinan", referencedColumnName = "KdStatus", insertable = false, updatable = false, nullable = true) })
	// @ForeignKey(name="none")
	private Status statusPerkawinan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false, nullable = true),
			@JoinColumn(name = "KdStatusPegawai", referencedColumnName = "KdStatus", insertable = false, updatable = false, nullable = true), })
	// @ForeignKey(name="none")
	private Status statusPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "NoHistori", referencedColumnName = "NoHistori", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private StrukHistori strukHistori;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPangkat", referencedColumnName = "KdPangkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")//
	private Pangkat pangkat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganPegawai", referencedColumnName = "KdGolonganPegawai", insertable = false, updatable = false, nullable = true), })
	// @ForeignKey(name="none")
	private GolonganPegawai golonganPegawai;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPTKP", referencedColumnName = "KdPTKP", insertable = false, updatable = false, nullable = true), })
	// @ForeignKey(name="none")
	private PenghasilanTidakKenaPajak penghasilanTidakKenaPajak;
	
	

}
