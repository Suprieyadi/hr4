package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.StrukClosingId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "StrukClosing_T",
indexes = {
			@Index(name = "StrukClosing_T_Index1",  columnList="statusEnabled", unique = false),
			@Index(name = "StrukClosing_T_Index2",  
				columnList="TglClosing, NoUrutLogin, NoUrutRuangan, KdKelompokTransaksi", unique = false)
		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukClosing {
    
    @EmbeddedId
    private StrukClosingId id ;
    
    private static final String cDefTglClosing = INTEGER ;
    @Column(name = "TglClosing", columnDefinition = cDefTglClosing)
    @NotNull(message = "strukclosing.tglclosing.notnull")
    private Long  tglClosing;
    
    private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuangan", columnDefinition = cDefKdRuangan)
    @NotNull(message = "strukclosing.kdruangan.notnull")
    private String kdRuangan;
    
    private static final String cDefTglAwal = INTEGER ;
    @Column(name = "TglAwal", columnDefinition = cDefTglAwal)
    private Long  tglAwal;
    
    private static final String cDefTglAkhir = INTEGER ;
    @Column(name = "TglAkhir", columnDefinition = cDefTglAkhir)
    private Long  tglAkhir;
    
    private static final String cDefKdKelompokTransaksi =TINYINT;
    @Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
    @NotNull(message = "strukclosing.kdkelompoktransaksi.notnull")
    private Integer kdKelompokTransaksi;
    
    private static final String cDefKdRuanganDiClose = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuanganDiClose", columnDefinition = cDefKdRuanganDiClose)
    private String kdRuanganDiClose;
    
    private static final String cDefKdPegawaiDiClose = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawaiDiClose", columnDefinition = cDefKdPegawaiDiClose)
    private String kdPegawaiDiClose;
    
    private static final String cDefTotalDeposit = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalDeposit", columnDefinition = cDefTotalDeposit)
    private Double totalDeposit;
    
    private static final String cDefTotalDiBayar = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalDiBayar", columnDefinition = cDefTotalDiBayar)
    private Double totalDiBayar;
    
    private static final String cDefTotalDiBayarClose = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalDiBayarClose", columnDefinition = cDefTotalDiBayarClose)
    private Double totalDiBayarClose;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukclosing.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoUrutLogin = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutLogin", columnDefinition = cDefNoUrutLogin)
    private String noUrutLogin;
    
    private static final String cDefNoUrutRuangan = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutRuangan", columnDefinition = cDefNoUrutRuangan)
    private String noUrutRuangan;
    
    private static final String cDefNoClosingBefore = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoClosingBefore", columnDefinition = cDefNoClosingBefore)
    private String noClosingBefore;
    
    private static final String cDefKdLevelTingkat =TINYINT;
    @Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
    private Integer kdLevelTingkat;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukclosing.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukclosing.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoClosing", referencedColumnName="NoClosing",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukClosing strukClosing;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdRuangan", referencedColumnName="KdRuangan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Ruangan ruangan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdKelompokTransaksi", referencedColumnName="KdKelompokTransaksi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KelompokTransaksi kelompokTransaksi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdLevelTingkat", referencedColumnName="KdLevelTingkat",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private LevelTingkat levelTingkat;
}
