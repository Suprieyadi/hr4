package com.jasamedika.medifirst2000.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.StrukPelayananId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*;


@Entity
@Table(name = "StrukPelayanan_T",
	indexes = {
			@Index(name = "StrukPelayanan_T_Index1",  
					columnList="statusEnabled", unique = false),
			@Index(name = "StrukPelayanan_T_Index2",  
			columnList="TglStruk, NoUrutLogin, NoUrutRuangan, KdKelompokTransaksi", unique = false)
		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukPelayanan {
    
    @EmbeddedId
    private StrukPelayananId id ;
    
    private static final String cDefNoStrukIntern = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "NoStrukIntern", columnDefinition = cDefNoStrukIntern)
    private String noStrukIntern;
    
    private static final String cDefTglStruk = INTEGER ;
    @Column(name = "TglStruk", columnDefinition = cDefTglStruk)
    @NotNull(message = "strukpelayanan.tglstruk.notnull")
    private Long  tglStruk;
    
    private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuangan", columnDefinition = cDefKdRuangan)
    @NotNull(message = "strukpelayanan.kdruangan.notnull")
    private String kdRuangan;
    
    private static final String cDefKdAntrian =INTEGER;
    @Column(name = "KdAntrian", columnDefinition = cDefKdAntrian)
    private Integer kdAntrian;
    
    private static final String cDefNoMasuk = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoMasuk", columnDefinition = cDefNoMasuk)
    private String noMasuk;
    
    private static final String cDefNoRegistrasi = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoRegistrasi", columnDefinition = cDefNoRegistrasi)
    private String noRegistrasi;
    
    private static final String cDefNoCM = VARCHAR + AWAL_KURUNG + 15 + AKHIR_KURUNG ;
    @Column(name = "NoCM", columnDefinition = cDefNoCM)
    private String noCM;
    
    private static final String cDefNamaPasienKlien = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaPasienKlien", columnDefinition = cDefNamaPasienKlien)
    private String namaPasienKlien;
    
    private static final String cDefKdRuanganAsal = VARCHAR + AWAL_KURUNG + 3 + AKHIR_KURUNG ;
    @Column(name = "KdRuanganAsal", columnDefinition = cDefKdRuanganAsal)
    @NotNull(message = "strukpelayanan.kdruanganasal.notnull")
    private String kdRuanganAsal;
    
    private static final String cDefKdRekanan =SMALLINT;
    @Column(name = "KdRekanan", columnDefinition = cDefKdRekanan)
    private Integer kdRekanan;
    
    private static final String cDefNamaRekanan = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaRekanan", columnDefinition = cDefNamaRekanan)
    private String namaRekanan;
    
    private static final String cDefKdAlamatRekanan =INTEGER;
    @Column(name = "KdAlamatRekanan", columnDefinition = cDefKdAlamatRekanan)
    private Integer kdAlamatRekanan;
    
    private static final String cDefKdRekananSales =SMALLINT;
    @Column(name = "KdRekananSales", columnDefinition = cDefKdRekananSales)
    private Integer kdRekananSales;
    
    private static final String cDefNamaRekananSales = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaRekananSales", columnDefinition = cDefNamaRekananSales)
    private String namaRekananSales;
    
    private static final String cDefKdAlamatRekananSales =INTEGER;
    @Column(name = "KdAlamatRekananSales", columnDefinition = cDefKdAlamatRekananSales)
    private Integer kdAlamatRekananSales;
    
    private static final String cDefNoFaktur = VARCHAR + AWAL_KURUNG + 20 + AKHIR_KURUNG ;
    @Column(name = "NoFaktur", columnDefinition = cDefNoFaktur)
    private String noFaktur;
    
    private static final String cDefTglFaktur = INTEGER ;
    @Column(name = "TglFaktur", columnDefinition = cDefTglFaktur)
    private Long  tglFaktur;
    
    private static final String cDefNamaUserFaktur = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaUserFaktur", columnDefinition = cDefNamaUserFaktur)
    private String namaUserFaktur;
    
    private static final String cDefTglJatuhTempo = INTEGER ;
    @Column(name = "TglJatuhTempo", columnDefinition = cDefTglJatuhTempo)
    private Long  tglJatuhTempo;
    
    private static final String cDefKdMetodeDelivery =TINYINT;
    @Column(name = "KdMetodeDelivery", columnDefinition = cDefKdMetodeDelivery)
    private Integer kdMetodeDelivery;
    
    private static final String cDefNamaKurirPengirim = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaKurirPengirim", columnDefinition = cDefNamaKurirPengirim)
    private String namaKurirPengirim;
    
    private static final String cDefKdRekananKurir =SMALLINT;
    @Column(name = "KdRekananKurir", columnDefinition = cDefKdRekananKurir)
    private Integer kdRekananKurir;
    
    private static final String cDefNamaRekananKurir = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaRekananKurir", columnDefinition = cDefNamaRekananKurir)
    private String namaRekananKurir;
    
    private static final String cDefTglTerimaKiriman = INTEGER ;
    @Column(name = "TglTerimaKiriman", columnDefinition = cDefTglTerimaKiriman)
    private Long  tglTerimaKiriman;
    
    private static final String cDefTotalBeratKg = FLOAT ;
    @Column(name = "TotalBeratKg", columnDefinition = cDefTotalBeratKg)
    private Float totalBeratKg;
    
    private static final String cDefQtyProduk = FLOAT ;
    @Column(name = "QtyProduk", columnDefinition = cDefQtyProduk)
    @NotNull(message = "strukpelayanan.qtyproduk.notnull")
    private Float qtyProduk;
    
    private static final String cDefQtyDetailJenisProduk = FLOAT ;
    @Column(name = "QtyDetailJenisProduk", columnDefinition = cDefQtyDetailJenisProduk)
    @NotNull(message = "strukpelayanan.qtydetailjenisproduk.notnull")
    private Float qtyDetailJenisProduk;
    
    private static final String cDefTotalHargaSatuan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHargaSatuan", columnDefinition = cDefTotalHargaSatuan)
    @NotNull(message = "strukpelayanan.totalhargasatuan.notnull")
    private java.math.BigDecimal totalHargaSatuan;
    
    private static final String cDefTotalDiscount = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalDiscount", columnDefinition = cDefTotalDiscount)
    @NotNull(message = "strukpelayanan.totaldiscount.notnull")
    private java.math.BigDecimal totalDiscount;
    
    private static final String cDefTotalDiscountSave = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalDiscountSave", columnDefinition = cDefTotalDiscountSave)
    @NotNull(message = "strukpelayanan.totaldiscountsave.notnull")
    private java.math.BigDecimal totalDiscountSave;
    
    private static final String cDefTotalDiscountGive = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalDiscountGive", columnDefinition = cDefTotalDiscountGive)
    @NotNull(message = "strukpelayanan.totaldiscountgive.notnull")
    private java.math.BigDecimal totalDiscountGive;
    
    private static final String cDefTotalPPN = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalPPN", columnDefinition = cDefTotalPPN)
    @NotNull(message = "strukpelayanan.totalppn.notnull")
    private java.math.BigDecimal totalPPN;
    
    private static final String cDefTotalPPH = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalPPH", columnDefinition = cDefTotalPPH)
    @NotNull(message = "strukpelayanan.totalpph.notnull")
    private java.math.BigDecimal totalPPH;
    
    private static final String cDefTotalBeaMaterai = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalBeaMaterai", columnDefinition = cDefTotalBeaMaterai)
    @NotNull(message = "strukpelayanan.totalbeamaterai.notnull")
    private java.math.BigDecimal totalBeaMaterai;
    
    private static final String cDefTotalBiayaKirim = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalBiayaKirim", columnDefinition = cDefTotalBiayaKirim)
    @NotNull(message = "strukpelayanan.totalbiayakirim.notnull")
    private java.math.BigDecimal totalBiayaKirim;
    
    private static final String cDefTotalBiayaTambahan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalBiayaTambahan", columnDefinition = cDefTotalBiayaTambahan)
    @NotNull(message = "strukpelayanan.totalbiayatambahan.notnull")
    private java.math.BigDecimal totalBiayaTambahan;
    
    private static final String cDefTotalPRekanan = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalPRekanan", columnDefinition = cDefTotalPRekanan)
    @NotNull(message = "strukpelayanan.totalprekanan.notnull")
    private java.math.BigDecimal totalPRekanan;
    
    private static final String cDefTotalPProfile = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalPProfile", columnDefinition = cDefTotalPProfile)
    @NotNull(message = "strukpelayanan.totalpprofile.notnull")
    private java.math.BigDecimal totalPProfile;
    
    private static final String cDefTotalHarusDiBayar = BIGDECIMAL + AWAL_KURUNG + "15,2" + AKHIR_KURUNG ;
    @Column(name = "TotalHarusDiBayar", columnDefinition = cDefTotalHarusDiBayar)
    @NotNull(message = "strukpelayanan.totalharusdibayar.notnull")
    private java.math.BigDecimal totalHarusDiBayar;
    
    private static final String cDefNoOrder = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoOrder", columnDefinition = cDefNoOrder)
    private String noOrder;
    
    private static final String cDefNoPlanning = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoPlanning", columnDefinition = cDefNoPlanning)
    private String noPlanning;
    
    private static final String cDefisDelivered =TINYINT;
    @Column(name = "isDelivered", columnDefinition = cDefisDelivered)
    @NotNull(message = "strukpelayanan.isdelivered.notnull")
    private Integer isDelivered;
    
    private static final String cDefNoSBMLast = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSBMLast", columnDefinition = cDefNoSBMLast)
    private String noSBMLast;
    
    private static final String cDefNoSBKLast = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoSBKLast", columnDefinition = cDefNoSBKLast)
    private String noSBKLast;
    
    private static final String cDefKdPegawaiPenerima = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "KdPegawaiPenerima", columnDefinition = cDefKdPegawaiPenerima)
    private String kdPegawaiPenerima;
    
    private static final String cDefNamaPegawaiPenerima = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaPegawaiPenerima", columnDefinition = cDefNamaPegawaiPenerima)
    private String namaPegawaiPenerima;
    
    private static final String cDefisPickup =TINYINT;
    @Column(name = "isPickup", columnDefinition = cDefisPickup)
    @NotNull(message = "strukpelayanan.ispickup.notnull")
    private Integer isPickup;
    
    private static final String cDefKdAlamatTujuan =INTEGER;
    @Column(name = "KdAlamatTujuan", columnDefinition = cDefKdAlamatTujuan)
    private Integer kdAlamatTujuan;
    
    private static final String cDefKdKelasLast = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdKelasLast", columnDefinition = cDefKdKelasLast)
    private String kdKelasLast;
    
    private static final String cDefKdKelasKamarLast = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG ;
    @Column(name = "KdKelasKamarLast", columnDefinition = cDefKdKelasKamarLast)
    private String kdKelasKamarLast;
    
    private static final String cDefKdKamarLast =SMALLINT;
    @Column(name = "KdKamarLast", columnDefinition = cDefKdKamarLast)
    private Integer kdKamarLast;
    
    private static final String cDefNoBedLast =TINYINT;
    @Column(name = "NoBedLast", columnDefinition = cDefNoBedLast)
    private Integer noBedLast;
    
    private static final String cDefQtyOrangLast =TINYINT;
    @Column(name = "QtyOrangLast", columnDefinition = cDefQtyOrangLast)
    private Integer qtyOrangLast;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefStatusEnabled =TINYINT;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukpelayanan.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefKdKelompokTransaksi =TINYINT;
    @Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
    @NotNull(message = "strukpelayanan.kdkelompoktransaksi.notnull")
    private Integer kdKelompokTransaksi;
    
    private static final String cDefNoClosing = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoClosing", columnDefinition = cDefNoClosing)
    private String noClosing;
    
    private static final String cDefNoUrutLogin = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutLogin", columnDefinition = cDefNoUrutLogin)
    private String noUrutLogin;
    
    private static final String cDefNoUrutRuangan = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoUrutRuangan", columnDefinition = cDefNoUrutRuangan)
    private String noUrutRuangan;
    
    private static final String cDefNoStrukBefore = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoStrukBefore", columnDefinition = cDefNoStrukBefore)
    private String noStrukBefore;
    
    private static final String cDefKdLevelTingkat =TINYINT;
    @Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
    private Integer kdLevelTingkat;
    
    private static final String cDefNoHistori = CHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "NoHistori", columnDefinition = cDefNoHistori)
    private String noHistori;
    
    private static final String cDefTglAmbil = INTEGER ;
    @Column(name = "TglAmbil", columnDefinition = cDefTglAmbil)
    private Long  tglAmbil;
    
    private static final String cDefNamaLengkapAmbil = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG ;
    @Column(name = "NamaLengkapAmbil", columnDefinition = cDefNamaLengkapAmbil)
    private String namaLengkapAmbil;
    
    private static final String cDefKdHubunganKeluarga =TINYINT;
    @Column(name = "KdHubunganKeluarga", columnDefinition = cDefKdHubunganKeluarga)
    private Integer kdHubunganKeluarga;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukpelayanan.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =SMALLINT;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukpelayanan.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdRuangan", referencedColumnName="KdRuangan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Ruangan ruangan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdRekanan", referencedColumnName="KdRekanan",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private Rekanan rekanan;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdMetodeDelivery", referencedColumnName="KdMetodeDelivery",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private MetodeDelivery metodeDelivery;
    
   /* @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoOrder", referencedColumnName="NoOrder",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukOrder strukOrder;*/
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoPlanning", referencedColumnName="NoPlanning",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukPlanning strukPlanning;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdKelompokTransaksi", referencedColumnName="KdKelompokTransaksi",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private KelompokTransaksi kelompokTransaksi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoClosing", referencedColumnName="NoClosing",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukClosing strukClosing;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoHistori", referencedColumnName="NoHistori",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukHistori strukHistori;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdLevelTingkat", referencedColumnName="KdLevelTingkat",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private LevelTingkat levelTingkat;
    
    /*@ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="NoHistori", referencedColumnName="NoHistori",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private StrukHistori strukHistori;*/
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdHubunganKeluarga", referencedColumnName="KdHubunganKeluarga",insertable=false, updatable=false),
    })
    // @ForeignKey(name="none")
     private HubunganKeluarga hubunganKeluarga;
}
