package com.jasamedika.medifirst2000.entity;

import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.SMALLINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.TINYINT;
import static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jasamedika.medifirst2000.entity.vo.MapGolonganPegawaiToPangkatId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "MapGolonganPegawaiToPangkat_M", indexes = { @Index(columnList = "StatusEnabled", name = "statusMapGolonganPegawaiToPangkat_M") })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MapGolonganPegawaiToPangkat {
	
	@EmbeddedId
	private MapGolonganPegawaiToPangkatId id;
	
	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "mapGolonganpegawaitopangkat.kddepartemen.notnull")
	private String kdDepartemen;
	
	private static final String cDefVersion =SMALLINT;
	@Version
	@Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;
	
	private static final String cDefStatusEnabled = TINYINT ;// AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "StatusEnabled", length = 1, precision = 1, scale = 0, columnDefinition = cDefStatusEnabled)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public Boolean statusEnabled;
	
	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "NoRec Harus Diisi")
	private String noRec;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Profile profile;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Departemen departemen;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganPegawai", referencedColumnName = "KdGolonganPegawai", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private GolonganPegawai golonganPegawai;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPangkat", referencedColumnName = "KdPangkat", insertable = false, updatable = false), })
	// @ForeignKey(name="none")
	private Pangkat pangkat;

}
