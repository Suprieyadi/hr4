package com.jasamedika.medifirst2000.Other;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HandlerMasterException extends RuntimeException {
	
	private int status;
	
	public HandlerMasterException(String msg, int status) {
		super("Status Http Error : " + status + " - Message : " + msg);
		this.status = status;
	}
	
	
}
