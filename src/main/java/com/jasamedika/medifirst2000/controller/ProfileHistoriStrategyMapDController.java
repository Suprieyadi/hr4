package com.jasamedika.medifirst2000.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jasamedika.medifirst2000.base.LocaleController;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriStrategyMapDDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriStrategyMapDDto;
import com.jasamedika.medifirst2000.security.AppPermission;
import com.jasamedika.medifirst2000.service.ProfileHistoriStrategyMapDService;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/profilehistoristrategymapd")
public class ProfileHistoriStrategyMapDController extends LocaleController {
	
	@Lazy
	@Autowired
	private ProfileHistoriStrategyMapDService profileHistoriStrategyMapDService;
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menyimpan ProfileHistoriStrategyMapD")
	@AppPermission(AppPermission.SAVE)
	@PostMapping("/save")
	public Map<String, Object> save(@Valid @RequestBody DataProfileHistoriStrategyMapDDto entity,
			HttpServletRequest request) {
		Map<String, Object> result = profileHistoriStrategyMapDService.saveList(entity);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Update ProfileHistoriStrategyMapD")
	@AppPermission(AppPermission.UPDATE)
	@PutMapping("/update/{version}")
	public Map<String, Object> update(@PathVariable("version") Integer version, 
			@Valid @RequestBody ProfileHistoriStrategyMapDDto entity, HttpServletRequest request) {
		Map<String, Object> result = profileHistoriStrategyMapDService.update(entity, version);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menghapus ProfileHistoriStrategyMapD Berdasarkan noHistori")
	@AppPermission(AppPermission.DELETE)
	@DeleteMapping("/del/{noHistori}")
	public Map<String, Object> deleteById(@PathVariable("noHistori") String noHistori) {
		Map<String, Object> result = profileHistoriStrategyMapDService.deleteByKode(noHistori);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan Semua ProfileHistoriStrategyMapD")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findAll")
	public Map<String, Object> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "id.kdDepartemenD") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "id.kdDepartemenD", required = false) String kdDepartemenD,
			@RequestParam(value = "id.kdPerspectiveD", required = false) Integer kdPerspectiveD) {
		Map<String, Object> result = profileHistoriStrategyMapDService.findAll(page, rows, sort, dir, kdDepartemenD, kdPerspectiveD);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriStrategyMapD Berdasarkan Period dan kdDepartemenD")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByPeriod/{startDate}/{endDate}/{kdDepartemenD}")
	public Map<String, Object> findByPeriod(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "kdPerspective") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "startDate", required = true) Long startDate, 
			@RequestParam(value = "endDate", required = true) Long endDate,
			@RequestParam(value = "kdDepartemenD", required = false) String kdDepartemenD){
		Map<String, Object> result = profileHistoriStrategyMapDService.findByPeriod(page, rows, sort, dir, startDate, endDate, kdDepartemenD);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriStrategyMapD Berdasarkan noHistori")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByKode/{noHistori}")
	public Map<String, Object> findByKode(@PathVariable("noHistori") String noHistori, HttpServletRequest request) {
		Map<String, Object> result = profileHistoriStrategyMapDService.findByKode(noHistori);
		return result;
	}

}
