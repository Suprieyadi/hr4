package com.jasamedika.medifirst2000.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jasamedika.medifirst2000.base.LocaleController;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriStrategyMapDKPIDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriStrategyMapDKPIDto;
import com.jasamedika.medifirst2000.security.AppPermission;
import com.jasamedika.medifirst2000.service.ProfileHistoriStrategyMapDKPIService;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/profilehistoristrategymapdkpi")
public class ProfileHistoriStrategyMapDKPIController extends LocaleController {
	
	@Lazy
	@Autowired
	private ProfileHistoriStrategyMapDKPIService profileHistoriStrategyMapDKPIService;
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menyimpan ProfileHistoriStrategyMapDKPI")
	@AppPermission(AppPermission.SAVE)
	@PostMapping("/save")
	public Map<String, Object> save(@Valid @RequestBody DataProfileHistoriStrategyMapDKPIDto entity,
			HttpServletRequest request) {
		Map<String, Object> result = profileHistoriStrategyMapDKPIService.saveList(entity);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Update ProfileHistoriStrategyMapDKPI")
	@AppPermission(AppPermission.UPDATE)
	@PutMapping("/update/{version}")
	public Map<String, Object> update(@PathVariable("version") Integer version, 
			@Valid @RequestBody ProfileHistoriStrategyMapDKPIDto entity, HttpServletRequest request) {
		Map<String, Object> result = profileHistoriStrategyMapDKPIService.update(entity, version);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menghapus ProfileHistoriStrategyMapDKPI Berdasarkan noHistori")
	@AppPermission(AppPermission.DELETE)
	@DeleteMapping("/del/{noHistori}")
	public Map<String, Object> deleteById(@PathVariable("noHistori") String noHistori) {
		Map<String, Object> result = profileHistoriStrategyMapDKPIService.deleteByKode(noHistori);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan Semua ProfileHistoriStrategyMapDKPI")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findAll")
	public Map<String, Object> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "id.kdDepartemenD") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "id.kdDepartemenD", required = false) String kdDepartemenD) {
		Map<String, Object> result = profileHistoriStrategyMapDKPIService.findAll(page, rows, sort, dir, kdDepartemenD);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriStrategyMapD Berdasarkan Period dan kdDepartemenDKPI")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByPeriod/{startDate}/{endDate}/{kdDepartemenD}")
	public Map<String, Object> findByPeriod(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "kdPerspective") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "startDate", required = true) Long startDate, 
			@RequestParam(value = "endDate", required = true) Long endDate,
			@RequestParam(value = "kdDepartemenD", required = false) String kdDepartemenD){
		Map<String, Object> result = profileHistoriStrategyMapDKPIService.findByPeriod(page, rows, sort, dir, startDate, endDate, kdDepartemenD);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriStrategyMapDKPI Berdasarkan noHistori")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByKode/{noHistori}")
	public Map<String, Object> findByKode(@PathVariable("noHistori") String noHistori, HttpServletRequest request) {
		Map<String, Object> result = profileHistoriStrategyMapDKPIService.findByKode(noHistori);
		return result;
	}

}
