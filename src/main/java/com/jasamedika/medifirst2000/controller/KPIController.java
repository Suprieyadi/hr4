package com.jasamedika.medifirst2000.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jasamedika.medifirst2000.base.LocaleController;
import com.jasamedika.medifirst2000.dto.KPIDto;
import com.jasamedika.medifirst2000.security.AppPermission;
import com.jasamedika.medifirst2000.service.KPIService;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/kpi")
public class KPIController extends LocaleController {
	
	@Lazy
	@Autowired
	private KPIService kpiService;

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menyimpan KPI")
	@AppPermission(AppPermission.SAVE)
	@PostMapping("/save")
	public Map<String, Object> save(@Valid @RequestBody KPIDto entity, HttpServletRequest request) {
		Map<String, Object> result = kpiService.save(entity);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Update KPI")
	@AppPermission(AppPermission.UPDATE)
	@PutMapping("/update/{version}")
	public Map<String, Object> update(@PathVariable("version") Integer version, @Valid @RequestBody KPIDto entity,
			HttpServletRequest request) {
		Map<String, Object> result = kpiService.update(entity, version);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menghapus KPI Berdasarkan KdKPI")
	@AppPermission(AppPermission.DELETE)
	@DeleteMapping("/del/{kode}")
	public Map<String, Object> deleteById(@PathVariable("kode") Integer kode) {
		Map<String, Object> result = kpiService.deleteByKode(kode);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan Semua KPI")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findAll")
	public Map<String, Object> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "namaKPI") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "namaKPI", required = false) String namaKPI) {
		Map<String, Object> result = kpiService.findAll(page, rows, sort, dir, namaKPI);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan KPI Berdasarkan kode")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByKode/{kode}")
	public Map<String, Object> findByKode(@PathVariable("kode") Integer kode, HttpServletRequest request) {
		Map<String, Object> result = kpiService.findByKode(kode);
		return result;
	}

}
