package com.jasamedika.medifirst2000.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jasamedika.medifirst2000.security.AppPermission;
import com.jasamedika.medifirst2000.service.GenericModelService;
import com.jasamedika.medifirst2000.util.CommonUtil;

import io.swagger.annotations.ApiOperation;

@Lazy
@RestController
@RequestMapping("/service")
public class GenericModelController {
	
	@Lazy
	@Autowired
	private GenericModelService genericModelService;

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan Data Berdasarkan Nama Tabel, paging, dan criteria pencarian")
	@AppPermission(AppPermission.VIEW)
	@GetMapping("/list-generic")
	public Map<String, Object> listGeneric(@RequestParam(value = "table", required = true) String entity,
			@RequestParam(value = "select", required = true, defaultValue = "*") String field,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "300") Integer rows,
			@RequestParam(value = "criteria", required = false) String criteria,
			@RequestParam(value = "values", required = false) String values,
			@RequestParam(value = "condition", defaultValue = "and", required = false) String condition,
			@RequestParam(value = "profile", defaultValue = "y", required = false) String useProfile) {

		Map<String, Object> modelGenericVO = CommonUtil.createMap();
		modelGenericVO.put("data",
				genericModelService.getAllData(entity, field, page, rows, criteria, values, condition, useProfile));

		return modelGenericVO;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan Data Berdasarkan Nama Tabel, paging, dan criteria pencarian")
	@AppPermission(AppPermission.VIEW)
	@GetMapping("/list-generic-page")
	public Map<String, Object> listGenericPage(@RequestParam(value = "table", required = true) String entity,
			@RequestParam(value = "select", required = true, defaultValue = "*") String field,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "criteria", required = false) String criteria,
			@RequestParam(value = "values", required = false) String values,
			@RequestParam(value = "condition", defaultValue = "and", required = false) String condition) {

		Map<String, Object> modelGenericVO = CommonUtil.createMap();
		modelGenericVO.put("data",
				genericModelService.getAllData(entity, field, page, rows, criteria, values, condition, "y"));

		return modelGenericVO;
	}

}
