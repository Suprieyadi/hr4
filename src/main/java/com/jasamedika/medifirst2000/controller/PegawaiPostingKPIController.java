package com.jasamedika.medifirst2000.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jasamedika.medifirst2000.base.LocaleController;
import com.jasamedika.medifirst2000.dto.DataPegawaiPostingKPIDto;
import com.jasamedika.medifirst2000.dto.PegawaiPostingKPIDto;
import com.jasamedika.medifirst2000.security.AppPermission;
import com.jasamedika.medifirst2000.service.PegawaiPostingKPIService;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/pegawaipostingkpi")
public class PegawaiPostingKPIController extends LocaleController {
	
	@Lazy
	@Autowired
	private PegawaiPostingKPIService pegawaiPostingKPIService;
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menyimpan PegawaiPostingKPI")
	@AppPermission(AppPermission.SAVE)
	@PostMapping("/save")
	public Map<String, Object> save(@Valid @RequestBody DataPegawaiPostingKPIDto entity,
			HttpServletRequest request) {
		Map<String, Object> result = pegawaiPostingKPIService.saveList(entity);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Update PegawaiPostingKPI")
	@AppPermission(AppPermission.UPDATE)
	@PutMapping("/update/{version}")
	public Map<String, Object> update(@PathVariable("version") Integer version, 
			@Valid @RequestBody PegawaiPostingKPIDto entity, HttpServletRequest request) {
		Map<String, Object> result = pegawaiPostingKPIService.update(entity, version);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menghapus PegawaiPostingKPI Berdasarkan noPosting")
	@AppPermission(AppPermission.DELETE)
	@DeleteMapping("/del/{noPosting}")
	public Map<String, Object> deleteById(@PathVariable("noPosting") String noPosting) {
		Map<String, Object> result = pegawaiPostingKPIService.deleteByKode(noPosting);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan Semua PegawaiPostingKPI")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findAll")
	public Map<String, Object> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "id.kdDepartemenD") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "id.kdDepartemenD", required = false) String kdDepartemenD) {
		Map<String, Object> result = pegawaiPostingKPIService.findAll(page, rows, sort, dir, kdDepartemenD);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan PegawaiPostingKPI Berdasarkan Period, kdDepartemenD dan kdPegawai")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByPeriod/{startDate}/{endDate}/{kdDepartemenD}/{kdPegawai}")
	public Map<String, Object> findByPeriod(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "id.kdDepartemenD") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "startDate", required = true) Long startDate, 
			@RequestParam(value = "endDate", required = true) Long endDate,
			@RequestParam(value = "id.kdDepartemenD", required = false) String kdDepartemenD,
			@RequestParam(value = "id.kdPegawai", required = false) String kdPegawai) {
		Map<String, Object> result = pegawaiPostingKPIService.findByPeriod(page, rows, sort, dir, startDate, endDate, kdDepartemenD, kdPegawai);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan PegawaiPostingKPI Berdasarkan noPosting")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByKode/{noPosting}")
	public Map<String, Object> findByKode(@PathVariable("noPosting") String noPosting, HttpServletRequest request) {
		Map<String, Object> result = pegawaiPostingKPIService.findByKode(noPosting);
		return result;
	}

}
