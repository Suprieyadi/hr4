package com.jasamedika.medifirst2000.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jasamedika.medifirst2000.base.LocaleController;
import com.jasamedika.medifirst2000.dto.DataProfileHistoriVisiMisiDto;
import com.jasamedika.medifirst2000.dto.ProfileHistoriVisiMisiDto;
import com.jasamedika.medifirst2000.security.AppPermission;
import com.jasamedika.medifirst2000.service.ProfileHistoriVisiMisiService;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/profilehistorivisimisi")
public class ProfileHistoriVisiMisiController extends LocaleController {
	
	@Lazy
	@Autowired
	private ProfileHistoriVisiMisiService profileHistoriVisiMisiService;
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menyimpan ProfileHistoriVisiMisi")
	@AppPermission(AppPermission.SAVE)
	@PostMapping("/save")
	public Map<String, Object> save(@Valid @RequestBody DataProfileHistoriVisiMisiDto entity,
			HttpServletRequest request) {
		Map<String, Object> result = profileHistoriVisiMisiService.saveList(entity);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Update ProfileHistoriVisiMisi")
	@AppPermission(AppPermission.UPDATE)
	@PutMapping("/update/{version}")
	public Map<String, Object> update(@PathVariable("version") Integer version, 
			@Valid @RequestBody DataProfileHistoriVisiMisiDto entity, HttpServletRequest request) {
		Map<String, Object> result = profileHistoriVisiMisiService.update(entity, version);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menghapus ProfileHistoriVisiMisi Berdasarkan noHistori dan kdMisi")
	@AppPermission(AppPermission.DELETE)
	@DeleteMapping("/del/{noHistori}")
	public Map<String, Object> deleteById(@PathVariable("noHistori") String noHistori) {
		Map<String, Object> result = profileHistoriVisiMisiService.deleteByKode(noHistori);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriVisiMisi Berdasarkan tglAwal")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findPeriodGroup")
	public Map<String, Object> findPeriodGroup(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "strukHistori.tglAwal") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "startDate", required = false) Long startDate,
			@RequestParam(value = "endDate", required = false) Long endDate
			) {
		Map<String, Object> result = profileHistoriVisiMisiService.findPeriodGroup(page, rows, sort, dir,startDate,endDate);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriVisiMisi Berdasarkan Period")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByPeriod/{startDate}/{endDate}")
	public Map<String, Object> findByPeriod(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(value = "dir", defaultValue = "keteranganLainnya") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "startDate", required = true) Long startDate, 
			@RequestParam(value = "endDate", required = true) Long endDate) {
		Map<String, Object> result = profileHistoriVisiMisiService.findByPeriod(page, rows, sort, dir, startDate, endDate);
		return result;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation("Api Untuk Menampilkan ProfileHistoriVisiMisi Berdasarkan noHistori")
	@AppPermission(AppPermission.SPECIALS)
	@GetMapping("/findByKode/{noHistori}")
	public Map<String, Object> findByKode(@PathVariable("noHistori") String noHistori, HttpServletRequest request) {
		Map<String, Object> result = profileHistoriVisiMisiService.findByKode(noHistori);
		return result;
	}

}
