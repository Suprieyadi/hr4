package com.jasamedika.medifirst2000;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
/*@EnableDiscoveryClient
@EnableEurekaClient*/
@EnableCaching(proxyTargetClass=true)
@EnableAsync(proxyTargetClass=true)
public class Hr4Microservice {
	public static void main(String[] args) {
		SpringApplication.run(Hr4Microservice.class, args);
	}
 
}