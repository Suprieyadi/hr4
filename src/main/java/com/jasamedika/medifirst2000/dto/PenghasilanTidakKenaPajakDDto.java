/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 08/12/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PenghasilanTidakKenaPajakDDto {

	private Integer kdPTKP;

	@NotNull(message = "PenghasilanTidakKenaPajakDDto.nosk.notnull")
	private String noSK;

	@NotNull(message = "PenghasilanTidakKenaPajakDDto.kdkomponenharga.notnull")
	private Integer kdKomponenHarga;

	private Float persenHargaSatuan;

	private java.math.BigDecimal hargaSatuan;

	private Float factorRate;

	private String operatorFactorRate;

	private java.math.BigDecimal hargaSatuanMax;

	private String kdDepartemen;

	@NotNull(message = "penghasilantidakkenapajakd.statusenabled.notnull")
	private Boolean statusEnabled;

	private String noRec;

	private Integer version;

}
