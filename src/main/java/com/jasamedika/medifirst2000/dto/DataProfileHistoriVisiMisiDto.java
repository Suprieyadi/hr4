package com.jasamedika.medifirst2000.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataProfileHistoriVisiMisiDto {
	
	@NotNull(message = "dataprofilehistorivisimisi.tglawal.notnull")
    private Long tglAwal;
    
    @NotNull(message = "dataprofilehistorivisimisi.tglakhir.notnull")
    private Long tglAkhir;
    
    @NotNull(message = "dataprofilehistorivisimisi.tglhistori.notnull")
    private Long tglHistori;
	
	List<ProfileHistoriVisiMisiDto> profileHistoriVisiMisi;
	
	private String noHistori;
	
	private Boolean statusEnabled;


}
