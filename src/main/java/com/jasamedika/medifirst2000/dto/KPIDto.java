package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KPIDto {
	
	private Integer kode;
    
    @NotNull(message = "kpi.namaKPI.notnull")
    private String namaKPI;
    
    private Integer kdKPIHead;
    
    private Integer noUrut;
    
    @NotNull(message = "kpi.kdtypedataobjek.notnull")
    private Integer kdTypeDataObjek;
    
    @NotNull(message = "kpi.kdsatuanhasil.notnull")
    private Integer kdSatuanHasil;
    
    @NotNull(message = "kpi.kdmetodehitungscore.notnull")
    private Integer kdMetodeHitungScore;
    
    @NotNull(message = "kpi.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    private String kdDepartemen;
    
    @NotNull(message = "kpi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;

}
