/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class JabatanDto {
    
    private String kode;
    
    @NotNull(message = "jabatan.namajabatan.notnull")
    private String namaJabatan;
    
    @NotNull(message = "jabatan.reportdisplay.notnull")
    private String reportDisplay;
    
    @NotNull(message = "jabatan.nourut.notnull")
    private Integer noUrut;
    
    private String kdJabatanHead;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    //@NotNull(message = "jabatan.kddepartemen.notnull")
    private String kdDepartemen;
    
    @NotNull(message = "jabatan.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
