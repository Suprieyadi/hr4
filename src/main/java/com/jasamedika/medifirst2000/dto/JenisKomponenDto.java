/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class JenisKomponenDto {
    
    private Integer kode;
    
    @NotNull(message = "jeniskomponen.namajeniskomponen.notnull")
    private String namaJenisKomponen;
    
    @NotNull(message = "jeniskomponen.reportdisplay.notnull")
    private String reportDisplay;
    
    private Integer noUrut;
    
    private Integer kdJenisKomponenHead;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    //@NotNull(message = "jeniskomponen.kddepartemen.notnull")
    private String kdDepartemen;
    
    @NotNull(message = "jeniskomponen.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
