package com.jasamedika.medifirst2000.dto;

import java.util.List;

import com.jasamedika.medifirst2000.entity.ProfileHistoriStrategyMap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataProfileHistoriStrategyMapDto {
	
	List<ProfileHistoriStrategyMapDto> profileHistoriStrategyMap;

}
