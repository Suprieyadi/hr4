package com.jasamedika.medifirst2000.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileHistoriStrategyMapDKPIDto {

	@NotNull(message = "profilehistoristrategymapdkpiid.nohistori.notnull")
	private String noHistori;

	@NotNull(message = "profilehistoristrategymapdkpiid.kddepartemend.notnull")
	private String kdDepartemenD;

	@NotNull(message = "profilehistoristrategymapdkpiid.kdperspectived.notnull")
	private Integer kdPerspectiveD;

	@NotNull(message = "profilehistoristrategymapdkpiid.kdstrategyd.notnull")
	private Integer kdStrategyD;

	@NotNull(message = "profilehistoristrategymapdkpiid.kdkpi.notnull")
	private Integer kdKPI;

	@NotNull(message = "profilehistoristrategymapdkpi.kdperspective.notnull")
	private Integer kdPerspective;

	@NotNull(message = "profilehistoristrategymapdkpi.kdstrategy.notnull")
	private Integer kdStrategy;

	@NotNull(message = "profilehistoristrategymapdkpi.bobotkpi.notnull")
	private Float bobotKPI;

	@NotNull(message = "profilehistoristrategymapdkpi.targetkpimin.notnull")
	private Float targetKPIMin;

	private Float targetKPIMax;

	@NotNull(message = "profilehistoristrategymapdkpi.kdmetodehitungactual.notnull")
	private Integer kdMetodeHitungActual;

	private Integer kdAsalData;

	private Integer kdPeriodeData;

	private String keteranganLainnya;

	@NotNull(message = "profilehistoristrategymapdkpi.kddepartemen.notnull")
	private String kdDepartemen;

	@NotNull(message = "profilehistoristrategymapdkpi.statusenabled.notnull")
	private Boolean statusEnabled;

	private String noRec;

	private Integer version;

	private Long tglAwal;

	private Long tglAkhir;

}
