/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DetailJenisProdukDto {
    
    private String kode;
    
    @NotNull(message = "detailjenisproduk.detailjenisproduk.notnull")
    private String namaDetailJenisProduk;
    
    @NotNull(message = "detailjenisproduk.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kdJenisProduk;
    
    @NotNull(message = "detailjenisproduk.persenhargacito.notnull")
    private Float persenHargaCito;
    
    private String kdDepartemen;
    
    private Integer kdAccount;
    
    private Integer isRegistrasiAset;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "detailjenisproduk.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
