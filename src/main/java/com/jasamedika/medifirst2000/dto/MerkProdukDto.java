/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 12/12/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import  javax.validation.constraints.NotNull;
import  javax.validation.constraints.Max;
import  lombok.Getter;
import  lombok.Setter;


@Getter
@Setter
public class MerkProdukDto {
    
    private Integer kode;
    
    @NotNull(message = "merkproduk.merkproduk.notnull")
    private String namaMerkProduk;
    
    @NotNull(message = "merkproduk.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kdDepartemen;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "merkproduk.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
