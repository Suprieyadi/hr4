package com.jasamedika.medifirst2000.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataProfileHistoriStrategyMapDDto {
	
	List<ProfileHistoriStrategyMapDDto> profileHistoriStrategyMapD;

}
