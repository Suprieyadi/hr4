package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileHistoriStrategyMapDto {
	
	@NotNull(message = "profilehistoristrategymapid.nohistori.notnull")
    private String noHistori;
	
	@NotNull(message = "profilehistoristrategymapid.kdperspective.notnull")
    private Integer kdPerspective;
	
	@NotNull(message = "profilehistoristrategymapid.kdstrategy.notnull")
    private Integer kdStrategy;
	
    private String kdDepartemen;
	
    @NotNull(message = "profilehistoristrategymap.statusenabled.notnull")
    private Boolean statusEnabled;
	
    private String noRec;
	
    private Integer version;
    
    private Long tglAwal;
    
    private Long tglAkhir;

}
