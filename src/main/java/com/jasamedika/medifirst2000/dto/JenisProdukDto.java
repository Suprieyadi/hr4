/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class JenisProdukDto {
    
    private String kode;
    
    @NotNull(message = "jenisproduk.jenisproduk.notnull")
    private String namaJenisProduk;
    
    @NotNull(message = "jenisproduk.reportdisplay.notnull")
    private String reportDisplay;
    
    //@NotNull(message = "jenisproduk.kdkelompokproduk.notnull")
    private Integer kdKelompokProduk;
    
    //@NotNull(message = "jenisproduk.kddepartemen.notnull")
    private String kdDepartemen;
    
    private Integer kdAccount;
    
    private String kdJenisProdukHead;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "jenisproduk.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
