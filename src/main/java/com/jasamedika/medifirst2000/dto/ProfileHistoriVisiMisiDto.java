package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileHistoriVisiMisiDto {
	
	@NotNull(message = "profilehistorivisimisiid.nohistori.notnull")
    private String noHistori;
	
	@NotNull(message = "profilehistorivisimisiid.kdmisi.notnull")
    private Integer kdMisi;
	
	private String keteranganLainnya;
	
    private String kdDepartemen;
	
    private String noRec;
	
    private Integer version;

}
