/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class JenisTransaksiDto {
    
    private Integer kode;
    
    @NotNull(message = "jenistransaksi.jenistransaksi.notnull")
    private String namaJenisTransaksi;
    
    @NotNull(message = "jenistransaksi.reportdisplay.notnull")
    private String reportDisplay;
    
    @NotNull(message = "jenistransaksi.metodeharganetto.notnull")
    private Integer metodeHargaNetto;
    
    @NotNull(message = "jenistransaksi.metodestokharganetto.notnull")
    private Integer metodeStokHargaNetto;
    
    @NotNull(message = "jenistransaksi.metodeambilharganetto.notnull")
    private Integer metodeAmbilHargaNetto;
    
    @NotNull(message = "jenistransaksi.sistemharganetto.notnull")
    private Integer sistemHargaNetto;
    
    @NotNull(message = "jenistransaksi.jenispersencito.notnull")
    private String jenisPersenCito;
    
    private String kdProdukCito;
    
    private String kdProdukRetur;
    
    private String kdKelasDefault;
    
    private String kdProdukDeposit;
    
    private Long  tglBerlakuTarif;
    
    private Integer kdKelompokPelayanan;
    
    private Integer sistemDiscount;
    
    private String kodeExternal;
    
    private String namaExternal;
    
   // @NotNull(message = "jenistransaksi.kddepartemen.notnull")
    private String kdDepartemen;
    
    @NotNull(message = "jenistransaksi.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
