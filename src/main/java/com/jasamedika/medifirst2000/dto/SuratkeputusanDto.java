package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SuratkeputusanDto
{
	private String pathFile;
	
	private Integer kdJenisKeputusan;
	
	@NotNull(message = "kdKelompokTransaksi.notnull")
	private Integer kdKelompokTransaksi;
	
	private String keteranganLainnya;
	
	private Integer version;
	
	@NotNull(message = "namaSK.notnull")
	private String namaSK;
	
	private String kdPegawai;
	
	private Long tglBerlakuAkhir;
	
	@NotNull(message = "suratkeputusan.tglberlakuawal.notnull")
	private Long tglBerlakuAwal;
	
	
	
	@Length(min = 1, max = 10, message = "noSK must be between 1 and 10 characters in length.")
	private String noSK;
 
    
}