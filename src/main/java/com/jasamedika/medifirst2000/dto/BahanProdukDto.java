/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BahanProdukDto {
    
    private Integer kode;
    
	@NotNull(message = "bahanproduk.namabahanproduk.notnull")
	private String namaBahanProduk;

	private String reportDisplay;
    
	private String kdDepartemen;

	private Integer kdKelompokProduk;
	
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "agama.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
