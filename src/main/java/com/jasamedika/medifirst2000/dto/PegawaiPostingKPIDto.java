package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PegawaiPostingKPIDto {
	
	@NotNull(message = "pegawaipostingkpiid.noposting.notnull")
    private String noPosting;
	
	@NotNull(message = "pegawaipostingkpiid.kdpegawai.notnull")
    private String kdPegawai;
	
	@NotNull(message = "pegawaipostingkpiid.nohistori.notnull")
    private String noHistori;
	
	@NotNull(message = "pegawaipostingkpiid.kddepartemend.notnull")
    private String kdDepartemenD;
	
	@NotNull(message = "pegawaipostingkpiid.kdperspectived.notnull")
    private Integer kdPerspectiveD;
	
	@NotNull(message = "pegawaipostingkpiid.kdstrategyd.notnull")
    private Integer kdStrategyD;
	
	@NotNull(message = "pegawaipostingkpiid.kdkpi.notnull")
    private Integer kdKPI;
	
	@NotNull(message = "pegawaipostingkpi.bobotkpi.notnull")
    private Float bobotKPI;
	
	@NotNull(message = "pegawaipostingkpi.targetkpi.notnull")
    private Float targetKPI;
	
	@NotNull(message = "pegawaipostingkpi.kdmetodehitungactual.notnull")
    private Integer kdMetodeHitungActual;
	
	private String keteranganLainnya;
	
	private String noRetur;
	
	private String noVerifikasi;
	
	private String noSK;
	
	private Integer kdKPISK;
	
	private Float bobotKPISK;
	
	private Float targetKPIMinSK;
	
	private Float targetKPIMaxSK;
	
	private Integer kdMetodeHitungActualSK;
	
	private Integer kdPeriodeDataSK;
	
	private String noHistoriActual;
	
	private Float totalActualKPI;
	
	private Float totalScoreKPI;
	
	private String noVerifikasiActual;
	
	@NotNull(message = "profilehistoristrategymapdkpi.statusenabled.notnull")
    private Boolean statusEnabled;
	
	private String noRec;

	private Integer version;

	private Long tglAwal;

	private Long tglAkhir;

}
