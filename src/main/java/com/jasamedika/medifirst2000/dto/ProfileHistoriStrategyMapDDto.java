package com.jasamedika.medifirst2000.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileHistoriStrategyMapDDto {
	
	@NotNull(message = "profilehistoristrategymapdid.nohistori.notnull")
    private String noHistori;
	
	@NotNull(message = "profilehistoristrategymapd.kdperspective.notnull")
    private Integer kdPerspective;
	
	@NotNull(message = "profilehistoristrategymapd.kdstrategy.notnull")
    private Integer kdStrategy;
	
    private String kdDepartemen;
	
    @NotNull(message = "profilehistoristrategymapd.statusenabled.notnull")
    private Boolean statusEnabled;
	
    private String noRec;
	
    private Integer version;
    
    private Long tglAwal;
    
    private Long tglAkhir;
    
    @NotNull(message = "profilehistoristrategymapdid.kdperspectived.notnull")
    private Integer kdPerspectiveD;
	
	@NotNull(message = "profilehistoristrategymapdid.kdstrategyd.notnull")
    private Integer kdStrategyD;
	
	@NotNull(message = "profilehistoristrategymapdid.kdstrategyd.notnull")
    private String kdDepartemenD;

}
